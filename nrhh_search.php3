<?
	$logout = true; 
	$cfgProgDir =  'login/phpSecurePages/';
	include($cfgProgDir . "secure.php");
	include ("config/dbinfo.inc.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>    
    <title>NRHH OTMs</title>
    <link rel="shortcut icon" href="/img/favicon.ico" />
    <link rel="stylesheet" href="nrhh-base.css" type="text/css" />
  </head>

  <body>
<img id="navlogo" src="/img/otm_logo.png" alt="NRHH Logo" />
<span id="header">
<b><font size="+1">Welcome to the</font><br><font size="+2">OTM Database Search</center></b></font>
</span><BR><BR>
<span id="navtabs">
</td>
</tr>
</table>

</span>
<a href=/support.php><img id="navhelp" src="img/help.gif" alt="Help" /></a>
<a href=/faqs.php id="navhelp"><BR><BR><B>F.A.Q.</B></a>&nbsp;&nbsp;<BR>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr class="bar-undertabs">
        <td><div></div></td>
      </tr>
</table>
    

    <table class="bar-status" width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr valign="middle">
        <td class="bar-login" width="25%">
	&#160; 
        </td>

<!--
        <td class="bar-search" width="65%">
          <div>
	<form name="form58" method="GET" action="/nrhh_search_query.php3"><B>Search OTMs for </b>Month: 
<?
include ($base_folder . "/queries/queries_lu_months.h");
echo "<select name='month'>";
echo "<option value=''></option>\r\n";

$i=0;
while ($i < $lum_num) {
$cdid=mysql_result($lum_result,$i,"lumid");
$month_name=mysql_result($lum_result,$i,"name");

echo "<OPTION VALUE='$cdid'>$month_name</OPTION>\r\n";
++$i;
}
?>
              </select>
Year: <input type=text name="year" size=7>
<input type="image"  name="image57" align="top" border="0" src="img/button-search.gif" /></form></div>

        </td>

-->
        <td class="bar-ssm" width="30%">
          &#160; 
        </td>

      </tr>
    </table>


    <table width="90%" align="center" cellspacing="20" cellpadding="0">
      <tr valign="top" align="left">
        <td>

	  <div class="linkchain">
	    <a href="/">NRHH Database Home</a>
	    <BR>
	  </div>


         <h2>Submit OTMS</h2>
	  <div class="linkchain">
	    <a href="/form_general.php3">General Category</a>
	    <BR>
	    <a href="/form_program.php3">Program Category</a>
	  </div>
<!--
         <h2>Search OTMS</h2>
	  <div class="linkchain">
	    <a href="/nrhh_search.php3">Advanced Search</a>
	    <BR>
	  </div>
-->
        <H2>Quick Report Links</H2>
          <div class="linkchain">
           <a 
href="/nrhh_search_query.php3?column=coid&condition=is&keyword_crid=&either_crid=AND&column=coid&condition=is&keyword_coid=&either=AND&column_ccid=ccid&condition_ccid=is&keyword_ccid=&either2=AND&column_award=award&condition_award=is&keyword_award=6&column_ym=year_month&condition_ym=is&keyword_ym=&year=&either4=AND&column_nom=nominee&condition_nom=is&keyword_nom=&cl=iau&order_field=otms.year_month&order=DESC&llimit=0&increment=250">NACURH 
Winners</A><BR>
          <a href="
/nrhh_search_query.php3?column=coid&condition=is&keyword_crid=&either_crid=AND&column=coid&condition=is&keyword_coid=&either=AND&column_ccid=ccid&condition_ccid=is&keyword_ccid=&either2=AND&column_award=award&condition_award=%3E%3D&keyword_award=4&column_ym=year_month&condition_ym=is&keyword_ym=&year=&either4=AND&column_nom=nominee&condition_nom=is&keyword_nom=&cl=iau&order_field=otms.year_month&order=DESC&llimit=0&increment=250">Regional 
Winners</A>
        <BR>

<? 
$query="select * from config_orgs left join config_dates_otmview on config_orgs.coid=config_dates_otmview.coid where config_orgs.level='regional' and month_view=substring(NOW(),6,2) and date_view < 
substring(NOW(),9,2)";
        $result=mysql_query($query);
        $num=mysql_numrows($result);

$i=0;
while ($i < $num) {
$coid=mysql_result($result,$i,"config_orgs.coid");
$org_name=mysql_result($result,$i,"config_orgs.name");
$lookup_month=mysql_result($result,$i,"month");

if ((intval($month_due) < $lookup_month)) {
        $show_year = $today_year - 1;
} else {
        $show_year = $today_year;
}

echo "<a 
href='/nrhh_search_query.php3?column=coid&condition=is&keyword_crid=$coid&either_crid=AND&column=coid&condition=is&keyword_coid=&either=AND&column_ccid=ccid&condition_ccid=is&keyword_ccid=&either2=AND&column_award=award&condition_award=is&keyword_award=&column_ym=year_month&condition_ym=is&keyword_ym=$lookup_month&year=$show_year&either4=AND&column_nom=nominee&condition_nom=is&keyword_nom=&cl=iau&order_field=config_categories.name&order=ASC&llimit=0&increment=250'>
$lookup_month/$show_year $org_name OTMs</A><BR>";

++$i;
}

?>

        </div>


          <h2>Sign into OTMS.NRHH.org</h2>

<div class="login-component">
  <form method="post" action='login/'>
    <div class="login-box">
        <div class="input-row">
          Username:  <input tabindex="2" type="text" name="entered_login" size="10" maxlength="64" value="" />
        </div>
        <div class="input-row">
          Password: <input tabindex="2" type="password" name="entered_password" size="10" maxlength="32" />
        </div>
        <div class="input-row">
          <input tabindex="2" type="submit" value=" &#160; Sign In&#160; " />
        </div>  
    </div>
  </form>
  <div class="linkchain">
    <a href="/form_create_account.php3">Create Account</a>
    <BR>
    <a href="/form_password.php3">Lost Password?</a>
  </div>
  
</div><BR><BR>
<CENTER><img src="img/img_logos.gif"></CENTER>
        </td>




<!-- MIDDLE COLUMN -->
        <td>

        <h2>Search OTMS</h2>

<? 
include ($base_folder . "/nrhh_search.h");
?>	



	</td>
<!--
<TD width=20%>
<CENTER><h2>NACRUH NRHH Website:</h2>
  <div class="linkchain">
<a href="http://www.nrhh.org">NACURH NRHH</a></div>
</CENTER>

<CENTER><h2>Regional NRHH Websites:</h2>
  <div class="linkchain">
<?
$cl_query="SELECT * FROM config_orgs WHERE level='regional' order by name";
$cl_result=mysql_query($cl_query);
$cl_num=mysql_numrows($cl_result);

$i = 0;
while ($i < $cl_num) {
	$orgname=mysql_result($cl_result,$i,"name");
	$nrhhweb=mysql_result($cl_result,$i,"nrhhweb");
	$nrhhwebaddress=mysql_result($cl_result,$i,"nrhhwebaddress");

	if (($nrhhweb == 1) && ($nrhhwebaddress != "")) {
		echo "<a href='http://$nrhhwebaddress' target='_new'>$orgname</a><BR>\r\n";
	} else { 
		echo "$orgname<BR>\r\n";
	}
	++$i;
}
?>
</div>
</CENTER>

<CENTER><h2>Campus NRHH Websites:</h2>
  <div class="linkchain">
<?
$i = 0;
while ($i < $cl_num) {
	$orgname=mysql_result($cl_result,$i,"name");
	$crid=mysql_result($cl_result,$i,"coid");

	if (($nrhhweb == 1) && ($nrhhwebaddress != "")) {
		echo "<a href='/chapter_links.php?region=$crid' target='_new'>$orgname</a><BR>\r\n";
	} else { 
		echo "$orgname<BR>\r\n";
	}
	++$i;
}
?>
</div>
</CENTER>

	</TD> -->
      </tr>
    </table>

<? include "copyright.h"; ?>

  </body>
</html>




