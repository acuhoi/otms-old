
<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../stylesheet.css'>
<TITLE>FAQs</TITLE>
</HEAD>
<BODY class=BODY>

<TABLE class=otm-table cellpadding='5' width=760>
<TR><TD>

<H3>Users</H3>

<UL>
<LI><a href="#ua1">What are the benefits to creating an account?</A>
<LI><a href="#ua2">I submitted an OTM before I created an account.  Can I edit it before the deadline?</a>
<LI><a href="#ua3">How do I create an account?</a>
<LI><a href="#ua4">I forgot my password.  What do I do?</a>
<LI><a href="#ua5">How come when I search for an OTM, I don't see the ones for this month?</a>
<LI><a href="#ua6">I can't edit previously submitted OTMs.  Why?</a>
<LI><a href="#ua7">My account is inactive.  What does that mean?</a>
</UL>


<H4><a name="ua2">What are the benefits to creating an account?</A></H4>
<UL>There are numerous benefits to creating an account with the OTM System.  Just a few include:<BR>
<UL><LI>Having the ability to edit OTMs before they are due
<LI>The system will partially fill out the OTM form for you
<LI>Keep track of previously submitted OTMs
<LI>See when OTMs are due to your campus or regional level
</UL>
</UL>

<H4><a name="ua2">I submitted an OTM before I created an account.  Can I edit it before the deadline?</a></H4>
<UL>OTMs submitted without first loggin into an account can not be edited by a user.  The only way for a user to edit an OTM after it has been submitted 
is by first logging into the system to submit the OTM.</UL>


<H4><a name="ua3">How do I create an account?</a></H4>
<UL>To create an account, direct your browser to the <I><a href="/form_create_account.php3">Create an Account</a></I> Page.  
Select the University that you currently attend and hit submit.  Then, fill out the next form.  When completed and after submitted, you will 
see a page saying that it will take approximately 24 hours for your account to be activated.  Every account submitted is automatically set to be 
inactive and an administrator will have to activate and validate your account request.  If you campus administrator has specified to send users 
an e-mail when their account is granted or denied, you will receive notification when it has been done.
</UL>


<H4><a name="ua4">How come when I search for an OTM, I don't see the ones for this month?</a></H4>
<UL>The dates that OTMs can be searched for are at the discretion of your regional administrator.  After logging into the system, check the dates to know 
when OTMs can be searched.  If you received confirmation that your OTM was submitted, then you can be rest assured that it has.  Just because you can not search 
for it doesn't mean that your campus and/or region will not be able to see it.
</UL>

<H4><a name="ua5">I can't edit previously submitted OTMs.  Why?</a></H4>
<UL>Only OTMs that have been submitted for the current month can be edited by a user.  Previous month submissions can not be edited.</UL>

<H4><a name="ua6">My account is inactive.  What does that mean?</a></H4>
<UL>When an account is inactive, you will not be able to log in and therefore will not be able to submit OTMs under that account.  If you are in 
need of submitting an OTM, you may submit one without logging into an account.

</TD></TR>
</TABLE>

</BODY>


</HTML>
