<?
	$requiredUserLevel = array(1,10,20,30);
	$cfgProgDir = 'phpSecurePages/';
	include($cfgProgDir . "secure.php");

include("../config/dbinfo.inc.php");
mysql_connect(localhost,$dbusername,$dbpassword);
@mysql_select_db($dbdatabase) or die( "Unable to select database");

if ($command=="adminupdate") { }
else {
include($base_folder . "/queries/queries_user.h");
}
include($base_folder . "/queries/queries_org.h");
?>

<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<HTML>
<HEAD>
<TITLE>General OTM Nomination Form</TITLE>
<? echo $stylesheet; ?>

</HEAD>
<FORM ACTION='/login/config_campus_info_update.php3' NAME='form1' METHOD='post'>

<CENTER>
<H2>Campus Change Form</H2>
<h5>for</h5>

<? echo "<h3>$school</h3><h4>$school_region_name Region</h4>\r\n"; ?>



<TABLE class='box-base' BORDER='0' CELLSPACING='0' CELLPADDING='8' WIDTH='500'>
<?
echo "<INPUT TYPE='hidden' NAME='coid' VALUE='$coid'>\r\n";
echo "<INPUT TYPE='hidden' NAME='parent_org_id' VALUE='$school_region_id'>\r\n";
if ($userLevel != 1) {
echo "<INPUT TYPE='hidden' NAME='school' VALUE='$school'>\r\n";
}
echo "<INPUT TYPE='hidden' NAME='active' VALUE='$conf_org_active'>\r\n";
?>

<TR>
<TH WIDTH=300 align=left><B><U>Field</b></TD>
<TH WIDTH=300 align=center><U>Your Input</TD>
</TR>

<?
if ($userLevel == 1) {
echo "<TR><TD WIDTH=200 align=left><B>Name</b></TD>";
echo "<TD WIDTH=300 align=center><INPUT NAME='school' SIZE='30' maxlength='50' VALUE='$school'></TD>\r\n";
echo "</TR>";
}

?>

<TR>
<TD WIDTH=200 align=left><B>1.  Campus Population</b></TD>
<? echo "<TD WIDTH=300 align=center><INPUT NAME='campus_pop' SIZE='10' maxlength='5' VALUE='$campus_pop'></TD>\r\n"; ?>
</TR>

<TR>
<TD WIDTH=200 align=left><b>2.  NRHH Chapter Size</b></TD>
<? echo "<TD WIDTH=300 align=center><INPUT NAME='chapter_size' SIZE='10' maxlength='3' VALUE='$chapter_size'></TD>\r\n"; ?>
</TR>

<TR>
<TD WIDTH=200 align=left><b>3.  Does your University have an NRHH Chapter?</b></TD>
<?
echo "<TD WIDTH=300><CENTER>\r\n";
if ($nrhh == 1) { echo "<input type='radio' name='nrhh' value='1' CHECKED> Yes  <input type='radio' name='nrhh' value='0'> No\r\n"; }
else { echo "<input type='radio' name='nrhh' value='1'> Yes <input type='radio' name='nrhh' value='0' CHECKED> No\r\n"; }
echo "</TD></TR>\r\n";
?>
</TR>

<TR>
<TD WIDTH=200 align=left><b>4.  Does your NRHH Chapter have a website?</b></TD>
<?
echo "<TD WIDTH=300><CENTER>\r\n";
if ($nrhhweb == 1) { echo "<input type='radio' name='nrhhweb' value='1' CHECKED> Yes  <input type='radio' name='nrhhweb' value='0'> No\r\n"; }
else { echo "<input type='radio' name='nrhhweb' value='1'> Yes <input type='radio' name='nrhhweb' value='0' CHECKED> No\r\n"; }
echo "</TD></TR>\r\n";
?>
</TR>

<TR>
<TD WIDTH=200 align=left><b>5.  If so, what is the web address?</b></TD>
<? echo "<TD WIDTH=300 align=center><INPUT NAME='nrhhwebaddress' SIZE='25' maxlength='50' VALUE='$nrhhwebaddress'><BR><I>Do not include the 'http://'</TD>\r\n"; ?>

</TR>

<TR>
<TD WIDTH=200 align=left><b>6.  Administrator's E-mail</b></TD>
<? echo "<TD WIDTH=300 align=center><INPUT NAME='admin_email' SIZE='25' maxlength='50' VALUE='$admin_email'></TD>\r\n"; ?>
</TR>


<TR>
<TD WIDTH=200 align=left><b>7.  Do you keep track of individual hall submissions?</b></TD>
<?
echo "<TD WIDTH=300><CENTER>\r\n";
if ($allow_suborgs == 1) {
echo "<input type='radio' name='allow_suborgs' value='1' CHECKED> Yes  <input type='radio' name='allow_suborgs' value='0'> No\r\n"; 
echo "<P><a href='config_suborgs.php?coid=$coid'>Configure Hall Organizations</a>\r\n";
}
else { echo "<input type='radio' name='allow_suborgs' value='1'> Yes <input type='radio' name='allow_suborgs' value='0' CHECKED> No\r\n"; }
echo "</TD></TR>\r\n";

echo "<TR><TD WIDTH=200 align=left><b>8.  Will your campus submit OTMs into the database before the regional deadline and then determine who will advance to the regional level?</b></TD>";
echo "<TD WIDTH=300><CENTER>\r\n";
if ($allow_presubmit == 1) { echo "<input type='radio' name='allow_presubmit' value='1' CHECKED> Yes  <input type='radio' name='allow_presubmit' value='0'> No\r\n"; }
else { echo "<input type='radio' name='allow_presubmit' value='1'> Yes <input type='radio' name='allow_presubmit' value='0' CHECKED> No\r\n"; }
echo "</TD></TR>\r\n";

if ($allow_presubmit == 1) {
echo "<TR><TD WIDTH=200 align=left><b>8B.  Turn on system voting for Campus OTM Committee?</b></TD>";
echo "<TD WIDTH=300><CENTER>\r\n";
if ($allow_voting == 1) { echo "<input type='radio' name='allow_voting' value='1' CHECKED> Yes  <input type='radio' name='allow_voting' value='0'> No\r\n"; }
else { echo "<input type='radio' name='allow_voting' value='1'> Yes <input type='radio' name='allow_voting' value='0' CHECKED> No\r\n"; }
echo "</TD></TR>\r\n";
}

echo "<TR><TD WIDTH=200 align=left><b>9.  E-mail Administrator when an account is requested</b></TD>";
echo "<TD WIDTH=300><CENTER>\r\n";
if ($ar_email_admin == 1) { echo "<input type='radio' name='ar_email_admin' value='1' CHECKED> Yes  <input type='radio' name='ar_email_admin' value='0'> No\r\n"; }
else { echo "<input type='radio' name='ar_email_admin' value='1'> Yes <input type='radio' name='ar_email_admin' value='0' CHECKED> No\r\n"; }
echo "</TD></TR>\r\n";

echo "<TR><TD WIDTH=200 align=left><b>10. E-mail user when account is activated by e-mail link</b></TD>";
echo "<TD WIDTH=300><CENTER>\r\n";
if ($ar_email_activated == 1) { echo "<input type='radio' name='ar_email_activated' value='1' CHECKED> Yes  <input type='radio' name='ar_email_activated' value='0'> No\r\n"; }
else { echo "<input type='radio' name='ar_email_activated' value='1'> Yes <input type='radio' name='ar_email_activated' value='0' CHECKED> No\r\n"; }
echo "</TD></TR>\r\n";

echo "<TR><TD WIDTH=200 align=left><b>11. E-mail user when account denied by e-mail link</b></TD>";
echo "<TD WIDTH=300><CENTER>\r\n";
if ($ar_email_denied == 1) { echo "<input type='radio' name='ar_email_denied' value='1' CHECKED> Yes  <input type='radio' name='ar_email_denied' value='0'> No\r\n"; }
else { echo "<input type='radio' name='ar_email_denied' value='1'> Yes <input type='radio' name='ar_email_denied' value='0' CHECKED> No\r\n"; }
echo "</TD></TR>\r\n";

echo "<TR><TD WIDTH=200 align=left><b>12. Turn on OTM Blurb Question?</b><BR><i>Regional Administrators can supercede this question</I></TD>";
echo "<TD WIDTH=300><CENTER>\r\n";
if ($otm_blurb_local == 1) { echo "<input type='radio' name='otm_blurb_local' value='1' CHECKED> Yes  <input type='radio' name='otm_blurb_local' value='0'> No\r\n"; }
else { echo "<input type='radio' name='otm_blurb_local' value='1'> Yes <input type='radio' name='otm_blurb_local' value='0' CHECKED> No\r\n"; }
echo "</TD></TR>\r\n";

if ($allow_presubmit == 1) {
?>

<TR>
<TD align=left colspan=2><TABLE class='box-base' align=center><TR><TD colspan=2 align=center><b>Please choose the categories your campus uses?</b></TD></TR>
<?
include($base_folder . "/queries/queries_categories.h");
$i=0;
while ($i < $conf_cat_num) {
$ccid=mysql_result($conf_cat_result,$i,"ccid");
$cat_name=mysql_result($conf_cat_result,$i,"name");
$cat_type=mysql_result($conf_cat_result,$i,"category_type");
$active=mysql_result($conf_cat_result,$i,"active");

echo "<TR><TD WIDTH=300 align=left><input type='checkbox' name='cb$i' value='$ccid' \r\n";
if (in_array ($ccid, $ar_cat)) { echo "CHECKED\r\n"; } else { } 
echo ">$cat_name</TD><TD width=100>$cat_type</TD></TR>\r\n";
// if ($cc_oid == 1) { echo "CHECKED"; } else { }
++$i;
}
echo "<INPUT TYPE='hidden' NAME='numbers' VALUE='$i'>\r\n";
echo "</TD></TR></TABLE>\r\n";
}
?>
<TR><TD colspan='2'><CENTER><B>Default E-mail Distribution List</B><BR><TEXTAREA NAME="email_distribution_list" WRAP=HARD ROWS=15 COLS=50>
<? echo $email_distribution_list; ?>
</TEXTAREA>
<BR>Please separate each e-mail address with a comma.
</TD></TR>

<TR><TD COLSPAN=3 ALIGN=CENTER>
<INPUT TYPE='button' VALUE='Update' OnClick='mysave(form1)'>
</TD></TR>

</TABLE>
</CENTER>
</FORM><script language="JavaScript" type="text/javascript">
function isempty(type) {
	if(type.length == 0) {
		return true
	}

	return false
}

function selectedboxes(cnt){
cnt = "";
enb = "";
var numb=document.form1.numbers.value;
   for(i=0; i<numb; i++){
   var enc=eval("document.form1.cb"+i+".checked");
     if(enc){
         enb=eval("document.form1.cb"+i+".value");
         if (cnt == "") { 
		cnt = enb;
		}
	 else {
         cnt = cnt + "," + enb;
	 }
     }
   }
   return cnt;
}

function mysave(form1) {
var str
var scb
<? if ($userLevel != 1) { ?>
        if(isempty(form1.chapter_size.value)) {
                alert("Error: Chapter Size is required.")
                form1.chapter_size.focus()
		return false
        }
        if(isempty(form1.campus_pop.value)) {
                alert("Error: Campus Population is required.")
                form1.campus_pop.focus()
		return false
        }
        if(isempty(form1.admin_email.value)) {
                alert("Error: An Administrator's E-mail is required.")
                form1.admin_email.focus()
                return false
        }

<?
}
if ($allow_presubmit == 1) {
echo "	scb = selectedboxes(); \r\n";
}
?>
 URL = "/login/config_campus_info_update.php3?selected_cat=" + scb;
	form1.action = URL
        form1.submit();	
}
</script>
</BODY></HTML>
