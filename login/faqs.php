<?
	$requiredUserLevel = array(1,10,20,30,35,40);
	$cfgProgDir = 'phpSecurePages/';
	include($cfgProgDir . "secure.php");


?>
<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../stylesheet.css'>
<TITLE>FAQs</TITLE>
</HEAD>
<BODY class=BODY>

<TABLE class=otm-table cellpadding='5' width=760>
<TR><TD>

<? if ($userLevel <= 20) { ?>
<H3>Regional Administrators</H3>
<UL>
<B>Regional Settings</B>
<LI><a href="#ra1">How do I configure the regional settings?</a>
<LI><a href="#ra10">How do I configure what categories universities may submit for?</a>
<LI><a href="#ra2">How do I add a university to my region?</a>
<LI><a href="#ra3">What is the difference between an active university and an inactive university?</a>
<LI><a href="#ra4">How do I change whether a university is active or inactive?</a>
<LI><a href="#ra5">I typed in a University Name wrong, why can't I delete it?</a>
</UL><UL><B>User Accounts</B>
<LI><a href="#ra6">Can I submit OTMs under my regional administrator account?</a>
<LI><a href="#ra7">What do the different access levels mean?  Campus Administrator, OTM Committee, and User?</a>
<LI><a href="#ra8">How do I designate someone as a campus administrator?</a>
</UL><UL><B>OTMs</B>
<LI><a href="#ra9">How come I can only see the OTMs, but not read them?  When can I read them?</a>
</UL><UL><B>Sending E-mails</B>
<LI><a href="#ra11">What is so great about having the system send out an e-mail of the regional winners?</a>
<LI><a href="#ra12">When sending a e-mail through the OTM System to a listserv, it doesn't get sent.  What's wrong?</a>
</UL><UL>
<LI><a href="#rao">What other information should I know?</a>
</UL>

<? } if ($userLevel <=30) { ?>
<H3>Campus Administrators</H3>

<UL><B>University Settings</B>
<LI><a href="#ca1">What are my responsibilities as a Campus Administrator?</a>
<LI><a href="#ca2">How do I configure the campus settings?</a>
</UL>
<UL><B>Universities that presubmit OTMs to the local level</B>
<LI><a href="#ca3">What does <I>presubmitting</I> mean?</a>
<LI><a href="#ca4">How do I configure what categories my university may submit for?</a>
<LI><a href="#ca5">How do I set local due dates?</a>
</UL>
<UL><B>Keeping track of individual hall sumibssions</B>
<LI><a href="#ca6">How do I configure the system to keep track of individual hall submissions?
<LI><a href="#ca7">How do I add a hall to my university?</a>
<LI><a href="#ca8">What is the difference between an active hall and an inactive hall?</a>
<LI><a href="#ca9">How do I change whether a hall is active or inactive?</a>
<LI><a href="#ca10">I typed in a Hall Name wrong, why can't I delete it?</a>
</UL>
<UL><B>User Accounts</B>
<LI><a href="#ca11">What is the process for people to get an account?</a>
<LI><a href="#ca12">I get an e-mail from the OTM DB System saying that someone is requesting an account.  What do I do?</a>
<LI><a href="#ca13">A user forgot his password.  How can I see what it was to let him know?
<LI><a href="#ca14">What do the different access levels mean?  Campus Administrator, OTM Committee, and User?</a>
<LI><a href="#ca15">How do I designate someone as a campus administrator or OTM committee member?</a>
<LI><a href="#ca16">Can I submit OTMs under my campus administrator account?</a>
</UL>
<UL><B>Sending E-mails</B>
<LI><a href="#ca17">What is so great about having the system send out an e-mail of the campus winners?</a>

</UL>
<UL><B>Submitting OTMs</B>
<LI><a href="#ca18">How come some universities have different categories than others?</a>
<LI><a href="#ca19">There are no categories in the drop down menu to select when submitting an OTM.  What's wrong?
<LI><a href="#ca20">When sending a e-mail through the OTM System to a listserv, it doesn't get sent.  What's wrong?</a>
</UL>

<UL><B>Forwarding OTMs to the regional level</B>
<LI><a href="#ca21">When can I forward OTMs on to the regional level?</a>
<LI><a href="#ca22">The column to submit OTMs to the regional level vanished!  How can I submit them on?</a>
<LI><a href="#ca23">I changed my OTMs to campus winners, but the regional level can not see them.  Why?</a>
<LI><a href="#ca24">The system is not allowing me to submit a specific category on to the regional level.  Why?</a>
</UL>

<UL>
<LI><a href="#cao">What other information should I know?</a>
</UL>

<? } if ($userLevel <=40) { ?>
<H3>Users</H3>

<UL>
<LI><a href="#ua1">What are the benefits to creating an account?</A>
<LI><a href="#ua2">I submitted an OTM before I created an account.  Can I edit it before the deadline?</a>
<LI><a href="#ua3">How do I create an account?</a>
<LI><a href="#ua4">I forgot my password.  What do I do?</a>
<LI><a href="#ua5">How come when I search for an OTM, I don't see the ones for this month?</a>
<LI><a href="#ua6">I can't edit previously submitted OTMs.  Why?</a>
<LI><a href="#ua7">My account is inactive.  What does that mean?</a>
</UL>

<? } ?>

<? if ($userLevel <= 20) { ?>

<P>
<H4><a name="ra1">How do I configure the regional settings?</a></H4>
<UL>For the system to work properly, there are a few settings that need to be configured.  After
logging into the system with your regional account and password, click on <B>Regional Info</B> on the
left hand side.  This link will bring up a page for you to fill out.  Under administrator e-mail, stick in 
the e-mail address of your head administrator.  Some regions may elect to have multiple regional administrators. 
Answer the next three questions that deal with users creating accounts.  You would only receive an e-mail 
when someone requests an account with their main organization as your region. <P>

Now, one of the most important parts is selecting which categories universities in your region have the option to 
submit for.  If there is a category that is not listed, please send an e-mail to otmdb@andrewbell.info with the 
name of the category and type of OTM submission requested (general or program).  In the event that a category is not 
selected, univerities who submit directly to the regional level will not have the option of submitting an OTM for that 
category.  Universities who do presubmit to their local level have the option of selecting which categories they select, but 
the campus administrator will only be able to submit OTMs on to the regional level that you select. <P>

The Default E-mail Distribution list is optional.  It is designed so that if you send out regional winner e-mails to the same 
people every month, you will not have to type them in every time. <P>

The last step involves configuring the dates of when OTMs are do to your region.  Click on the <B>Dates - Regional</B>.  For each month, 
select the month and date in which OTMs are due to your level.  <B>All OTMs are due in central standard time</B> on the date/time that you select.
</UL>

<H4><a name="ra2">How do I add a university to my region?</a></H4>
<UL>Login to the system and select <B>Universities</B>.  Type in the name of the university, select whether the university will be 
active or inactive, and click Add.
</UL>

<H4><a name="ra3">What is the difference between an active university and an inactive university?</a></H4>
<UL>An inactive university is not allowed to submit OTMs and is also not allowed to log into the system.  Previously submitted OTMs 
by an inactive university can still be searched for.
</UL>

<H4><a name="ra4">How do I change whether a university is active or inactive?</a></H4>
<UL>Login to the system and select <B>Universities</B>.  A university is active if it has a checked box by it.  Uncheck the universities 
you'd like to make inactive or active and click Update.
</UL>

<H4><a name="ra5">I typed in a University Name wrong, why can't I delete it?</a></H4>
<UL>After a university is added, it can not be deleted.  This is due to if an OTM has been submitted for that university, it needs an
association with it.  If the name of the university was typed in wrong, send an e-mail to otmdb@andrewbell.info 
with the correct spelling and it will be adjusted.
</UL>

<H4><a name="ra6">Can I submit OTMs under my regional administrator account?</a></H4>
<UL>Because your regional account is associated with your region and not a university, regional account holders must request an 
account with their local university to submit.  If you do not wish to retain or edit OTMs that you have submitted, you may 
elect to submit through the main web page and selecting your univerity.
</UL>

<H4><a name="ra7">What do the different access levels mean?</a></H4>
<UL><B>Campus Administrator</B> - This account is for those who need to make modifications to and configure their local university.  They 
have a lot of the same options of a regional administrator, ie:  setting local dates, otm categories, whether to presubmit or not, 
if they'd like to keep track of hall submissions, and can also administrate users for their university.  Every university should have 
at lease one campus administrator.  Universities may have more than one if desired.  Campus Administrators can submit OTMs under their 
account.<P>
<B>OTM Committee</B> - Due to the fact that OTMs are not viewable until after the regional date has passed, universities that 
allow presubmission may need to have people judge which OTMs will be submitted to the regional level.  OTM Committee level access 
allows users to see OTMs submitted after their local due date.  OTM Committee members can submit OTMs under their account.  
During the grading period (dates between local due date and regional due date), the OTMs submitted for that month will automatically 
appear after logging into the system.<P>
<B>User</B> - This access is for those who want to keep track of OTMs submitted, be able to edit them, and also see when OTMs are due.<P>
</UL>

<H4><a name="ra8">How do I designate someone as a campus administrator?</a></H4>
<UL>Log into the system and select <B>Admin Users</B>.  This will bring up all users for your region.  If they have 
just requested an account, it will automatically be set as an inactive user.  After finding their name, select edit.  Scroll down to 
access and select campus administrator.  Make sure they are set to Active so that they may log in.
</UL>

<H4><a name="ra9">How come I can only see the OTMs, but not read them?  When can I read them?</a></H4>
<UL>You will see all OTMs submitted for your region from the 1st up to your regional deadline.  During this time period, you will see 
all OTMs submitted by schools in your region.  After the deadline, you will only see OTMs that have been submitted to the regional level 
and at the point will be able to read them.
</UL>

<H4><a name="ra10">How do I configure what categories universities may submit for?</a></H4>
<UL>Click on <B>Regional Info</B>.  Check each category that universities may submit.  Click on update.
</UL>

<H4><a name="ra11">What is so great about having the system send out an e-mail of the regional winners?</a></H4>
<UL>By using the <B>Send E-mail</B> option, the system can automatically generate the body of the e-mail and query the database 
to find out which OTMs were selected as regional winners.  Furthermore, you have the option of having the system place 
a link under each recipient so that people may be directed to that OTM without having the search through the database.
</UL>

<H4><a name="ra12">When sending a e-mail through the OTM System to a listserv, it doesn't get sent.  What's wrong?</a></H4>
<UL>There is a possibility that your listserv is set up in a way to only allow people who are on the list to send to the list.  To fix 
this problem, add the e-mail address of otms@otms.nrhh.org to your listserv.  Make sure it is set to not *receive* e-mail because it 
will bounce back to the listserv administrator.
</UL>

<H4><a name="rao">What other information should I know?</a></H4>
<LI>All system dates and times are Central Standard Time
</UL>

<? } if ($userLevel <=30) { ?>


<H4><a name="ca1">What are my responsibilities as a Campus Administrator?</a></H4>
<UL>The responsiblities of a Campus Administrator are to:<BR>
<UL><LI>Configure your university preferences.
    <LI>Manage user accounts for your university.
    <LI>If your university presubmits OTMs, you will need to submit campus winners to your regional level.
    <LI>If your university presubmits OTMs, you will need to configure campus OTM due dates.
</UL>
</UL>

<H4><a name="ca2">How do I configure the campus settings?</a></H4>

<H4><a name="ca3">What does presubmitting mean?</a></H4>
<UL>Presubmitting means that your university will submit OTMs to the campus level before submitting them on to the regional level.  The system was designed
so that universities who submit locally first can have a chance to read, judge, and forward them on to their region.
</UL>

<H4><a name="ca4">How do I configure what categories my university may submit for?</A></H4>
<UL>After logging into the system, click on <B>Campus Info</B>.  The only time a campus administrator can choose which categories their university 
may submit for is when they allow presubmissions to the local level.  Check the box near each category that your university accepts and click update.
</UL>


<H4><a name="ca5">How do I set local due dates?</a></H4>
<UL>To set local dates, the university must be set up to allow presubmissions to the local level.  After logging into the system, click on <B>Dates - Local</B>.  Click 
on edit next to each month, select the month, date, and hour you want them due, and click update.  All OTMs are due Central Standard Time on the date/time specified.  In the event that you do not 
have the option of specifying dates after changing your university preferences to allow presubmission, log out of the system and log back in.<P>
<B>Do NOT set the OTMs due at the same date and time that they are due on the regional level.</B>  When a campus decides to presubmit OTMs, the campus 
administrator <B>MUST</B> go in and promote them to the regional level for the regional administrator to see the OTM.  In the event that your campus decides to have them 
due at the same time as your regional level (for example of the summer), turn off presubmitting before people start submitting OTMs for that month.  This will set each OTM to be sent to the regional level after the due date.
</UL>

<H4><a name="ca6">How do I configure the system to keep track of individual hall submissions?</a></H4>
<UL>After logging into the system, click on <B>Campus Info</B> and make sure that <I>Keeping track of hall submissions</I> is set to <I>Yes</I>.  After clicking update, 
the system will automatically ask you to enter in individual halls or organizations that you would like to keep track of.  In the event that the system was already set 
to keep track of individual hall submissions, just click on the link that says <I>Configure Hall Organizations</I>.
</UL>

<H4><a name="ca7">How do I add a hall to my university?</a></H4>
<UL>Log into the system, click on <B>Campus Info</B> and then click on the link that says <I>Configure Hall Organizations</I>.  Type in the name of the organization you 
want to add and click the button that says <I>Add</I>.
</UL>

<H4><a name="ca8">What is the difference between an active hall and an inactive hall?</a></H4>
<UL>An active hall can submit OTMs, while an inactive hall will not show up in the list of drop down items to submit an OTM.
</UL>

<H4><a name="ca9">How do I change whether a hall is active or inactive?</a></H4>
<UL>Log into the system, click on <B>Campus Info</B> and then click on the link that says <I>Configure Hall Organizations</I>.  Uncheck the box next to the hall or 
organization and click update.
</UL>

<H4><a name="ca10">I typed in a Hall Name wrong, why can't I delete it?</a></H4>
<UL>After a hall or organization is added, it can not be deleted.  This is due to if an OTM has been submitted for that hall, it needs an
association with it.  If the name of the hall was typed in wrong, send an e-mail to otmdb@andrewbell.info 
with the correct spelling and it will be adjusted.
</UL>

<H4><a name="ca11">What is the process for people to get an account?</a></H4>
<UL>The process for people to get an account is quite simple.  Direct people to otms.nrhh.org and click on <B>Create an Account</B>.  The system will then prompt 
them for their university.  Click submit after the university has been selected and fill out the required information.  Click submit again.  All new accounts when 
they are created are defaulted to inactive.  They are instructed that it may take up to 24 hours for the account to be activated.  If the campus administrator has 
their campus settings set up to e-mail them in the event that an account is requested, you will receive an e-mail with two links.  One link will activate the account 
and the second one will remove the account request and deny the user from getting an account.  Dependent upon the campus configuration, the user may or may not receive 
an e-mail stating that the account request was granted or denied.
</UL>

<H4><a name="ca12">I get an e-mail from the OTM DB System saying that someone is requesting an account.  What do I do?</a></H4>
<UL>If the campus administrator has 
their campus settings set up to e-mail them in the event that an account is requested, you will receive an e-mail with two links.  One link will activate the account 
and the second one will remove the account request and deny the user from getting an account.  Dependent upon the campus configuration, the user may or may not receive 
an e-mail stating that the account request was granted or denied.
</UL>

<H4><a name="ca13">A user forgot his password.  How can I see what it was to let him know?</a></H4>
<UL>No administrator of the system can view a password that any account holder has.  In the event that a user has forgot his or her password, an administrator 
may go in and change it or an administrator may direct the user to otms.nrhh.org and have them click on the <i>Forget your password</I> link.  The user will then 
be prompted for some information to fill out and upon successful completion of the form, the system will change their password and send them an e-mail with the 
new information.
</UL>


<H4><a name="ca14">What do the different access levels mean?></a></H4>
<UL><B>Campus Administrator</B> - This account is for those who need to make modifications to and configure their local university.  They 
have a lot of the same options of a regional administrator, ie:  setting local dates, otm categories, whether to presubmit or not, 
if they'd like to keep track of hall submissions, and can also administrate users for their university.  Every university should have 
at lease one campus administrator.  Universities may have more than one if desired.  Campus Administrators can submit OTMs under their 
account.<P>
<B>OTM Committee</B> - Due to the fact that OTMs are not viewable until after the regional date has passed, universities that 
allow presubmission may need to have people judge which OTMs will be submitted to the regional level.  OTM Committee level access 
allows users to see OTMs submitted after their local due date.  OTM Committee members can submit OTMs under their account.  
During the grading period (dates between local due date and regional due date), the OTMs submitted for that month will automatically 
appear after logging into the system.<P>
<B>User</B> - This access is for those who want to keep track of OTMs submitted, be able to edit them, and also see when OTMs are due.<P>
</UL>

<H4><a name="ca15">How do I designate someone as a campus administrator or OTM committee member?</a></H4>
<UL>Log into the system and select <B>Admin Users</B>.  This will bring up all users for your university.  If they have 
just requested an account, it will automatically be set as an inactive user.  After finding their name, select edit.  Scroll down to 
access and select campus administrator or OTM committee member.  Make sure they are set to <I>Active</I> so that they may log in.
</UL>

<H4><a name="ca16">Can I submit OTMs under my campus administrator account?</a></H4>
<UL>Yes.  However, as a campus administrator, the system will not automatically split the OTMs you've submitted for the current month up from 
the rest of the OTMs that have been submitted by your entire university.
</UL>

<H4><a name="ca17">What is so great about having the system send out an e-mail of the regional winners?</a></H4>
<UL>By using the <B>Send E-mail</B> option, the system can automatically generate the body of the e-mail and query the database 
to find out which OTMs were selected as campus winners.  Furthermore, you have the option of having the system place 
a link under each recipient so that people may be directed to that OTM without having the search through the database.
</UL>

<H4><a name="ca18">How come some universities have different categories than others?</a></H4>
<UL>The system has been designed in such a way to give as much flexibility to each campus administrator as possible including allowing a 
campus to be able to submit OTMs for different combinations of categories.
</UL>

<H4><a name="ca19">There are no categories in the drop down menu to select when submitting an OTM.  What's wrong?</a></h4>
<UL>If the campus administrator has designated that your university is set up to submit all OTMs to the local level before submitting them on to 
the regional level, then the campus administrator did not select which OTMs can be submitted.  In the event that the campus submits directly to the regional 
level and there are not categories to submit for, the regional administrator has not designated which categories can be submitted.
</UL>

<H4><a name="ca20">When sending an e-mail through the OTM System to a listserv, it doesn't get sent.  What's wrong?</a></H4>
<UL>There is a possibility that your listserv is set up in a way to only allow people who are on the list to send to the list.  To fix 
this problem, add the e-mail address of otms@otms.nrhh.org to your listserv.  Make sure it is set to not *receive* e-mail because it 
will bounce back to the listserv administrator.
</UL>

<H4><a name="ca21">When can I forward OTMs on to the regional level?</a></H4>
<UL>OTMs can be forwarded on to the regional level after your local due date and before the regional due date.
</UL>

<H4><a name="ca22">The column to submit OTMs to the regional level vanished!  How can I submit them on?</a></H4>
<UL>The columns to submit OTMs on to the regional level appears when the current date is between the local due date and before the regional 
due date.  As of this time, there is no method to submit them on.
</UL>

<H4><a name="ca23">I changed my OTMs to campus winners, but the regional level can not see them. Why? </a></H4>
<UL>Changing the award of an OTM does not automatically mean that it will get promoted to the next level.  The only way for an OTM to be 
submitted on is through the Submission Column.
</UL>

<H4><a name="ca24">The system is not allowing me to submit a specific category on to the regional level. Why?  </a></H4>
<UL>The system will detect which OTM categories your regional level accepts.  If you are trying to submit an OTM on to the regional level 
that they do not accept, the system will not allow it.  If there are two categories in which only one gets forwarded on, change the cateogory to 
that of which the regional level accepts.  <I>Ie:</I>  If your regional level does not accept diversity OTMs, but you would like to submit your 
diversity OTM instead of the social OTM, then you'd need to change the diversity OTM to the social category and forward it on.  A campus may have 
an OTM that is campus winner of two categories.
</UL>

<H4><a name="cao">What other information should I know?</a></H4>
<UL><LI>When submitting an OTM on to the regional level, the system will automatically set it as a campus winner.
<LI>All system dates and times are Central Standard Time
</UL>

<? } if ($userLevel <=40) { ?>

<H4><a name="ua2">What are the benefits to creating an account?</A></H4>
<UL>There are numerous benefits to creating an account with the OTM System.  Just a few include:<BR>
<UL><LI>Having the ability to edit OTMs before they are due
<LI>The system will partially fill out the OTM form for you
<LI>Keep track of previously submitted OTMs
<LI>See when OTMs are due to your campus or regional level
</UL>
</UL>

<H4><a name="ua2">I submitted an OTM before I created an account.  Can I edit it before the deadline?</a></H4>
<UL>OTMs submitted without first loggin into an account can not be edited by a user.  The only way for a user to edit an OTM after it has been submitted 
is by first logging into the system to submit the OTM.</UL>


<H4><a name="ua3">How do I create an account?</a></H4>
<UL>To create an account, direct your browser to the <I><a href="http://otms.nrhh.org/form_create_account.php3">Create an Account</a></I> Page.  
Select the University that you currently attend and hit submit.  Then, fill out the next form.  When completed and after submitted, you will 
see a page saying that it will take approximately 24 hours for your account to be activated.  Every account submitted is automatically set to be 
inactive and an administrator will have to activate and validate your account request.  If you campus administrator has specified to send users 
an e-mail when their account is granted or denied, you will receive notification when it has been done.
</UL>

<H4><a name="ua4">How come when I search for an OTM, I don't see the ones for this month?</a></H4>
<UL>The dates that OTMs can be searched for are at the discretion of your regional administrator.  After logging into the system, check the dates to know 
when OTMs can be searched.  If you received confirmation that your OTM was submitted, then you can be rest assured that it has.  Just because you can not search 
for it doesn't mean that your campus and/or region will not be able to see it.
</UL>

<H4><a name="ua5">I can't edit previously submitted OTMs.  Why?</a></H4>
<UL>Only OTMs that have been submitted for the current month can be edited by a user.  Previous month submissions can not be edited.</UL>

<H4><a name="ua6">My account is inactive.  What does that mean?</a></H4>
<UL>When an account is inactive, you will not be able to log in and therefore will not be able to submit OTMs under that account.  If you are in 
need of submitting an OTM, you may submit one without logging into an account.

<? } ?>

</TD></TR>
</TABLE>

</BODY>


</HTML>
