<?
	$requiredUserLevel = array(1);
	$cfgProgDir = 'phpSecurePages/';
	include($cfgProgDir . "secure.php");

include("../config/dbinfo.inc.php");


?>

<HTML>
<HEAD>
<? echo $stylesheet; ?>
</HEAD>
<BODY class=BODY>
<?
echo "<h3>Database Statistics:</h3>\r\n";

echo "<TABLE class=stat-table><TR><TD>\r\n";


echo "<TABLE class=stat-table cellpadding='5'><TR><TD valign=top>\r\n";


echo "<B>Users breakdown:</B>\r\n";
echo "<UL><TABLE class=admin-table cellpadding='1'>";

$total = 0;

$admin_query="
	SELECT count( uid ) , active
	FROM users
	GROUP  BY active order by active
";
$admin_result=mysql_query($admin_query);
$admin_num=mysql_numrows($admin_result); 
$inactive_users=mysql_result($admin_result,1,0);
$active_users=mysql_result($admin_result,2,0);
$deactivated_users=mysql_result($admin_result,3,0);
$deleted_users=mysql_result($admin_result,4,0);
echo "<TR><TD>Total number of active users:</TD><TD align=right>$active_users</TD></TR>\r\n";
echo "<TR><TD>Total number of inactive users:</TD><TD align=right>$inactive_users</TD></TR>\r\n";
echo "<TR><TD>Total number of deactivated users:</TD><TD align=right>$deactivated_users</TD></TR>\r\n";
echo "<TR><TD>Total number of deleted users:</TD><TD align=right>$deleted_users</TD></TR>\r\n";
$total = $inactive_users + $active_users + $deactivated_users + $deleted_users;
echo "<TR><TD><B>Total Registered Users</B></TD><TD align=right>$total</TD></TR>\r\n";
echo "<TR><TD><BR></TD></TR>";
echo "<TR><TD><BR></TD></TR>";

$admin_query="
SELECT uid,start_date,active,flag_set_inactive from users where start_date>=subdate(NOW(),INTERVAL 1 MONTH) and active='0' and flag_set_inactive='0'";
$admin_result=mysql_query($admin_query);
$admin_num=mysql_numrows($admin_result);
echo "<TR><TD>New User Requests not activated in the last month:</TD><TD align=right>$admin_num</TD></TR>\r\n";
echo "<TR><TD><BR></TD><TD align=right></TD></TR>\r\n"; 

$admin_query="
	SELECT count( uid ) , userlevel
	FROM users
	where active=1
	GROUP  BY userlevel ORDER BY userlevel ASC
";
$admin_result=mysql_query($admin_query);
$admin_num=mysql_numrows($admin_result); 
$domain_admins=mysql_result($admin_result,0,0);
$national_admins=mysql_result($admin_result,1,0);
$regional_admins=mysql_result($admin_result,2,0);
$campus_admins=mysql_result($admin_result,3,0);
$campus_comm=mysql_result($admin_result,4,0);
$regular_users=mysql_result($admin_result,5,0);


echo "<TR><TD>Domain Admins:</TD><TD align=right>$domain_admins</TD></TR>\r\n";
echo "<TR><TD>National Admins:</TD><TD align=right>$national_admins</TD></TR>\r\n";
$admin_query="
	SELECT uid, reg_voting
	FROM users
	where nat_voting = 1
";
$admin_result=mysql_query($admin_query);
$admin_num=mysql_numrows($admin_result); 
echo "<TR><TD>National Committee:</TD><TD align=right>$admin_num</TD></TR>\r\n";
echo "<TR><TD>Regional Admins:</TD><TD align=right>$regional_admins</TD></TR>\r\n";
$admin_query="
	SELECT uid, reg_voting
	FROM users
	where reg_voting = 1
";
$admin_result=mysql_query($admin_query);
$admin_num=mysql_numrows($admin_result); 
echo "<TR><TD>Regional Committee:</TD><TD align=right>$admin_num</TD></TR>\r\n";
echo "<TR><TD>Campus Admins:</TD><TD align=right>$campus_admins</TD></TR>\r\n";
echo "<TR><TD>Campus Committee:</TD><TD align=right>$campus_comm</TD></TR>\r\n";
echo "<TR><TD>Regular Users:</TD><TD align=right>$regular_users</TD></TR>\r\n";
echo "</TABLE>\r\n";


echo "<P>";

$query="select userlevel as 'UserLevel', users.active as 'Active Status', count(*) as 'Number' from config_orgs LEFT JOIN users ON config_orgs.coid=users.coid group by userlevel,users.active";
$result=mysql_query($query);
$num_rows=mysql_numrows($result);
$num_fields=mysql_num_fields($result);

$align="right";
include("../show_table_std.h");

echo "</TD></TR></TABLE>";

echo "</TD></TR></TABLE>";
echo "</UL><P>";


echo "<B>OTM Category breakdown:</B>\r\n";
echo "<UL><TABLE class=admin-table cellpadding='2'>";

$co_query="SELECT * FROM `config_orgs` where level='regional' order by name ";
$co_result=mysql_query($co_query);
$co_num=mysql_numrows($co_result);

$query="select config_categories.name as 'Category Name', ";

for ($i=0; $i<$co_num; ++$i) {
	$coid=mysql_result($co_result,$i,"coid");
	$crname=mysql_result($co_result,$i,"name");
	$query = $query . " sum(if(config_orgs.coid=$coid,1,0)) as '$crname', ";
}

$query = $query . "count(*) as 'TOTAL'"; 
$query = $query . " from otms INNER JOIN config_orgs ON config_orgs.coid=otms.region INNER JOIN config_categories USING (ccid) group by otms.ccid order by config_categories.name";

$result=mysql_query($query);
$num_rows=mysql_numrows($result);
$num_fields=mysql_num_fields($result);

$align="right";
include("../show_table_std.h");
echo "</UL>\r\n";


echo "<B>Organization breakdown:</B>\r\n";
echo "<UL><TABLE class=admin-table cellpadding='2'>";
$cc_query="SELECT * FROM `config_orgs` where active=1";
$cc_result=mysql_query($cc_query);
$cc_num=mysql_numrows($cc_result); 
echo "<TR><TD>Active Orgs</TD><TD align=right>$cc_num</TD></TR>";

$cc_query="SELECT * FROM `config_orgs` where active=0";
$cc_result=mysql_query($cc_query);
$cc_num=mysql_numrows($cc_result); 
echo "<TR><TD>Inactve Orgs</TD><TD align=right>$cc_num</TD></TR>";

$cc_query="SELECT * FROM `config_orgs` where allow_suborgs=1";
$cc_result=mysql_query($cc_query);
$cc_num=mysql_numrows($cc_result); 
echo "<TR><TD>Orgs with Suborgs</TD><TD align=right>$cc_num</TD></TR>";

$cc_query="SELECT * FROM `config_orgs` where allow_presubmit=1";
$cc_result=mysql_query($cc_query);
$cc_num=mysql_numrows($cc_result); 
echo "<TR><TD>Orgs Presubmitting</TD><TD align=right>$cc_num</TD></TR>";

$cc_query="SELECT distinct config_orgs.coid FROM `config_orgs` LEFT JOIN users ON config_orgs.coid=users.coid where userlevel=30";
$cc_result=mysql_query($cc_query);
$cc_num=mysql_numrows($cc_result);
echo "<TR><TD>Orgs with Campus Admins</TD><TD align=right>$cc_num</TD></TR>";

$cc_query="SELECT * FROM `config_orgs` where account_request_email_admin=1";
$cc_result=mysql_query($cc_query);
$cc_num=mysql_numrows($cc_result); 
echo "<TR><TD>Orgs e-mailing admin when new account is created</TD><TD align=right>$cc_num</TD></TR>";

$cc_query="SELECT * FROM `config_orgs` where nrhh=1";
$cc_result=mysql_query($cc_query);
$cc_num=mysql_numrows($cc_result); 
echo "<TR><TD>Orgs w/ NRHH Chapters</TD><TD align=right>$cc_num</TD></TR>";

$cc_query="SELECT * FROM `config_orgs` where nrhhweb=1";
$cc_result=mysql_query($cc_query);
$cc_num=mysql_numrows($cc_result); 
echo "<TR><TD>Orgs w/ NRHH Website</TD><TD align=right>$cc_num</TD></TR>";

$cc_query="SELECT distinct config_orgs.coid FROM config_dates LEFT JOIN config_orgs on config_dates.coid=config_orgs.coid left join config_dates d2 on config_dates.month=d2.month and d2.coid=config_orgs.parent_org_id
where                (CAST(concat(config_dates.month_due,config_dates.date_due,config_dates.hour_due,config_dates.minute_due,'00') as SIGNED) - (CAST(concat(d2.month_due,d2.date_due,d2.hour_due,d2.minute_due,'00') AS SIGNED))) < 0100000000 and
                (CAST(concat(config_dates.month_due,config_dates.date_due,config_dates.hour_due,config_dates.minute_due,'00') as SIGNED) - (CAST(concat(d2.month_due,d2.date_due,d2.hour_due,d2.minute_due,'00') AS SIGNED))) >= 0";
$cc_result=mysql_query($cc_query);
$cc_num=mysql_numrows($cc_result);               
echo "<TR><TD>Orgs w/ due dates past regional deadline</TD><TD align=right>$cc_num</TD></TR>";


echo "</TABLE></UL>";
?>

</TD></TR></TABLE>

</BODY>


</HTML>
