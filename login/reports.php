<?
	$requiredUserLevel = array(1);
	$cfgProgDir = 'phpSecurePages/';
	include($cfgProgDir . "secure.php");

include("../config/dbinfo.inc.php");

if ($q==1) { $query="SELECT lname as 'Last Name', fname as 'First Name', sex as 'Gender', if(users.birthday is NULL or users.birthday='0000-00-00','',YEAR(CURRENT_DATE)-YEAR(users.birthday)-IF(DAYOFYEAR(CURRENT_DATE)>DAYOFYEAR(users.birthday),0,1)) AS 'Age', address as 'Address', city as 'City', state as 'State', zip as 'Zip', email as 'E-mail', phone_home as 'Home Phone', phone_mob as 'Mobile Phone', phone_work as 'Work Phone' FROM users where member_status = 'M' AND active='1' ORDER BY lname"; }
elseif ($q==2) { $query="SELECT lname as 'Last Name', fname as 'First Name', sex as 'Gender', if(users.birthday is NULL or users.birthday='0000-00-00','',YEAR(CURRENT_DATE)-YEAR(users.birthday)-IF(DAYOFYEAR(CURRENT_DATE)>DAYOFYEAR(users.birthday),0,1)) AS 'Age', address as 'Address', city as 'City', state as 'State', zip as 'Zip', email as 'E-mail', phone_home as 'Home Phone', phone_mob as 'Mobile Phone', phone_work as 'Work Phone' FROM users WHERE member_status = 'V' AND active='1' ORDER BY lname"; }
elseif ($q==3) { $query="SELECT lname as 'Last Name', fname as 'First Name', address as 'Address', city as 'City', state as 'State', zip as 'Zip', email as 'E-mail', phone_home as 'Home Phone', phone_mob as 'Mobile Phone', phone_work as 'Work Phone' FROM users WHERE member_status = 'M' AND name_tag = '0' AND active='1' ORDER BY lname"; }
elseif ($q==4) { $query="SELECT offices.office as 'Office', lname as 'Last Name', fname as 'First Name', address as 'Address', city as 'City', state as 'State', zip as 'Zip', email as 'E-mail', phone_home as 'Home Phone', phone_mob as 'Mobile Phone', phone_work as 'Work Phone' FROM users LEFT JOIN offices on users.oid=offices.oid WHERE users.oid <> '0'"; }
elseif ($q==5) { $query="SELECT name as 'Small Group', lname as 'Last Name', fname as 'First Name', email as 'E-mail', phone_home as 'Home Phone' FROM smallgroups LEFT JOIN users ON users.sgid=smallgroups.sgid ORDER BY smallgroups.name, users.lname"; }
elseif ($q==6) { $query="SELECT concat(fname,' ',lname) as 'Name', email as 'Email', sex as 'Sex' FROM users where email_pr='1' order by users.lname"; }
elseif ($q==7) { $query="SELECT lname as 'Last Name', fname as 'First Name', sex as 'Gender', member_status as 'Status', if(users.birthday is NULL or users.birthday='0000-00-00','',YEAR(CURRENT_DATE)-YEAR(users.birthday)-IF(DAYOFYEAR(CURRENT_DATE)>DAYOFYEAR(users.birthday),0,1)) AS 'Age', address as 'Address', city as 'City', state as 'State', zip as 'Zip', email as 'E-mail', phone_home as 'Home Phone', phone_mob as 'Mobile Phone', phone_work as 'Work Phone' FROM users where active='1' ORDER BY lname"; }
elseif ($q==8) { $query="SELECT * FROM form_submissions order by submitted"; }
else {
$query= "
	SELECT logs_otms.otmid, 
	CONCAT('LINK:/otm_',config_categories.category_type,'.php3?otmid=',otms.oid,'&vid=',otms.vid) as 'Link1', 
	count(*) as 'Viewed', 
	CONCAT('LINK:/login/admin_otmview_stats.php?oid=',otms.oid) as 'Link2',
	otms.nominee AS 'Nominee', 
	config_orgs.name as 'University' 
	from logs_otms 
	left join otms on logs_otms.otmid=otms.oid 
	LEFT JOIN config_categories ON otms.ccid=config_categories.ccid
	left join config_orgs on otms.coid=config_orgs.coid 
	group by logs_otms.otmid 
	order by count(*) DESC
	LIMIT 300";
}

if ($query) {
	$result=mysql_query($query);
	$num_rows=mysql_numrows($result); 
	$num_fields=mysql_num_fields($result); 

	if ($echosql == 1) { echo $query; }

	$query_slash = addslashes($query);
	$query_enc = urlencode($query_slash);
?>
<HTML>
<HEAD>
<? echo "$stylesheet"; ?>
</HEAD>
<BODY class=BODY>
<H2>REPORT</H2>

<?
echo "<a href='excelreport.php?query=$query_enc'>Export to Excel</a><P>\r\n";

echo "<TABLE class=admin-table cellpadding='3'>"; 

echo "<TR bgcolor='#D3DCE3'>"; 

$i=0;
while ($i < $num_fields) {
$fn=mysql_field_name($result,$i);
echo "<TH>$fn</TH>";
++$i;
}
echo "</TR>\r\n";
?>

<?
$rowcolor = '#dddddd';

if ($num_rows > 0) {
   for ($j = 0; $j<$num_rows; $j++) {

echo "<TR bgcolor='$rowcolor'>\r\n";

       for ($k = 0; $k<$num_fields; $k++) {
           echo "<td align=center>" . mysql_result($result,$j, $k) . "</td>";
       }

if ( $rowcolor == '#dddddd' ) { $rowcolor = '#cccccc'; }
elseif ( $rowcolor == '#cccccc' ) { $rowcolor = '#dddddd'; }
echo "</TR>\r\n";

   }
}


?>
</TABLE>

<?
echo "$num_rows Records";
?>

</BODY>
</HTML>
<?
}
?>
