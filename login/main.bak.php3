<?
	$cfgProgDir = 'phpSecurePages/';
	include($cfgProgDir . "secure.php");

include("../config/dbinfo.inc.php");

    $timer = new timer; 
    $timer->start(); 

if ($order) {  } else { $order="config_categories.name,co2.name,config_orgs.name"; }

$web_query_string=$_SERVER['QUERY_STRING'];

$c=0; // Variable for highlighting
$sc_submit_region=0;
$sc_submit_national=0;
$sc_submit_global=0;
$sc_vote_region=0;
$sc_vote_national=0;
$form=1;


function columnLink ($tvalue,$old,$new,$old_llimit,$web_query_string) {
	if ($tvalue == "Month-Year") {
	echo "<TH COLSPAN=2>";
	} else {
        echo "<TH>";
	}	
		$new_order=str_replace("order_field","order",$new);
		echo "<a href=\"$PHP_SELF?$new_order\">$tvalue</a>";
	echo "</TH>";
        }



if ($command == "delete") {
	include "otm_delete.h";
}

if ($command == "forward_remove") {
	include "otm_forward_remove.h";
}

if ($command == "vote_remove") {
	include "otm_vote_remove.h";
}

include ($base_folder . "/queries/queries_user.h");


if ($userLevel == 1) {
} else {



// username lookup connection
	include ($base_folder . "/queries/queries_org.h");

	if ($org_level == "regional") {
		$pcoid = $coid;
	}
}

include ($base_folder . "/queries/queries_user_regcom.h");
if (($allow_presubmit != 1) && ($school_region_id > 1)) { // added parent org info 6/9/03
	$coid = $school_region_id;
}

include ($base_folder . "/queries/queries_user_natcom.h");




?>

<HTML>
<HEAD>
<? echo $stylesheet; ?>

<?
// if ($userLevel == 1) {
echo "<script src='functions.js' type='text/javascript' language='javascript'></script>";
//}
?>

<SCRIPT TYPE="text/javascript">
<!--
function popup(mylink, windowname)
{
if (! window.focus)return true;
var href;
if (typeof(mylink) == 'string')
href=mylink;
else
href=mylink.href;
window.open(href, windowname, 'width=500,height=350,scrollbars=yes');
return false;
}
//-->
</SCRIPT>

</HEAD>
<BODY class=body>


<!--
<table><tr><TD>
<div class="site-info">
<h3>Server Maintenance Notice</h3>
    <p>The OTM Database System will be offline starting Friday, March 18th, 2011 at 11 PM Central Time in order to perform system maintenance.   
The system is scheduled to be restored by 2 PM on Saturday, March 19th.  We apologize for the inconvience.</p>
  </div>
</TD></TR></TABLE>
-->
<?
checkMaintenance();

/*
echo $PHP_SELF;
echo "<BR>";
echo $_SERVER['PHP_SELF'];
echo $cfg_site_home;
echo "<BR>";

echo "today date: $today_date_value";
echo "<BR>";
echo "today regional date due: $today_regional_date_due";
echo "<BR>";
echo "today local date due: $today_local_date_due";
echo "<BR>";
echo "org date due: $org_date_due";
echo "<BR>";
echo $regional_query;
echo "<BR>";
echo "lookup month: $lookup_month<BR>";
echo "userlevel: $userLevel<BR>";
echo "parent org: $school_region_id<BR>";

*/
echo $message;

/*
echo "<BR>";
echo "<H1>Welcome ";
echo "$user_fname ";
echo "$user_lname";
echo "!</H1>\r\n";
echo "<BR>";

*/

/* TURNED OFF
if ($userLevel == 30) {
?>      
<H4><font color="#FF0000">Will your campus submit OTMs into the database before the regional deadline and then determine who will advance to the regional level?</FONT></H4>
<UL><font size=2>If your campus submits OTMs into the database and then determine which OTMs will advance to the regional level, this settings needs to be set to <B>YES</B>.  If your campus first decides
which OTMs will be submitted to the regional level, and then writes it (ie: your campus only submits one OTM per category), then this setting should be set to <B>NO</B>.  ***PLEASE MAKE SURE YOU
UNDERSTAND THIS QUESTION AND CONFIGURE YOUR CAMPUS SETTINGS APPROPRIATELY***  Not setting this correctly can cause OTMs to not be submitted to the regional level. Click on <B>Campus Info</B> on the left side 
to check the settings that are configured for your university.  If you are not sure what setting your university should have, please contact Andrew Bell @ <a href="mailto:akb@andrewbell.info">akb@andrewbell.info</a>
</UL>   

<?
}
*/

?>
<P>

<?
if ($userLevel == 1) {  //DOMAIN ADMINISTRATOR
	if ($today_month == 1) {
 		$lookup_month = 12;
 		$show_year = $today_year - 1;
	} else {
		$lookup_month = $today_month - 1;
		$show_year=$today_year;
	}

	$lumid = $lookup_month;
	include ($base_folder . "/queries/queries_lu_month.h");
	echo "<h3>OTMS submitted for $month_name $show_year</h3>\r\n";
	$otms_query = $otms_query_std . "WHERE otms.year_month='$show_year-$lookup_month-00' ORDER BY $order limit 100";
	$otms_result=mysql_query($otms_query);
	$otms_num=mysql_numrows($otms_result); 

	//COLUMNS TO SHOW ADMIN
	$config_columns_query="SELECT * FROM config_columns WHERE ccid='u1'";
	$config_columns_result=mysql_query($config_columns_query);
	$show_column=mysql_fetch_array($config_columns_result);
	echo "<B>Total OTMs submitted for the month: $otms_num</B><p>\r\n";

}

if (($userLevel == 10) || (($user_nat_voting == 1) && ($beg_nat < $today_date_value) && ($today_date_value < $end_nat))) { // SPECIAL THINGS NATIONAL ADMINISTRATORS

	$dd_query="SELECT *, concat(month_due,date_due,hour_due,minute_due) as tes FROM config_dates where coid='1' and concat(month_due,date_due,hour_due,minute_due) < '$today_date_value' ORDER BY `tes` DESC LIMIT 1";
	$dd_result=mysql_query($dd_query);
	$dd_num=mysql_numrows($dd_result);

	$lookup_month=mysql_result($dd_result,0,"month");
	$date_due=mysql_result($dd_result,0,"date_due");
	$month_due=mysql_result($dd_result,0,"month_due");
	$hour_due=mysql_result($dd_result,0,"hour_due");

#echo $dd_query;

	$lumid = $lookup_month;
	include ($base_folder . "/queries/queries_lu_month.h");

	if ((intval($month_due) < $lookup_month)) {
		$show_year = $today_year - 1;
	} else {
		$show_year = $today_year;
	}

#echo "month due: " . $month_due;
#echo $lookup_month;

	$otms_query = $otms_query_std . "WHERE otms.year_month='$show_year-$lookup_month-00' and otms.award_level=3 ORDER BY $order";
	$otms_result=mysql_query($otms_query);
	$otms_num=mysql_numrows($otms_result); 

	if ($userLevel == 10) {
		$config_columns_query="SELECT * FROM config_columns WHERE ccid='u10gp'";
		echo "<font size=4><B>OTMS submitted for $month_name $show_year</B></FONT>&nbsp;&nbsp;&nbsp;&nbsp;($otms_num Total OTMs)\r\n";
		echo "<BR><BR>";
	} else {
		$show_previous_submissions = 0;
		$config_columns_query="SELECT * FROM config_columns WHERE ccid='u15gp'";
		echo "<font size=4><B>OTMS to judge for $month_name $show_year - National Level</B></FONT>\r\n";
		echo "<BR><BR>";
	}
	$config_columns_result=mysql_query($config_columns_query);
	$show_column=mysql_fetch_array($config_columns_result);

	//**********************************

	$category_query="SELECT * FROM config_orgs WHERE coid=$coid";

	$category_result=mysql_query($category_query);
	$category_num=mysql_numrows($category_result); 
	$school_categories=mysql_result($category_result,0,"categories");
	$ar_cat = split(",", $school_categories);

	$categorye_query="SELECT * FROM config_categories order by name";
	$categorye_result=mysql_query($categorye_query);
	$categorye_num=mysql_numrows($categorye_result); 

	$b=0;
	while ($b < $categorye_num) {
		$ccid=mysql_result($categorye_result,$b,"ccid");
		$cat_name=mysql_result($categorye_result,$b,"name");

		if ((((in_array (intval($ccid), $ar_cat))) && ($userLevel == 10)) || ((in_array (intval($ccid), $ar_user_nat_voting_cat)))) {
			$otms_query = $otms_query_std . "WHERE otms.year_month='$show_year-$lookup_month-00' and otms.award_level=3 AND otms.ccid=$ccid ORDER BY $order";
//			$otms_query="SELECT * FROM otms LEFT JOIN config_categories ON otms.ccid=config_categories.ccid LEFT JOIN config_orgs ON otms.coid=config_orgs.coid WHERE $judge_criteria otms.year_month='$show_year-$rlookup_month-00' and otms.region=$user_coid and (otms.award_level=2 or otms.award_level=3) AND otms.ccid=$ccid ORDER BY config_orgs.name";
			$otms_result=mysql_query($otms_query);
			$otms_num=mysql_numrows($otms_result); 
				if ($otms_num != 0) {
					echo "<font size=3><B>$cat_name</B></font>&nbsp;&nbsp;&nbsp;&nbsp;($otms_num OTMs)<BR><BR>";
					include "../show_table.h";

				}
		}
		++$b;
	}
	$otms_num=0;
}

// ***************************************************
if ((($userLevel == 20) || (($userLevel == 30) && ($allow_presubmit != 1)) || (($userLevel == 35) && ($allow_presubmit != 1)) || (($userLevel == 40) && ($allow_presubmit != 1))) || (($user_reg_voting == 1) && ($beg < $today_date_value) && ($today_date_value < $end))) { //REGIONAL OTM ADMIN 

	if (($user_reg_voting == 1) && ($beg < $today_date_value) && ($today_date_value < $end)) { // PROMOTE USER HIGHER
		$coid = $school_region_id;
		$user_coid = $school_region_id;
		$school_region_id = 1;
	}

	//LOCAL QUERY+
	$dd_query="SELECT *, concat(month_due,date_due,hour_due,minute_due) as tes FROM config_dates where coid='$coid' and concat(month_due,date_due,hour_due,minute_due) > '$today_date_value' ORDER BY `tes` ASC LIMIT 1";
	$dd_result=mysql_query($dd_query);
	$dd1_num=mysql_numrows($dd_result);

	if ($dd1_num != 1) {
		$dd_query="SELECT *, concat(month_due,date_due,hour_due,minute_due) as tes FROM config_dates where coid='$coid' and concat(month_due,date_due,hour_due,minute_due) < '$today_date_value' ORDER BY `tes` ASC LIMIT 1";
		$dd_result=mysql_query($dd_query);
		$dd_num=mysql_numrows($dd_result);
	}

	//PARENT QUERY
	$pcoid = $school_region_id;
	$rdd_query="SELECT *, concat(month_due,date_due,hour_due,minute_due) as tes FROM config_dates where coid='$pcoid' and concat(month_due,date_due,hour_due,minute_due) > '$today_date_value' ORDER BY `tes` ASC LIMIT 1";
	$rdd_result=mysql_query($rdd_query);
	$rdd1_num=mysql_numrows($rdd_result);

	if ($rdd1_num != 1) {
		$rdd_query="SELECT *, concat(month_due,date_due,hour_due,minute_due) as tes FROM config_dates where coid='$pcoid' and concat(month_due,date_due,hour_due,minute_due) < '$today_date_value' ORDER BY `tes` ASC LIMIT 1";
		$rdd_result=mysql_query($rdd_query);
		$rdd_num=mysql_numrows($rdd_result);
	}

	$lookup_month=mysql_result($dd_result,0,"month");
	$date_due=mysql_result($dd_result,0,"date_due");
	$month_due=mysql_result($dd_result,0,"month_due");
	$hour_due=mysql_result($dd_result,0,"hour_due");

	$local = $month_due . $date_due . $hour_due;
	
	$rlookup_month=mysql_result($rdd_result,0,"month");
	$rdate_due=mysql_result($rdd_result,0,"date_due");
	$rmonth_due=mysql_result($rdd_result,0,"month_due");
	$rhour_due=mysql_result($rdd_result,0,"hour_due");

	$parent = $rmonth_due . $rdate_due . $rhour_due;
	/*
	echo "Local: $local<BR>";
	echo "Parent: $parent<BR>";
	echo "Month Due: $month_due<BR>";
	echo "RMonth Due: $rmonth_due<BR>";
	echo $parent-$local;
	*/

	//if (((($local > $parent) && ($month_due ==$rmonth_due)) && ($userLevel != 30)) || ((($local > $parent) && ($month_due ==$rmonth_due)) && ($userLevel == 30) && (($user_reg_voting == 1) && ($beg < $today_date_value) && ($today_date_value < $end)))) { //GRADING PERIOD  QUESTION THE ACCURATENESS OF SECOND CHECK.  
	//if (((($local > $parent) && ($month_due >$rmonth_due)) && ($userLevel != 30)) || ((($local > $parent) && ($month_due ==$rmonth_due)) && ($userLevel == 30) && (($user_reg_voting == 1) && ($beg < $today_date_value) && ($today_date_value < $end)))) { //GRADING PERIOD  QUESTION THE ACCURATENESS OF SECOND CHECK.  
	if ((($today_month . $today_date . $today_hour  < $parent) & ($month_due !=$rmonth_due) && ($userLevel !=30)) || (($today_month . $today_date . $today_hour  < $parent) & ($month_due !=$rmonth_due) && ($userLevel == 30) && ($user_reg_voting ==1))) {
	//if (($local > $parent) && (($parent-$local) < 60000)) {

		if (($dd1_num != 1) && ($dd1_num != 1)) { //QUESTION THE ACCURATENESS OF THIS STATEMENT.
			$show_year = $today_year;
		} elseif ((intval($rmonth_due) < $rlookup_month)) {
			$show_year = $today_year - 1;
		} else {
			$show_year = $today_year;
		}
		$lumid = $rlookup_month;
		include ($base_folder . "/queries/queries_lu_month.h");
	
		if (($otm_blurb_local == 1) && (($userLevel == 20) || ($userLevel == 30))) {
			echo "<a href='otm_blurbs.php?coid=$coid&month=$rlookup_month&year=$show_year'>OTM Blurb Sheet</a>";
		}
	
		if (($user_reg_voting == 1) && ($beg < $today_date_value) && ($today_date_value < $end)) { 
			$config_columns_query="SELECT * FROM config_columns WHERE ccid='u25gp'";
			echo "<h3>OTMs to judge for the month of $month_name $show_year - Regional Level</h3>\r\n";
		} else {
			$config_columns_query="SELECT * FROM config_columns WHERE ccid='u20gp'";
			echo "<h3>Campus Winners submitted for the month of $month_name $show_year</h3>\r\n";
		} 
	
		$config_columns_result=mysql_query($config_columns_query);
		$show_column=mysql_fetch_array($config_columns_result);
	
		$category_query="SELECT * FROM config_orgs WHERE coid=$coid";
		$category_result=mysql_query($category_query);
		$category_num=mysql_numrows($category_result); 
		$school_categories=mysql_result($category_result,0,"categories");
		$ar_cat = split(",", $school_categories);
	
		$categorye_query="SELECT * FROM config_categories order by name";
		$categorye_result=mysql_query($categorye_query);
		$categorye_num=mysql_numrows($categorye_result); 

		$b=0;
		while ($b < $categorye_num) {
			$ccid=mysql_result($categorye_result,$b,"ccid");
			$cat_name=mysql_result($categorye_result,$b,"name");
	
			// this query include $judge_criteria -- all regional admins need to not be on the regional committee or they do not get to view the OTMs
			if ((((in_array (intval($ccid), $ar_cat))) && ($userLevel == 20)) || ((in_array (intval($ccid), $ar_user_reg_voting_cat)))) { // Generates errors -- hidden in production
				$otms_query = $otms_query_std . "WHERE otms.year_month='$show_year-$rlookup_month-00' and otms.region=$user_coid and (otms.award_level=2 or otms.award_level=3) AND otms.ccid=$ccid ORDER BY $order";
				$otms_result=mysql_query($otms_query);
				$otms_num=mysql_numrows($otms_result); 
				if ($otms_num != 0) {
					echo "<font size=3><B>$cat_name</B></font>&nbsp;&nbsp;&nbsp;&nbsp;($otms_num OTMs)<BR><BR>";
					include "../show_table.h";
				}
			}
			++$b;
		}
		$otms_num=0;
	
		if ($userLevel == 20) { // SHOW OTMS NOT PROMOTED
			$config_columns_query="SELECT * FROM config_columns WHERE ccid='u20'";
			$config_columns_result=mysql_query($config_columns_query);
			$show_column=mysql_fetch_array($config_columns_result);
			echo "<h3>OTMs NOT promoted to Regional Level for the month of $month_name $show_year</h3>\r\n";
			$otms_query = $otms_query_std . "WHERE otms.year_month='$show_year-$rlookup_month-00' and otms.region=$user_coid and (otms.award_level<2) ORDER BY $order";
			$otms_result=mysql_query($otms_query);
			$otms_num=mysql_numrows($otms_result); 
			if ($otms_num != 0) {
				include "../show_table.h";
			}
		} else { }
	$otms_num=0;
	//*******************************
	} elseif (($userLevel == 20) || ($userLevel == 30)) {  
		if ($dd1_num != 1) {
			$show_year = $today_year;
		} elseif ((intval($month_due) < $lookup_month) && ($dd1_num == 1)) {
			$show_year = $today_year - 1;
		} else {
			$show_year = $today_year;
		}

		if ($userLevel == 30) {
			$show_previous_submissions = 1;
			$otms_query = $otms_query_std . "WHERE otms.year_month='$show_year-$lookup_month-00' and otms.coid=$user_coid ORDER BY $order";
		} else {
			$otms_query = $otms_query_std . "WHERE otms.year_month='$show_year-$lookup_month-00' and otms.region=$user_coid ORDER BY $order";
		}
		$lumid = $lookup_month;
		include ($base_folder . "/queries/queries_lu_month.h");
		echo "<h3>All OTMS submitted for the month of $month_name $show_year - Regional Level</h3>\r\n";

		$otms_result=mysql_query($otms_query);
		$otms_num=mysql_numrows($otms_result); 
		$config_columns_query="SELECT * FROM config_columns WHERE ccid='u$userLevel'";
		$config_columns_result=mysql_query($config_columns_query);
		$show_column=mysql_fetch_array($config_columns_result);
		echo "<B>Total OTMs submitted for the month: $otms_num - Regional Level</B><p>\r\n";

		} elseif ((($userLevel == 35) || ($userLevel == 40)) && ($show_previous_submissions !=0)) {
			$show_previous_submissions = 1;
			if ($dd1_num != 1) {
				$show_year = $today_year;
			} elseif ((intval($month_due) < $lookup_month) && ($dd1_num == 1)) {
				$show_year = $today_year - 1;
			} else {
				$show_year = $today_year;
			}

		$lumid = $lookup_month;
		include ($base_folder . "/queries/queries_lu_month.h");

		echo "<h3>Your OTMs submitted for $month_name $show_year</h3>\r\n";
		$otms_query = $otms_query_std . "WHERE `uid` = $ID AND `year_month` = '$show_year-$lookup_month-00' ";

		// COLUMNS TO SHOW EVERYONE ELSE
		$otms_result=mysql_query($otms_query);
		$otms_num=mysql_numrows($otms_result); 
		$config_columns_query="SELECT * FROM config_columns WHERE ccid='ucm'";
		$config_columns_result=mysql_query($config_columns_query);
		$show_column=mysql_fetch_array($config_columns_result);
		}
	}

if ((($userLevel == 30) && ($allow_presubmit == 1)) || (($userLevel == 35) && ($allow_presubmit == 1)) || (($userLevel == 40) && ($allow_presubmit == 1))) { //REMOVED CODE

                $coid = $user_coid_s;
                $user_coid = $user_coid_s;
                $school_region_id = $school_region_id_s;


	//LOCAL QUERY
	$dd_query="SELECT *, concat(month_due,date_due,hour_due,minute_due) as tes FROM config_dates where coid='$coid' and concat(month_due,date_due,hour_due,minute_due) > '$today_date_value' ORDER BY `tes` ASC LIMIT 1";
	$dd_result=mysql_query($dd_query);
	$dd1_num=mysql_numrows($dd_result);

	if ($dd1_num != 1) {
		$dd_query="SELECT *, concat(month_due,date_due,hour_due,minute_due) as tes FROM config_dates where coid='$coid' and concat(month_due,date_due,hour_due,minute_due) < '$today_date_value' ORDER BY `tes` ASC LIMIT 1";
		$dd_result=mysql_query($dd_query);
		$dd_num=mysql_numrows($dd_result);
	}
	//PARENT QUERY
	$pcoid = $school_region_id;
	$rdd_query="SELECT *, concat(month_due,date_due,hour_due,minute_due) as tes FROM config_dates where coid='$pcoid' and concat(month_due,date_due,hour_due,minute_due) > '$today_date_value' ORDER BY `tes` ASC LIMIT 1";
	$rdd_result=mysql_query($rdd_query);
	$rdd1_num=mysql_numrows($rdd_result);

	if ($rdd1_num != 1) {
		$rdd_query="SELECT *, concat(month_due,date_due,hour_due,minute_due) as tes FROM config_dates where coid='$pcoid' and concat(month_due,date_due,hour_due,minute_due) < '$today_date_value' ORDER BY `tes` ASC LIMIT 1";
		$rdd_result=mysql_query($rdd_query);
		$rdd_num=mysql_numrows($rdd_result);
	}

	$lookup_month=mysql_result($dd_result,0,"month");
	$date_due=mysql_result($dd_result,0,"date_due");
	$month_due=mysql_result($dd_result,0,"month_due");
	$hour_due=mysql_result($dd_result,0,"hour_due");
	$local = $month_due . $date_due . $hour_due;

	$rlookup_month=mysql_result($rdd_result,0,"month");
	$rdate_due=mysql_result($rdd_result,0,"date_due");
	$rmonth_due=mysql_result($rdd_result,0,"month_due");
	$rhour_due=mysql_result($rdd_result,0,"hour_due");

	$parent = $rmonth_due . $rdate_due . $rhour_due;

	if (((($local > $parent) && ($month_due !=$rmonth_due)) || (($local < $parent) && ($month_due !=$rmonth_due))) && (($userLevel == 30) || ($userLevel == 35))) { //GRADING PERIOD  QUESTION THE ACCURATENESS OF SECOND CHECK.  

		if (($dd1_num != 1) && ($dd1_num != 1)) { //QUESTION THE ACCURATENESS OF THIS STATEMENT.	
			$show_year = $today_year;
		} elseif ((intval($rmonth_due) < $rlookup_month)) {
			$show_year = $today_year - 1;
		} else {
			$show_year = $today_year;
		}

		$otms_query = $otms_query_std . "WHERE otms.year_month='$show_year-$rlookup_month-00' and otms.coid=$user_coid ORDER BY $order";
		$lumid = $rlookup_month;
		include ($base_folder . "/queries/queries_lu_month.h");

		if (($otm_blurb_local == 1) && ($userLevel == 30)) {
			echo "<a href='otm_blurbs.php?coid=$coid&month=$rlookup_month&year=$show_year'>OTM Blurb Sheet</a>";
		}

		echo "<h3>$conf_org_name OTMS submitted for $month_name $show_year</h3>\r\n";

		//COLUMNS TO SHOW COMMITTEE MEMBERS DURING GRADING PERIOD

		$config_columns_query="SELECT * FROM config_columns WHERE ccid='u" . $userLevel . "gp'";
		$config_columns_result=mysql_query($config_columns_query);
		$show_column=mysql_fetch_array($config_columns_result);
		echo "<B>Total OTMs submitted for the month: $otms_num - Campus Level</B><p>\r\n";

		$category_query="SELECT * FROM config_orgs WHERE coid=$coid";
		$category_result=mysql_query($category_query);
		$category_num=mysql_numrows($category_result); 
		$school_categories=mysql_result($category_result,0,"categories");
		$ar_cat = split(",", $school_categories);

		$categorye_query="SELECT * FROM config_categories order by name";
		$categorye_result=mysql_query($categorye_query);
		$categorye_num=mysql_numrows($categorye_result); 

		$b=0;
		while ($b < $categorye_num) {
			$ccid=mysql_result($categorye_result,$b,"ccid");
			$cat_name=mysql_result($categorye_result,$b,"name");

			// this query include $judge_criteria -- all regional admins need to not be on the regional committee or they do not get to view the OTMs
			// if ((((in_array ($ccid, $ar_cat))) && ($userLevel == 30)) || ((in_array ($ccid, $ar_user_reg_voting_cat)))) { // Generates errors -- hidden in production
			if ((((in_array (intval($ccid), $ar_cat))))) { // Generates errors -- hidden in production
				// $otms_query = $otms_query_std . "WHERE $judge_criteria otms.year_month='$show_year-$rlookup_month-00' and otms.region=$user_coid and (otms.award_level=2 or otms.award_level=3) AND otms.ccid=$ccid ORDER BY $order";
				$otms_query = $otms_query_std . "WHERE otms.year_month='$show_year-$rlookup_month-00' and otms.coid=$user_coid AND otms.ccid=$ccid ORDER BY $order";
				$otms_result=mysql_query($otms_query);
				$otms_num=mysql_numrows($otms_result); 
				if ($otms_num != 0) {
					echo "<font size=3><B>$cat_name</B></font>&nbsp;&nbsp;&nbsp;&nbsp;($otms_num OTM";
					if ($otms_num > 1) { echo "s"; }
					echo ")<BR><BR>";
					include "../show_table.h";
				}
			}
		++$b;
		}
	$otms_num=0;

	} elseif ($userLevel == 30) {
		$show_previous_submissions = 1;
	if ($dd1_num != 1) {
		$show_year = $today_year;
	} elseif ((intval($month_due) < $lookup_month) && ($dd1_num == 1)) {
		$show_year = $today_year - 1;
	} else {
		$show_year = $today_year;
	}

	$otms_query = $otms_query_std . "WHERE otms.year_month='$show_year-$lookup_month-00' and otms.coid=$user_coid ORDER BY $order";
	$lumid = $lookup_month;
	include ($base_folder . "/queries/queries_lu_month.h");
	echo "<h3>All OTMS submitted for the month of $month_name $show_year</h3>\r\n";

	$otms_result=mysql_query($otms_query);
	$otms_num=mysql_numrows($otms_result); 
	$config_columns_query="SELECT * FROM config_columns WHERE ccid='u30'";
	$config_columns_result=mysql_query($config_columns_query);
	$show_column=mysql_fetch_array($config_columns_result);
	echo "<B>Total OTMs submitted for the month: $otms_num</B><p>\r\n";

}  elseif (($userLevel == 35) || ($userLevel == 40)) {
	if (!$show_previous_submissions) { $show_previous_submissions = 1; }
if ($dd1_num != 1) {
	$show_year = $today_year;
} elseif ((intval($month_due) < $lookup_month) && ($dd1_num == 1)) {
	$show_year = $today_year - 1;
} else {
	$show_year = $today_year;
}

$lumid = $lookup_month;
include ($base_folder . "/queries/queries_lu_month.h");

echo "<h3>Your OTMs submitted for $month_name $show_year</h3>\r\n";
$otms_query = $otms_query_std . "WHERE `uid` = $ID AND `year_month` = '$show_year-$lookup_month-00' ";

// COLUMNS TO SHOW EVERYONE ELSE
$otms_result=mysql_query($otms_query);
$otms_num=mysql_numrows($otms_result); 
// determining columns to show
$config_columns_query="SELECT * FROM config_columns WHERE ccid='ucm'";
$config_columns_result=mysql_query($config_columns_query);
$show_column=mysql_fetch_array($config_columns_result);
}

}

if ($otms_num != 0) {
include "../show_table.h";
}

echo "<P>\r\n";

// ******************************* showing next month if there are otms submitted for it...

if (($userLevel == 30) || ($userLevel == 20) || ($userLevel == 1)) {
	$new_month = $rlookup_month + 1;
	if ($new_month == 13) {
  		$new_month = 1;
  		$new_year = $show_year + 1;
	} else { $new_year = $show_year; }

	if ($userLevel == 30) {
		$otms_query = $otms_query_std . "WHERE otms.year_month>='$new_year-$new_month-00' and otms.coid=$user_coid ORDER BY $order";
	} elseif ($userLevel == 1) {
		$new_month = $today_month;
		$new_year = $today_year;
		$otms_query = $otms_query_std . "WHERE otms.year_month>='$new_year-$new_month-00' ORDER BY $order";
	} else {
		$otms_query = $otms_query_std . "WHERE otms.year_month='$new_year-$new_month-00' and otms.region=$user_coid and otms.award_level=2 ORDER BY $order";
	}

	$otms_result=mysql_query($otms_query);
	$otms_num=mysql_numrows($otms_result); 
	if ($otms_num > 0) {
		$lumid = $new_month;
		include ($base_folder . "/queries/queries_lu_month.h");
		echo "<h3>All OTMS submitted for the month of $month_name $new_year</h3>\r\n";
		$config_columns_query="SELECT * FROM config_columns WHERE ccid='u" ."$userLevel" . "gpn'";
		$config_columns_result=mysql_query($config_columns_query);
		$show_column=mysql_fetch_array($config_columns_result);
		echo "<B>Total OTMs submitted for NEXT month: $otms_num</B><br><i><font color=red>They may need to be back dated to the current submission month</font></I><p>";
		include "../show_table.h";
	}
}
//****************************************

echo "<P>";
echo "Check out the <a href='faqs.php'>Frequently Asked Questions</a> about the OTM Database System!";

//}
/*
// INFORMATION TO USER CAMPUS ADMIN WHO ARE PRESUBMITTING... TURNED OFF IN 2004
if (($userLevel == 30) && ($allow_presubmit == 1)) {
?>
<H4><a name="ca5">How do I set local due dates?</a></H4>
<UL>To set local dates, the university must be set up to allow presubmissions to the local level.  After logging into the system, click on <B>Dates - Local</B>.  Click
on edit next to each month, select the month, date, and hour you want them due, and click update.  All OTMs are due Central Standard Time on the date/time specified.  In the event that you do not    
have the option of specifying dates after changing your university preferences to allow presubmission, log out of the system and log back in.<P>
<B>Do NOT set the OTMs due at the same date and time that they are due on the regional level.</B>  When a campus decides to presubmit OTMs, the campus 
administrator <B>MUST</B> go in and promote them to the regional level for the regional administrator to see the OTM.  In the event that your campus decides to have them
due at the same time as your regional level (for example of the summer), turn off presubmitting before people start submitting OTMs for that month.  
This will set each OTM to be sent to the regional level after the due date.
</UL>
<?
}
*/

// SHOW PREVIOUSLY SUBMITTED OTMS
if ($show_previous_submissions == 1) {
	$total_otms_num = $otms_num;
	$otms_query = $otms_query_std . "WHERE `uid` = $ID AND `year_month` < '$show_year-$lookup_month-00' ORDER BY otms.year_month DESC";
	$otms_result=mysql_query($otms_query);
	$otms_num=mysql_numrows($otms_result); 

	if ($otms_num > 0) {  
		echo "<h3>Your OTMs submitted for previous months</h3>\r\n";
		$config_columns_query="SELECT * FROM config_columns WHERE ccid='upm'";
		$config_columns_result=mysql_query($config_columns_query);
		$show_column=mysql_fetch_array($config_columns_result);
		include "../show_table.h";
		echo "<P>\r\n";
	}
}

//SUBMITTING OTMS TO REGIONAL LEVEL JAVACSRIPT SECTION
if ($sc_submit_region == 1) {
?>
<SCRIPT TYPE="text/JavaScript">
function submitregion(form) {
pri = "";
thr = "";
 for (i=0; i<form.elements.length; i++) {
      if ((form.elements[i].type == "radio") && (form.elements[i].checked))
         {
	 pri++;
         if (thr == "") { 
		thr = form.elements[i].value;
		}
	 else {
         thr = thr + "," + form.elements[i].value;
	 }

         }
      }
<?
$action_d = "'/login/otm_forward.php3?forward_value=2&selected='+thr+'&count='+pri;\r\n";
echo "	form.action = $action_d";
?>
	form.submit();
}
<?
echo "</script>\r\n";
}

if ($sc_submit_national == 1) {
?>
<SCRIPT TYPE="text/JavaScript">
function submitnational(form) {
pri = "";
thr = "";
 for (i=0; i<form.elements.length; i++) {
      if ((form.elements[i].type == "radio") && (form.elements[i].checked))
         {
	 pri++;
         if (thr == "") { 
		thr = form.elements[i].value;
		}
	 else {
         thr = thr + "," + form.elements[i].value;
	 }

         }
      }
<?
$action_d = "'/login/otm_forward.php3?forward_value=3&selected='+thr+'&count='+pri;\r\n";
echo "	form.action = $action_d";
?>
	form.submit();
}
<?
echo "</script>\r\n";
}

if ($sc_submit_global == 1) {
?>
<SCRIPT TYPE="text/JavaScript">
function submitglobal(form) {
pri = "";
thr = "";
 for (i=0; i<form.elements.length; i++) {
      if ((form.elements[i].type == "radio") && (form.elements[i].checked))
         {
	 pri++;
         if (thr == "") { 
		thr = form.elements[i].value;
		}
	 else {
         thr = thr + "," + form.elements[i].value;
	 }

         }
      }
<?
$action_d = "'/login/otm_forward.php3?forward_value=4&selected='+thr+'&count='+pri;\r\n";
echo "	form.action = $action_d";
?>
	form.submit();
}
<?
echo "</script>\r\n";
}

//VOTING SECTION JAVASCRIPT
// THIS WILL NOT WORK IF A USER IS VOTING IN TWO DIFFERENT LEVELS AT THE SAME TIME
if (($sc_vote_region == 1) || ($sc_vote_national == 1) || ($sc_vote_campus == 1)) { 
?>

<SCRIPT TYPE="text/JavaScript">


function myvote(form) {
pri = "";
thr = "";


 for (i=0; i<form.elements.length; i++) {
      if ((form.elements[i].type == "radio") && (form.elements[i].checked))
         {
	 pri++;
         if (thr == "") { 
		thr = form.elements[i].value;
		}
	 else {
         thr = thr + "," + form.elements[i].value;
	 }

         }
      }
<?
$action_d = "'/login/otm_vote.php?award_level='+form.awd_lvl.value + '&selected='+thr+'&count='+pri;\r\n";
echo "	form.action = $action_d";
?>
	form.submit();

}


<?
echo "</script>\r\n";
}




//show delete OTM javascript
deleteOTM();

if ($sc_submit_region == 1) {
?>
<SCRIPT TYPE="text/JavaScript">
function confirm_reg_retract(otmid, ccid) {
	input_box=confirm("Retract this Regional Level Submission?");
	if (input_box==true) {
<?
		$page = ($PHP_SELF);
		$page = split("/", $page); 
		$n = count($page)-1; 
		$page = $page[$n]; 
		$page = split("\.", $page, 2); 
		$page = $page[0]; 
		$display = "var URL = '$page.php3?command=forward_remove&otmid=' + otmid + '&ccid=' + ccid;\r\n";
		echo $display;
?>
		self.location = URL
	}
	else {  }
}
<?
echo "</script>\r\n";
}


if ($sc_vote_campus == 1) {
?>
<SCRIPT TYPE="text/JavaScript">
function confirm_reg_retract(otmid, uid, award_level) {
	input_box=confirm("Retract this vote?");
	if (input_box==true) {
<?
		$page = ($PHP_SELF);
		$page = split("/", $page); 
		$n = count($page)-1; 
		$page = $page[$n]; 
		$page = split("\.", $page, 2); 
		$page = $page[0]; 
		$display = "var URL = '$page.php3?command=vote_remove&otmid=' + otmid + '&uid=' + $ID + '&award_level=' + award_level;\r\n";
		echo $display;
?>
		self.location = URL
	}
	else {  }
}
<?
echo "</script>\r\n";
}



if ($sc_vote_region == 1) {
?>
<SCRIPT TYPE="text/JavaScript">
function confirm_reg_retract(otmid, uid, award_level) {
	input_box=confirm("Retract this vote?");
	if (input_box==true) {
<?
		$page = ($PHP_SELF);
		$page = split("/", $page); 
		$n = count($page)-1; 
		$page = $page[$n]; 
		$page = split("\.", $page, 2); 
		$page = $page[0]; 
		$display = "var URL = '$page.php3?command=vote_remove&otmid=' + otmid + '&uid=' + $ID + '&award_level=' + award_level;\r\n";
		echo $display;
?>
		self.location = URL
	}
	else {  }
}
<?
echo "</script>\r\n";
}

if ($sc_submit_national == 1) {
?>
<SCRIPT TYPE="text/JavaScript">
function confirm_reg_retract(otmid, ccid) {
	input_box=confirm("Retract this National Level Submission?");
	if (input_box==true) {
<?
		$page = ($PHP_SELF); 
		$page = split("/", $page); 
		$n = count($page)-1; 
		$page = $page[$n]; 
		$page = split("\.", $page, 2); 
		$page = $page[0]; 
		$display = "var URL = '$page.php3?command=forward_remove&otmid=' + otmid + '&ccid=' + ccid;\r\n";
		echo $display;
?>
		self.location = URL
	}
	else {  }
}
<?
echo "</script>\r\n";
}

if ($sc_vote_national == 1) {
?>
<SCRIPT TYPE="text/JavaScript">
function confirm_nat_retract(otmid, uid, award_level) {
	input_box=confirm("Retract this vote?");
	if (input_box==true) {
<?
		$page = ($PHP_SELF);
		$page = split("/", $page); 
		$n = count($page)-1; 
		$page = $page[$n]; 
		$page = split("\.", $page, 2); 
		$page = $page[0]; 
		$display = "var URL = '$page.php3?command=vote_remove&otmid=' + otmid + '&uid=' + $ID + '&award_level=' + award_level;\r\n";
		echo $display;
?>
		self.location = URL
	}
	else {  }
}
<?
echo "</script>\r\n";
}

if ($sc_submit_global == 1) {
?>
<SCRIPT TYPE="text/JavaScript">
function confirm_nat_retract(otmid, ccid) {
	input_box=confirm("Retract this National Winner?");
	if (input_box==true) {
<?
		$page = ($PHP_SELF); 
		$page = split("/", $page); 
		$n = count($page)-1; 
		$page = $page[$n]; 
		$page = split("\.", $page, 2); 
		$page = $page[0]; 
		$display = "var URL = '$page.php3?command=forward_remove&otmid=' + otmid + '&ccid=' + ccid;\r\n";
		echo $display;
?>
		self.location = URL
	}
	else {  }
}
<?
echo "</script>\r\n";
}

    $timer->stop(); 
    $timer->showDuration();
?>
</BODY>

</HTML>

<? mysql_close(); ?>
