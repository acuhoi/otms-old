<?php
/**************************************************************/
/*              phpSecurePages version 0.28 beta              */
/*       Written by Paul Kruyt - phpSecurePages@xs4all.nl     */
/*                http://www.phpSecurePages.com               */
/**************************************************************/
/*           Start of phpSecurePages Configuration            */
/**************************************************************/


/****** Installation ******/
$cfgIndexpage = '/index.php';
  // page to go to, if login is cancelled
  // Example: if your main page is http://www.mydomain.com/index.php
  // the value would be $cfgIndexpage = '/index.php'
$admEmail = 'akb9465@ksu.edu';
  // E-mail adres of the site administrator
  // (This is being showed to the users on an error, so you can be notified by the users)
$noDetailedMessages = false;
  // Show detailed error messages (false) or give one single message for all errors (true).
  // If set to 'false', the error messages shown to the user describe what went wrong.
  // This is more user-friendly, but less secure, because it could allow someone to probe
  // the system for existing users.
$passwordEncryptedWithMD5 = true;		// Set this to true if the passwords are encrypted
                                          // with the MD5 algorithm
                                          // (not yet implanted, expect this in a next release)
$languageFile = 'lng_english.php';        // Choose the language file
$bgImage = 'bg_lock.gif';                 // Choose the background image
$bgRotate = true;                         // Rotate the background image from list
                                          // (This overrides the $bgImage setting)


/****** Lists ******/
// List of backgrounds to rotate through
$backgrounds[] = 'bg_lock.gif';
$backgrounds[] = 'bg_lock2.gif';


/****** Database ******/
$useDatabase = true;                     // choose between using a database or data as input

/* this data is necessary if a database is used */
$cfgServerHost = 'localhost';             // MySQL hostname
$cfgServerPort = '';                      // MySQL port - leave blank for default port
$cfgServerUser = 'root';                  // MySQL user
$cfgServerPassword = 'ycr69ksu';                  // MySQL password

$cfgDbDatabase = 'nrhh_otms';        // MySQL database name containing phpSecurePages table
$cfgDbTableUsers = 'users';         // MySQL table name containing phpSecurePages user fields
$cfgDbLoginfield = 'username';                // MySQL field name containing login word
$cfgDbPasswordfield = 'password';         // MySQL field name containing password
$cfgDbUserLevelfield = 'otysuserlevel';       // MySQL field name containing user level
  // Choose a number which represents the category of this users authorization level.
  // Leave empty if authorization levels are not used.
  // See readme.txt for more info.
$cfgDbUserIDfield = 'uid';        // MySQL field name containing user identification
  // enter a distinct ID if you want to be able to identify the current user
  // Leave empty if no ID is necessary.
  // See readme.txt for more info.


/****** Database - PHP3 ******/
/* information below is only necessary for servers with PHP3 */
$cfgDbTableSessions = 'phpSP_sessions';
  // MySQL table name containing phpSecurePages sessions fields
$cfgDbTableSessionVars = 'phpSP_sessionVars';
  // MySQL table name containing phpSecurePages session variables fields


/****** Data ******/
$useData = true;                          // choose between using a database or data as input


/**************************************************************/
/*             End of phpSecurePages Configuration            */
/**************************************************************/


// https support
if (getenv("HTTPS") == 'on') {
	$cfgUrl = 'https://';
} else {
	$cfgUrl = 'http://';
}

// getting other login variables
$cfgHtmlDir = $cfgProgDir;
if ($message) $messageOld = $message;
$message = false;

// Create a constant that can be checked inside the files to be included.
// This gives an indication if secure.php has been loaded correctly.
define("LOADED_PROPERLY", true);


// include functions and variables
function admEmail() {
	// create administrators email link
	global $admEmail;
	return("<A HREF='mailto:$admEmail'>$admEmail</A>");
}

include($cfgProgDir . "lng/" . $languageFile);
include($cfgProgDir . "session.php");

include($cfgProgDir . "nonlogin.php");

// choose between login or logout
echo $HTTP_GET_VARS["logout"];
?>
