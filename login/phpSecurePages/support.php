<?PHP
error_reporting(E_ALL);
ini_set("display_errors", 1);

#  if(defined("CRYPT_BLOWFISH") && CRYPT_BLOWFISH) {
#    echo "CRYPT_BLOWFISH is enabled!";
#  } else {
#    echo "CRYPT_BLOWFISH is NOT enabled!";
#  }

phpinfo();

/**
 * This code will benchmark your server to determine how high of a cost you can
 * afford. You want to set the highest cost that you can without slowing down
 * you server too much. 8-10 is a good baseline, and more is good if your servers
 * are fast enough. The code below aims for ≤ 50 milliseconds stretching time,
 * which is a good baseline for systems handling interactive logins.
 */
/*
$timeTarget = 0.05; // 50 milliseconds 

$cost = 8;
do {
    $cost++;
    $start = microtime(true);
    password_hash("test", PASSWORD_BCRYPT,$cost);
    $end = microtime(true);
} while (($end - $start) < $timeTarget);


crypt('rasmuslerdorf', '$6$rounds=5000$usesomesillystringforsalt$')

echo "Appropriate Cost Found: " . $cost . "\n";
*/

/*
* Generate a secure hash for a given password. The cost is passed
* to the blowfish algorithm. Check the PHP manual page for crypt to
* find more information about this setting.
*/
function generate_hash($password, $cost=11){
        /* To generate the salt, first generate enough random bytes. Because
         * base64 returns one character for each 6 bits, the we should generate
         * at least 22*6/8=16.5 bytes, so we generate 17. Then we get the first
         * 22 base64 characters
         */
#        $salt=substr(base64_encode(openssl_random_pseudo_bytes(17)),0,22);
	$salt = substr(str_replace('+', '.', base64_encode(pack('N4', mt_rand(), mt_rand(), mt_rand(), mt_rand()))), 0, 22); 
        /* As blowfish takes a salt with the alphabet ./A-Za-z0-9 we have to
         * replace any '+' in the base64 string with '.'. We don't have to do
         * anything about the '=', as this only occurs when the b64 string is
         * padded, which is always after the first 22 characters.
         */
//        $salt=str_replace("+",".",$salt);
        /* Next, create a string that will be passed to crypt, containing all
         * of the settings, separated by dollar signs
         */
echo $salt;
        $param='$'.implode('$',array(
                "2y", //select the most secure version of blowfish (>=PHP 5.3.7)
                str_pad($cost,2,"0",STR_PAD_LEFT), //add the cost in two digits
                $salt //add the salt
        ));
       
echo "<BR>";
echo $param;
        //now do the actual hashing
echo "<BR>";
echo crypt($password,$param);
        return crypt($password,$param);
}

$cr=generate_hash("test1test");
echo "<BR>";
echo $cr;
?>
