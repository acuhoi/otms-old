<?

	$cfgProgDir = 'phpSecurePages/';
	include($cfgProgDir . "secure.php");

$command="update";
$i=0;

$otm_type = "program";

include("../config/dbinfo.inc.php");

if (!$otmid) { echo "<CENTER><H3>NO OTM TO EDIT</H3></CENTER>"; }
else {
$oid = $otmid;
include ($base_folder . "/queries/queries_otm_program.h");
include ($base_folder . "/queries/queries_user.h");

if ((($ID == $otm_uid) && (strcmp($otm_year_month,$lookup_date))) || (($userLevel == 30) && ($user_coid == $otm_coid)) || (($userLevel == 20) && ($user_coid == $otm_crid)) || ($userLevel == 10) || ($userLevel == 1)) {

//include "form_edit_add_org_upd.h";

?>

<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<HTML>
<HEAD>
<TITLE>Program OTM Nomination Form</TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../stylesheet.css'>
<SCRIPT LANGUAGE="JavaScript">

function wordCounter(string, countfield) {
   var re = /\s+/g;
   var words = string.split(re);
   countfield.value = words.length;
}


</script>



</HEAD>
<?
echo "<CENTER><H3>$message</H3>\r\n";
echo "<H2>General OTM Nomination Form</H2>\r\n";

//MIGHT NOT BE IMPLEMENTED IN FINAL RELEASE
if (($userLevel == 1) || ($userLevel == 3)) {
//include "form_edit_add_org.h";
}

echo "<TABLE class='box-base' BORDER='0' CELLSPACING='0' CELLPADDING='8' WIDTH='620'>\r\n";
echo "<FORM ACTION='../form_program_update.php3' NAME='form1' METHOD='post'>\r\n";
echo "<INPUT TYPE='hidden' NAME='otm_year' VALUE='$otm_year'>\r\n";
echo "<INPUT TYPE='hidden' NAME='otm_month' VALUE='$otm_month'>\r\n";
echo "<INPUT TYPE='hidden' NAME='date_of_entry' VALUE='$otm_date_of_entry'>\r\n";
echo "<INPUT TYPE='hidden' NAME='caid' VALUE='$otm_award'>\r\n";
echo "<INPUT TYPE='hidden' NAME='uid' VALUE='$otm_uid'>\r\n";
echo "<INPUT TYPE='hidden' NAME='command' VALUE='$command'>\r\n";
echo "<INPUT TYPE='hidden' NAME='otmid' VALUE='$otmid'>\r\n";

if (($userLevel == 1) || ($userLevel == 10)  || ($userLevel == 20) || ($userLevel == 30)) {
echo "<TR><TD COLSPAN=2 align=center>\r\n";

    $query="SELECT * FROM config_awards";
    $ca_result=mysql_query($query);
    $ca_num=mysql_numrows($ca_result);
echo "<B>OTM Award:</B><BR>\r\n";
echo "<SELECT NAME='award' size='1'>\r\n";
echo "<OPTION VALUE='0'>No Award</OPTION>\r\n";

$i=0;
while ($i < $ca_num) {
    $otms_caid=mysql_result($ca_result,$i,"caid");
    $otms_award=mysql_result($ca_result,$i,"name");
    $otms_ar=mysql_result($ca_result,$i,"access_required");
if ($otm_caid == $otms_caid) {
echo "<OPTION VALUE='$otms_caid' SELECTED>$otms_award</OPTION>\r\n";
}
elseif (($userLevel <= $otms_ar) || ($userLevel == 1)) {
echo "<OPTION VALUE='$otms_caid'>$otms_award</OPTION>\r\n";
}
else {
}
++$i;
}
echo "</SELECT>\r\n";
echo "</TD><TR>\r\n";
}


if (($userLevel == 1)) {
echo "<TR><TD COLSPAN=2 align=center>\r\n";
echo "<B>OTM Award Level:</B><BR>\r\n";
echo "<SELECT NAME='award_level' size='1'>\r\n";

echo "<OPTION VALUE='1'";
if ($otm_award_level == 1) { echo " SELECTED"; }
echo ">Campus</OPTION>\r\n";
echo "<OPTION VALUE='2'";
if ($otm_award_level == 2) { echo " SELECTED"; }
echo ">Regional</OPTION>\r\n";
echo "<OPTION VALUE='3'";
if ($otm_award_level == 3) { echo " SELECTED"; }
echo ">National</OPTION>\r\n";

echo "</SELECT>\r\n";
echo "</TD></TR>\r\n";
} else { echo "<INPUT TYPE='hidden' NAME='award_level' VALUE='$otm_award_level'>\r\n"; }




echo "<TR>\r\n";

?>
<TD WIDTH=300 align=center>
<B>Month:</B><BR>
<?
if (($userLevel == 1) || ($userLevel == 20) || ($userLevel == 30)) { /// ************************************************
$m = 0;

include ($base_folder . "/queries/queries_lu_months.h");

echo "<SELECT NAME='otm_month' size='1'>\r\n";
while ($m < $lum_num) {
$mname=mysql_result($lum_result,$m,"name");
$mnumber=mysql_result($lum_result,$m,"lumid");

if ($otm_month == $mnumber) {
echo "<OPTION VALUE='$mnumber' SELECTED>$mname</OPTION>\r\n";
}
else {
echo "<OPTION VALUE='$mnumber'>$mname</OPTION>\r\n";
}
++$m;
}
echo "</SELECT>\r\n";
}
else {
echo $otm_month_name;
}

?>
</TD>

<TD WIDTH=300 align=center>
<B>Year:</B><BR>
<?
if (($userLevel == 1) || ($userLevel == 20) || ($userLevel == 30)) { /// ************************************************
echo "<INPUT NAME='otm_year' SIZE='4' maxlength='4' value='$otm_year'><P>\r\n";
}
else {
echo $otm_year;
}
?>
</TD>
</TR>

<TR>
<TD COLSPAN=2>
<CENTER><B> Category:</B><BR>
<SELECT NAME="otm_category" size="1">
<OPTION VALUE="">(Select Category)</OPTION>
<?
$category_query="SELECT * FROM config_categories WHERE category_type='program' AND active='1' order by name";
$conf_cat_result=mysql_query($category_query);
$category_num=mysql_numrows($conf_cat_result); 
$i=0;
while ($i < $category_num) {
$cat_ccid=mysql_result($conf_cat_result,$i,"ccid");
$cat_name=mysql_result($conf_cat_result,$i,"name");
	if ((in_array ($cat_ccid, $ar_cat))) {
		if ($cat_ccid == $otm_ccid) {
			echo "<OPTION VALUE='$cat_ccid' SELECTED>$cat_name</OPTION>\r\n";
		} else {
			echo "<OPTION VALUE='$cat_ccid'>$cat_name</OPTION>\r\n";
		}
	} else {

	}
	++$i;
}
?>
</SELECT>
</TD>
</TR>

<TR>
<TD align=center>
<B>Nominee's School:</B><BR>
<? echo $school; ?>
</TD>

<TD align=center>
<B>Region:</B><BR>
<? echo $region; ?>
</TD>
</TR>

<TR>
<TD COLSPAN=2 ALIGN=CENTER>
<HR>
</TD>
</TR>

<? if (($otm_sub_org_id > 0) && ($otm_rec_org_id > 0 )) {
$coid = $otm_coid;
include ($base_folder . "/queries/queries_org_sub.h");
echo "<TR><TD><CENTER><b>Nominee's organization:</b><BR>\r\n";
echo "<SELECT NAME='rec_org'>\r\n";
echo "<OPTION VALUE=''>(Select Organization)</OPTION>\r\n";

$i=0;
while ($i < $org_sub_num) {
$csoid=mysql_result($org_sub_result,$i,"csoid");
$org_name=mysql_result($org_sub_result,$i,"name");


if ($otm_rec_org_id == $csoid) {
echo "<OPTION VALUE='$csoid' SELECTED>$org_name</OPTION>\r\n";
}
else {
echo "<OPTION VALUE='$csoid'>$org_name</OPTION>\r\n";
}
++$i;
}

echo "</SELECT></TD><TD>\r\n";
echo "<CENTER><b>Nominator's organization:</b><BR>\r\n";
echo "<SELECT NAME='sub_org'>\r\n";
echo "<OPTION VALUE=''>(Select Organization)</OPTION>\r\n";

$i=0;
while ($i < $org_sub_num) {
$csoid=mysql_result($org_sub_result,$i,"csoid");
$org_name=mysql_result($org_sub_result,$i,"name");
if ($otm_sub_org_id == $csoid) {
echo "<OPTION VALUE='$csoid' SELECTED>$org_name</OPTION>\r\n";
}
else {
echo "<OPTION VALUE='$csoid'>$org_name</OPTION>\r\n";
}
++$i;

}
}
echo "</CENTER></TD></TR>\r\n";


echo "<TR><TD>\r\n";
echo "<b>Person in Charge:</b><BR><INPUT NAME='person_in_charge' SIZE='35' maxlength='50' value='$otm_person_in_charge'><P>\r\n";
echo "<B>Address:</B><BR>\r\n";
echo "<INPUT NAME='nominee_address_1' SIZE='35' maxlength='35' value='$otm_nominee_address_1'><BR>\r\n";
echo "<INPUT NAME='nominee_address_2' SIZE='35' maxlength='35' value='$otm_nominee_address_2'><BR>\r\n";
echo "<INPUT NAME='nominee_address_3' SIZE='35' maxlength='35' value='$otm_nominee_address_3'><P>\r\n";
echo "<B>Phone:</B><BR>\r\n";
echo "<INPUT NAME='nominee_phone' SIZE='15' VALUE='$otm_nominee_phone' maxlength='12'><P>\r\n";
echo "<B>Email Address:</B><BR>\r\n";
echo "<INPUT NAME='nominee_email' SIZE='35' maxlength='40' value='$otm_nominee_email'>\r\n";
echo "<P><CENTER><b>On-Campus Population:</b><BR>\r\n";
echo "$otm_campus_pop\r\n";
echo "</CENTER></TD>\r\n";

echo "<TD>\r\n";

echo "<B>Nominator:</B><BR><INPUT NAME='nominator' SIZE='35' maxlength='50' value='$otm_nominator'><P>\r\n";
echo "<B>Address:</B><BR>\r\n";
echo "<INPUT NAME='nominator_address_1' SIZE='35' maxlength='35' value='$otm_nominator_address_1'><BR>\r\n";
echo "<INPUT NAME='nominator_address_2' SIZE='35' maxlength='35' value='$otm_nominator_address_2'><BR>\r\n";
echo "<INPUT NAME='nominator_address_3' SIZE='35' maxlength='35' value='$otm_nominator_address_3'><P>\r\n";
echo "<B>Phone:</B><BR>\r\n";
echo "<INPUT NAME='nominator_phone' VALUE='$otm_nominator_phone' SIZE='15' maxlength='12'><P>\r\n";
echo "<B>Email Address:</B><BR>\r\n";
echo "<INPUT NAME='nominator_email' SIZE='35' maxlength='40' value='$otm_nominator_email'><P>\r\n";
echo "<CENTER><b>Chapter Size:</b><BR>\r\n";
echo "$otm_chapter_size</CENTER>";
?>
</TD></TR>

<TR>
<TD COLSPAN=2>
<CENTER><B>Program Title:</B><BR>
<? echo "<INPUT NAME='nominee' size='50' maxlength='50' value='$otm_nominee'>\r\n"; ?>
</TD>
</TR>

<TD>
<B>Target Population (in numbers):</B><BR>
<? echo "<INPUT NAME='target_pop' SIZE='35' maxlength='4' value='$otm_target_pop'>\r\n"; ?>
<P>
<B>Number of People in Attendance:</B><BR>
<? echo "<INPUT NAME='attendance_pop' SIZE='35' maxlength='4' value='$otm_attendance_pop'>\r\n"; ?>
<P>
<B>Number of People Needed to Organize:</B><BR>
<? echo "<INPUT NAME='organize_pop' SIZE='35' maxlength='4' value='$otm_organize_pop'>\r\n"; ?>
</TD>

<TD>
<B>Time Needed to Organize:</B><BR>
<? echo "<INPUT NAME='organize_time' SIZE='35' maxlength='35' value='$otm_organize_time'>\r\n"; ?>
<P>
<B>Date(s) of Program:</B><BR>
<? echo "<INPUT NAME='date_of_program' SIZE='35' maxlength='50' value='$otm_date_of_program'>\r\n"; ?>
<P>
<B>Cost of Program:</B><BR>
<? echo "<INPUT NAME='cost_of_program' SIZE='35' maxlength='35' value='$otm_cost_of_program'>\r\n"; ?>
</TD>
</TR>

<TR>
<TD COLSPAN=2 ALIGN=CENTER>
<HR>
<P><B>Origin of program:</b>
<TEXTAREA NAME="origin" WRAP=HARD ROWS=5 COLS=72>
<? 
$otm_origin = ereg_replace("(\r\n\r\n)", "<P>", $otm_origin);
$otm_origin = ereg_replace("(\r\n\t)", "<P>", $otm_origin);  
$otm_origin = ereg_replace("(\r\n)", "", $otm_origin);        
$otm_origin = ereg_replace("(<P>)", "\r\n\r\n", $otm_origin);

echo $otm_origin; 

?>
</TEXTAREA>
<p><b>Word Count (200 Maximum)</font>:</b><BR>
<? echo "<INPUT NAME='origin_count' SIZE='20' maxlength='3' value='$otm_origin_count'><BR>\r\n"; ?>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.origin.value,this.form.origin_count);"></p>


<HR>
<P><B>Please give a short description of program:</B>
<TEXTAREA NAME="description" WRAP=HARD ROWS=20 COLS=70>
<? 
$otm_description = ereg_replace("(\r\n\r\n)", "<P>", $otm_description);
$otm_description = ereg_replace("(\r\n\t)", "<P>", $otm_description);  
$otm_description = ereg_replace("(\r\n)", "", $otm_description);        
$otm_description = ereg_replace("(<P>)", "\r\n\r\n", $otm_description); 

echo $otm_description; 

?>
</TEXTAREA>
<p><b>Word Count (400 Maximum)</font>:</b><BR>
<? echo "<INPUT NAME='description_count' SIZE='20' maxlength='3' value='$otm_description_count'><BR>\r\n"; ?>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.description.value,this.form.description_count);"></p>

<HR>
<P><B>Goals of program:</B>
<TEXTAREA NAME="goals" WRAP=HARD ROWS=10 COLS=70>
<? 

$otm_goals = ereg_replace("(\r\n\r\n)", "<P>", $otm_goals);
$otm_goals = ereg_replace("(\r\n\t)", "<P>", $otm_goals);  
$otm_goals = ereg_replace("(\r\n)", "", $otm_goals);        
$otm_goals = ereg_replace("(<P>)", "\r\n\r\n", $otm_goals); 

echo $otm_goals; ?>
</TEXTAREA>
<p><b>Word Count (200 Maximum)</font>:</b><BR>
<? echo "<INPUT NAME='goal_count' SIZE='20' maxlength='3' value='$otm_goal_count'><BR>\r\n"; ?>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.goals.value,this.form.goal_count);"></p>

<HR>
<P><B>Positive and lasting effects of the program:</B>
<TEXTAREA NAME="effects" WRAP=HARD ROWS=10 COLS=70>
<? 

$otm_effects = ereg_replace("(\r\n\r\n)", "<P>", $otm_effects);
$otm_effects = ereg_replace("(\r\n\t)", "<P>", $otm_effects);  
$otm_effects = ereg_replace("(\r\n)", "", $otm_effects);        
$otm_effects = ereg_replace("(<P>)", "\r\n\r\n", $otm_effects); 

echo $otm_effects; ?>
</TEXTAREA>
<p><b>Word Count <font size="2">(200 Maximum)</font>:</b><BR>
<? echo "<INPUT NAME='effect_count' SIZE='20' maxlength='3' value='$otm_effect_count'><BR>\r\n"; ?>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.effects.value,this.form.effect_count);"></p>

<HR>
<P><B>Short evaluation of the program:</B>
<TEXTAREA NAME="evaluation" WRAP=HARD ROWS=10 COLS=70>
<? 
$otm_evaluation = ereg_replace("(\r\n\r\n)", "<P>", $otm_evaluation);
$otm_evaluation = ereg_replace("(\r\n\t)", "<P>", $otm_evaluation);  
$otm_evaluation = ereg_replace("(\r\n)", "", $otm_evaluation);        
$otm_evaluation = ereg_replace("(<P>)", "\r\n\r\n", $otm_evaluation); 

echo $otm_evaluation; ?>
</TEXTAREA>
<p><b>Word Count <font size="2">(200 Maximum)</font>:</b><BR>
<? echo "<INPUT NAME='evaluation_count' SIZE='20' maxlength='3' value='$otm_evaluation_count'><BR>\r\n"; ?>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.evaluation.value,this.form.evaluation_count);"></p>

<HR>
<P><B>How could this program be adapted to other campuses?</B>
<TEXTAREA NAME="adapted" WRAP=HARD ROWS=10 COLS=70>
<?

$otm_adapted = ereg_replace("(\r\n\r\n)", "<P>", $otm_adapted);
$otm_adapted = ereg_replace("(\r\n\t)", "<P>", $otm_adapted);  
$otm_adapted = ereg_replace("(\r\n)", "", $otm_adapted);        
$otm_adapted = ereg_replace("(<P>)", "\r\n\r\n", $otm_adapted); 

 echo $otm_adapted; ?>
</TEXTAREA>
<p><b>Word Count <font size="2">(200 Maximum)</font>:</b><BR>
<? echo "<INPUT NAME='adapt_count' SIZE='20' maxlength='3' value='$otm_adapt_count'><BR>\r\n"; ?>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.adapted.value,this.form.adapt_count);"></p>
</Center>
</TD>
</TR>



<TR>
<TD COLSPAN=2 ALIGN=CENTER>

<HR SIZE=4>
</Center>

<P>World Wide Web nominations must be typed on this form and each area <BR>
<b>must not exceed maximum word count specified</b>.  Nominations not following <BR>
this format may not be considered for a national award.  No additional material <BR>
will be considered. <P>
<p align="right">Revised May 1999</FONT>
<HR SIZE=4>
</TD>
</TR>

<?
include ($base_folder . "/form_blurb.h");


echo "<INPUT TYPE='hidden' NAME='school' VALUE='$school'>\r\n";
echo "<INPUT TYPE='hidden' NAME='cuid' VALUE='$otm_coid'>\r\n";
echo "<INPUT TYPE='hidden' NAME='region' VALUE='$region'>\r\n";
echo "<INPUT TYPE='hidden' NAME='crid' VALUE='$otm_crid'>\r\n";

echo "<INPUT TYPE='hidden' NAME='campus_pop' VALUE='$otm_campus_pop'>\r\n";
echo "<INPUT TYPE='hidden' NAME='chapter_size' VALUE='$otm_chapter_size'>\r\n";



?>

<TR><TD COLSPAN=2 ALIGN=CENTER><INPUT TYPE="button" VALUE="Save" OnClick="mysave(form1)"><INPUT TYPE="button" VALUE="Cancel" VALUE="Clear Form" OnClick="mycancel(form1)"><P></TD></TR>
</TABLE>
</TD>
</TR>

</TABLE>
</CENTER>

</FORM><script language="JavaScript" type="text/javascript">
function isempty(type) {
	if(type.length == 0) {
		return true
	}

	return false
}
function mycancel(form1) {
	form1.action = "/login/main.php3";
	form1.submit();

}
function mysave(form1) {
var str

        if(isempty(form1.otm_category.value)) {
                alert("Error: Category is required.")
                form1.otm_category.focus()
		return false
        }
<? if ($allow_suborgs == 1) { ?>
        if(isempty(form1.rec_org.value)) {
                alert("Error: Receiving Organization is required.")
                form1.rec_org.focus()
		return false
        }
        if(isempty(form1.sub_org.value)) {
                alert("Error: Submitting Organization is required.")
                form1.sub_org.focus()
		return false
        }
<? } ?>
        if(isempty(form1.nominee.value)) {
                alert("Error: A Program Title is required.")
                form1.nominee.focus()
		return false
        }
        if(isempty(form1.nominator.value)) {
                alert("Error: Nominator is required.")
                form1.nominator.focus()
		return false
        }
        if(isempty(form1.person_in_charge.value)) {
                alert("Error: A Nominee is required.")
                form1.program_title.focus()
		return false
        }
        if(isempty(form1.origin.value)) {
                alert("Error: The origin of the program is required.")
                form1.origin.focus()
		return false
        } 
        if(isempty(form1.description.value)) {
                alert("Error: A short description of the program is required.")
                form1.description.focus()
		return false
        } 
        if(isempty(form1.goals.value)) {
                alert("Error: The goals of the program is required.")
                form1.goals.focus()
		return false
        } 
        if(isempty(form1.effects.value)) {
                alert("Error: Possitive and lasting effects of the program is required.")
                form1.effects.focus()
		return false
        } 
        if(isempty(form1.evaluation.value)) {
                alert("Error: A short evaluation of the program is required.")
                form1.evaluation.focus()
		return false
        } 
        if(isempty(form1.adapted.value)) {
                alert("Error: How this program could be adapted is required.")
                form1.adapted.focus()
		return false
        } 
<? if (($otm_blurb_local == 1) || ($otm_blurb_reg == 1)) { ?>
        if(isempty(form1.otm_blurb.value)) {
                alert("Error: A Short Summary is required.")
                form1.otm_blurb.focus()
		return false
        } 
<? } ?>

                wordCounter(form1.origin.value,form1.origin_count);
                wordCounter(form1.description.value,form1.description_count);
                wordCounter(form1.goals.value,form1.goal_count);
                wordCounter(form1.effects.value,form1.effect_count);
                wordCounter(form1.evaluation.value,form1.evaluation_count);
                wordCounter(form1.adapted.value,form1.adapt_count);

if (form1.command.value == "update") {
        URL = "../form_program_submitted.php3?action=" + form1.command.value + "&otmid=" + form1.otmid.value
} else { URL = "../form_program_submitted.php3" } 
	form1.action = URL
        form1.submit();	


}
</script>
</BODY>


</HTML>
<?
}

else { echo "<CENTER><H3>YOU ARE NOT ALLOWED TO EDIT THIS OTM</H3></CENTER>"; }

}
