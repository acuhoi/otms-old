<?
$logout = false; 
	$cfgProgDir = 'phpSecurePages/';
	include($cfgProgDir . "secure.php");

include("../config/dbinfo.inc.php");
//$connection=mysql_connect(localhost,$dbusername,$dbpassword) or die("Could not connect to the database server"); 


include ($base_folder . "/queries/queries_user.h");
if ($user_coid != 0) {
include ($base_folder . "/queries/queries_org.h");
}
?>

<HTML>
<HEAD>
<LINK REL='stylesheet' TYPE='text/css' HREF='../stylesheet.css'>
</HEAD>
<body class=frame-left>
<? 
echo "<img src='../img/otm_logo.png' align='middle' vspace='5' hspace='10'>";
echo "<BR><BR><CENTER><B>Welcome<BR>$user_fname $user_lname!</B></CENTER><BR><BR>";
echo "<A HREF='main.php3?uid=$uid&order=config_categories.name' target='main'>Home</A><P>\r\n";
echo "<P>\r\n";
echo "<P>\r\n";

if ($userLevel == 1 || $userLevel == 10 || $userLevel == 20 || $userLevel == 30) {
	echo "<B>Administrative Functions</b>\r\n";
	echo "\r\n";
	echo "<LI><a href='admin_users.php3?order=lname&active=1' target='main'>Admin Users</a>\r\n";

        if (($userLevel == 1) || ($userLevel == 10)) {
                echo "<LI><a href='admin_orgs.php?order=config_orgs.coid' target='main'>Admin Orgs</a>\r\n";
        }

	if ($userLevel == 1) {
		echo "<LI><a href='admin_users.php3?order=lname&active=0' target='main'>Inactive Users</a>\r\n";
		echo "<LI><a href='/login/admin_users.php3?order=lname&active=2&gid=' target='main'>Deactivated</a>\r\n";
		echo "<LI><a href='reg_add.php' target='main'>Add Region</a>\r\n";
	}

	if ($coid == 1) { // NATIONAL OFFICE
	echo "<LI><a href='/login/committee_nat.php' target='main'>Committee</a>\r\n";
	}
	if ($userLevel == 20) { // REGIONAL LEVEL TO ADJUST OTM COMMITTEES
	echo "<LI><a href='/login/committee_reg.php' target='main'>Committee</a>\r\n";
	}
	echo "<LI><a href='/login/email_awards_ques.php3?coid=$coid' target='main'>Send-Email</a>\r\n";

	if (($org_level == "regional")) {
		echo "<LI><a href='/login/univ_add.php?coid=$coid' target='main'>Universities</a>\r\n";
	}

	if ($userLevel == 10 || $userLevel == 20 || $userLevel == 30) {
		echo "<LI><a href='/login/calc_points.php?coid=$coid' target='main'>Point Calculation</a>";
	}

	if ($userLevel == 1) {
		echo "<P>\r\n";
		echo "<B>Statistics</b>\r\n";
		echo "\r\n";
		echo "<LI><a href='admin_db_stats.php' target='main'>Database</a>\r\n";
		echo "<LI><a href='admin_db_stats_byday.php' target='main'>by Day</a>\r\n";
		echo "<LI><a href='admin_db_stats_bymonth.php' target='main'>by Month</a>\r\n";
		echo "<LI><a href='admin_db_stats_byuser.php?order=lname&active=1' target='main'>by User</a>\r\n";
		echo "<LI><a href='admin_db_stats_byregion.php' target='main'>by Region</a>\r\n";
		echo "<LI><a href='admin_db_stats_byuniv.php' target='main'>by Univ</a>\r\n";
		echo "<LI><a href='admin_user_stats.php' target='main'>User Logins</a>\r\n";
		echo "<LI><a href='admin_otmview_stats.php' target='main'>OTM Views</a>\r\n";
	}
	echo "<P>\r\n";
	echo "<B>Configuration Options</b>\r\n";
	echo "\r\n";

	//echo "<LI><!a href='/login/config_awards.php3?edit=0' target='main'>Awards</a>\r\n"; ********************

	if ($coid == 1) {
	echo "<LI><a href='/login/config_regional_info.php3' target='main'>Org Info</a>\r\n";

	}

	if (($school_region_id > 1)) {
	echo "<LI><a href='/login/config_campus_info.php3' target='main'>Campus Info</a>\r\n";
		if ($allow_suborgs == 1) {
		echo "<LI><a href='/login/config_suborgs.php' target='main'>Hall Orgs</a>\r\n";
		}
	}

	if ((($allow_presubmit==1) && (($school_region_id != 1) || ($school_region_id != 0)))) {
	echo "<LI><a href='/login/config_dates.php3?coid=$coid' target='main'>Config Dates</a>\r\n";
	}

	if (($school_region_id == 1)) {
	echo "<LI><a href='/login/config_regional_info.php3' target='main'>Org Info</a>\r\n";
	}
	if (($school_region_id == 1) || ($coid == 1)) {
	echo "<LI><a href='/login/config_dates_reg.php3?coid=$coid' target='main'>Config Dates</a>\r\n";
	}

	if (($userLevel == 1)) {
	echo "<LI><a href='/login/config_columns.php3' target='main'>Columns</a>\r\n";
	}
}

echo "<P>\r\n";
echo "<a href='nrhh_search.php3' target='main'>Search OTMs</a>\r\n";
echo "<P>\r\n";

if (($userLevel == 30) && ($allow_presubmit==0)) {
	echo "<B>Information</b>\r\n";
	echo "<a href='config_dates.php3?coid=$coid' target='main'>Dates</a><BR>\r\n";
	echo "<P>\r\n";
}
elseif (($userLevel == 30) || ($userLevel == 35) || ($userLevel == 40)) {
	echo "<B>Information</b><BR>\r\n";
	echo "<a href='config_dates.php3?coid=$coid' target='main'>Dates</a><BR>\r\n";
	echo "<P>\r\n";
}

if (($userLevel == 40) || ($userLevel == 35) || ($userLevel == 30) || ($userLevel == 20) || ($userLevel == 10) || ($userLevel == 1)) {
	echo "<B>Preferences</b><BR>\r\n";
	echo "<a href='change_info.php3?uid=$ID&vid=$vid' target='main'>User Information</a><BR>\r\n";
	echo "<P>\r\n";
} else { }

if (($userLevel == 10) || ($userLevel == 20)) { 
	echo "To submit OTMs under an account, you must create an account under your University";
} elseif ($userLevel == 1) {
	echo "<B>OTM Submissions</b><BR>\r\n";
	echo "<a href='../form_general.php3?coid=0' target='main'>Submit General</a><BR>\r\n";
	echo "<a href='../form_program.php3?coid=0' target='main'>Submit Program</a><BR>\r\n";
	echo "<P>\r\n";
} else {
	echo "<B>OTM Submissions</b><BR>\r\n";
	echo "<a href='form_general.php3' target='main'>Submit General</a><BR>\r\n";
	echo "<a href='form_program.php3' target='main'>Submit Program</a><BR>\r\n";
	echo "<P>\r\n";
}

if (($userLevel == 1) || ($userLevel == 10)) {
        echo "<B><a href='/login/export-list.php' target='main'>Export</a></B><P>";
}

echo "\r\n";
echo "<P>\r\n";

//if ($userLevel == 1) {
//	echo "<a href='admin/' target='main'>Server Logs</a><BR>\r\n";
//}
if ($ID == 2) {
	echo "<a href='http://admin.nrhh.org/' target='_new'>phpMyAdmin-2.5.2</a><BR>\r\n";
}


echo "<P>\r\n";

echo "<A HREF='logout.php' target='_top'>LogOut</A>\r\n";


echo "<P>\r\n";
echo "<P><font size=-6>$version</font><P>\r\n";


?>
<a href="/login/support.php" target="main"><img id="navhelp" src="../img/help.gif" alt="Help" / border=0></a><a href=faqs.php target="main" id="navhelp"><B><BR>F.A.Q.</a></B></a><span id=navhelp>
<?

//echo "Please send all comments concerning the OTM Database System to: <a href='mailto:otmdb@andrewbell.info'>Andrew Bell</a>\r\n";

?>
</BODY>
</HTML>

