<?

	$cfgProgDir = 'phpSecurePages/';
	include($cfgProgDir . "secure.php");

$command="update";
$i=0;

$otm_type = "general";

include("../config/dbinfo.inc.php");

if (!$otmid) { echo "<CENTER><H3>NO OTM TO EDIT</H3></CENTER>"; }
else {

$oid = $otmid;
include ($base_folder . "/queries/queries_otm_general.h");
include ($base_folder . "/queries/queries_user.h");

if ((($ID == $otm_uid) && (strcmp($otm_year_month,$lookup_date))) || (($userLevel == 30) && ($user_coid == $otm_coid)) || (($userLevel == 20) && ($user_coid == $otm_crid)) || ($userLevel == 10) || ($userLevel == 1)) {

// include "form_edit_add_org_upd.h";

?>

<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<HTML>
<HEAD>
<TITLE>General OTM Nomination Form</TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../stylesheet.css'>
<SCRIPT LANGUAGE="JavaScript">

function wordCounter(string, countfield) {
   var re = /\s+/g;
   var words = string.split(re);
   countfield.value = words.length;
}

</script>



</HEAD>
<?
echo "<CENTER><H3>$message</H3>\r\n";
echo "<H2>General OTM Nomination Form</H2>\r\n";

//MIGHT NOT BE IMPLEMENTED IN FINAL RELEASE
if (($userLevel == 1) || ($userLevel == 3)) {
//include "form_edit_add_org.h";
}

echo "<TABLE class='box-base' BORDER='0' CELLSPACING='0' CELLPADDING='8' WIDTH='620'>\r\n";
echo "<FORM ACTION='../form_general_update.php3' NAME='form1' METHOD='post'>\r\n";
echo "<INPUT TYPE='hidden' NAME='otm_year' VALUE='$otm_year'>\r\n";
echo "<INPUT TYPE='hidden' NAME='otm_month' VALUE='$otm_month'>\r\n";
echo "<INPUT TYPE='hidden' NAME='date_of_entry' VALUE='$otm_date_of_entry'>\r\n";
echo "<INPUT TYPE='hidden' NAME='caid' VALUE='$otm_award'>\r\n";
echo "<INPUT TYPE='hidden' NAME='uid' VALUE='$otm_uid'>\r\n";
echo "<INPUT TYPE='hidden' NAME='command' VALUE='$command'>\r\n";
echo "<INPUT TYPE='hidden' NAME='otmid' VALUE='$otmid'>\r\n";

if (($userLevel == 1) || ($userLevel == 10)  || ($userLevel == 20) || ($userLevel == 30)) {
echo "<TR><TD COLSPAN=2 align=center>\r\n";

    $query="SELECT * FROM config_awards";
    $ca_result=mysql_query($query);
    $ca_num=mysql_numrows($ca_result);
echo "<B>OTM Award:</B><BR>\r\n";
echo "<SELECT NAME='award' size='1'>\r\n";
echo "<OPTION VALUE='0'>No Award</OPTION>\r\n";

$i=0;
while ($i < $ca_num) {
    $otms_caid=mysql_result($ca_result,$i,"caid");
    $otms_award=mysql_result($ca_result,$i,"name");
    $otms_ar=mysql_result($ca_result,$i,"access_required");
if ($otm_caid == $otms_caid) {
echo "<OPTION VALUE='$otms_caid' SELECTED>$otms_award</OPTION>\r\n";
}
elseif (($userLevel <= $otms_ar) || ($userLevel == 1)) {
echo "<OPTION VALUE='$otms_caid'>$otms_award</OPTION>\r\n";
}
else {
}
++$i;
}
echo "</SELECT>\r\n";
echo "</TD></TR>\r\n";
}

if (($userLevel == 1)) {
echo "<TR><TD COLSPAN=2 align=center>\r\n";
echo "<B>OTM Award Level:</B><BR>\r\n";
echo "<SELECT NAME='award_level' size='1'>\r\n";

echo "<OPTION VALUE='1'";
if ($otm_award_level == 1) { echo " SELECTED"; }
echo ">Campus</OPTION>\r\n";
echo "<OPTION VALUE='2'";
if ($otm_award_level == 2) { echo " SELECTED"; }
echo ">Regional</OPTION>\r\n";
echo "<OPTION VALUE='3'";
if ($otm_award_level == 3) { echo " SELECTED"; }
echo ">National</OPTION>\r\n";

echo "</SELECT>\r\n";
echo "</TD></TR>\r\n";
} else { echo "<INPUT TYPE='hidden' NAME='award_level' VALUE='$otm_award_level'>\r\n"; }


echo "<TR>\r\n";

?>
<TD WIDTH=300 align=center>
<B>Month:</B><BR>
<?
if (($userLevel == 1) || ($userLevel == 20) || ($userLevel == 30)) { /// ************************************************
$m = 0;

include ($base_folder . "/queries/queries_lu_months.h");

echo "<SELECT NAME='otm_month' size='1'>\r\n";
while ($m < $lum_num) {
$mname=mysql_result($lum_result,$m,"name");
$mnumber=mysql_result($lum_result,$m,"lumid");

if ($otm_month == $mnumber) {
echo "<OPTION VALUE='$mnumber' SELECTED>$mname</OPTION>\r\n";
}
else {
echo "<OPTION VALUE='$mnumber'>$mname</OPTION>\r\n";
}
++$m;
}
echo "</SELECT>\r\n";
}
else {
echo $otm_month_name;
}

?>
</TD>

<TD WIDTH=300 align=center>
<B>Year:</B><BR>
<?
if (($userLevel == 1) || ($userLevel == 20) || ($userLevel == 30)) { /// ************************************************
echo "<INPUT NAME='otm_year' SIZE='4' maxlength='4' value='$otm_year'><P>\r\n";
}
else {
echo $otm_year;
}
?>
</TD>
</TR>

<TR>
<TD COLSPAN=2>
<CENTER><B> Category:</B><BR>
<SELECT NAME="otm_category" size="1">
<OPTION VALUE="">(Select Category)</OPTION>
<?
$category_query="SELECT * FROM config_categories WHERE category_type='general' AND active='1' order by name";
$conf_cat_result=mysql_query($category_query);
$category_num=mysql_numrows($conf_cat_result); 

$i=0;
while ($i < $category_num) {
$cat_ccid=mysql_result($conf_cat_result,$i,"ccid");
$cat_name=mysql_result($conf_cat_result,$i,"name");
	if ((in_array ($cat_ccid, $ar_cat))) {
		if ($cat_ccid == $otm_ccid) {
			echo "<OPTION VALUE='$cat_ccid' SELECTED>$cat_name</OPTION>\r\n";
		} else {
			echo "<OPTION VALUE='$cat_ccid'>$cat_name</OPTION>\r\n";
		}
	} else {

	}
	++$i;
}
?>
</SELECT>
</TD>
</TR>

<TR>

<TD align=center>
<B>Nominee's School:</B><BR>
<? echo $school; ?>
</TD>

<TD align=center>
<B>Region:</B><BR>
<? echo $region; ?>
</TD>
</TR>

<TR>
<TD COLSPAN=2 ALIGN=CENTER>
<HR>
</TD>
</TR>

<? if (($otm_sub_org_id > 0) && ($otm_rec_org_id > 0 )) {
$coid = $otm_coid;

include ($base_folder . "/queries/queries_org_sub.h");
echo "<TR><TD><CENTER><b>Nominee's organization:</b><BR>\r\n";
echo "<SELECT NAME='rec_org'>\r\n";
echo "<OPTION VALUE=''>(Select Organization)</OPTION>\r\n";

$i=0;
while ($i < $org_sub_num) {
$csoid=mysql_result($org_sub_result,$i,"csoid");
$org_name=mysql_result($org_sub_result,$i,"name");


if ($otm_rec_org_id == $csoid) {
echo "<OPTION VALUE='$csoid' SELECTED>$org_name</OPTION>\r\n";
}
else {
echo "<OPTION VALUE='$csoid'>$org_name</OPTION>\r\n";
}
++$i;
}

echo "</SELECT></TD><TD>\r\n";
echo "<CENTER><b>Nominator's organization:</b><BR>\r\n";
echo "<SELECT NAME='sub_org'>\r\n";
echo "<OPTION VALUE=''>(Select Organization)</OPTION>\r\n";

$i=0;
while ($i < $org_sub_num) {
$csoid=mysql_result($org_sub_result,$i,"csoid");
$org_name=mysql_result($org_sub_result,$i,"name");
if ($otm_sub_org_id == $csoid) {
echo "<OPTION VALUE='$csoid' SELECTED>$org_name</OPTION>\r\n";
}
else {
echo "<OPTION VALUE='$csoid'>$org_name</OPTION>\r\n";
}
++$i;

}

echo "</CENTER></TD></TR>\r\n";

}

echo "<TR><TD>\r\n";
echo "<b>Nominee:</b><BR><INPUT NAME='nominee' SIZE='35' maxlength='50' value='$otm_nominee'><P>\r\n";
echo "<B>Address:</B><BR>\r\n";
echo "<INPUT NAME='nominee_address_1' SIZE='35' maxlength='35' value='$otm_nominee_address_1'><BR>\r\n";
echo "<INPUT NAME='nominee_address_2' SIZE='35' maxlength='35' value='$otm_nominee_address_2'><BR>\r\n";
echo "<INPUT NAME='nominee_address_3' SIZE='35' maxlength='35' value='$otm_nominee_address_3'><P>\r\n";
echo "<B>Phone:</B><BR>\r\n";
echo "<INPUT NAME='nominee_phone' SIZE='15' VALUE='$otm_nominee_phone' maxlength='12'><P>\r\n";
echo "<B>Email Address:</B><BR>\r\n";
echo "<INPUT NAME='nominee_email' SIZE='35' maxlength='40' value='$otm_nominee_email'>\r\n";
echo "<P><CENTER><b>On-Campus Population:</b><BR>\r\n";
echo "$campus_pop\r\n";
echo "</CENTER></TD>\r\n";

echo "<TD>\r\n";

echo "<B>Nominator:</B><BR><INPUT NAME='nominator' SIZE='35' maxlength='50' value='$otm_nominator'><P>\r\n";
echo "<B>Address:</B><BR>\r\n";
echo "<INPUT NAME='nominator_address_1' SIZE='35' maxlength='35' value='$otm_nominator_address_1'><BR>\r\n";
echo "<INPUT NAME='nominator_address_2' SIZE='35' maxlength='35' value='$otm_nominator_address_2'><BR>\r\n";
echo "<INPUT NAME='nominator_address_3' SIZE='35' maxlength='35' value='$otm_nominator_address_3'><P>\r\n";
echo "<B>Phone:</B><BR>\r\n";
echo "<INPUT NAME='nominator_phone' VALUE='$otm_nominator_phone' SIZE='15' maxlength='12'><P>\r\n";
echo "<B>Email Address:</B><BR>\r\n";
echo "<INPUT NAME='nominator_email' SIZE='35' maxlength='40' value='$otm_nominator_email'><P>\r\n";
echo "<CENTER><b>Chapter Size:</b><BR>\r\n";
echo "$chapter_size</CENTER>";
?>
</TD></TR>

<TR>
<TD COLSPAN=2 ALIGN=CENTER>
<HR>
<p>Please explain the <b>outstanding</b> contributions of the nominee <b>during
the month of nomination</b><BR>(i.e., how the nominee addressed recognition,
motivation, and support for you or your organization).</p>
<HR>
</TD>
</TR>

<TR>
<TD COLSPAN=2 ALIGN=CENTER>
<TEXTAREA NAME="nomination" WRAP=HARD ROWS=25 COLS=72>
<? 
$otm_nomination = ereg_replace("(\r\n\r\n)", "<P>", $otm_nomination);
$otm_nomination = ereg_replace("(\r\n\t)", "<P>", $otm_nomination);
$otm_nomination = ereg_replace("(\r\n)", "", $otm_nomination); 
$otm_nomination = ereg_replace("(<P>)", "\r\n\r\n", $otm_nomination);

echo $otm_nomination 
?>
</TEXTAREA>
<p><b>Word Count <font size="2">(600 Maximum)</font>:</b><BR>
<? echo "<INPUT NAME='word_count' SIZE='20' maxlength='3' value='$otm_word_count'>\r\n"; ?>
<BR>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.nomination.value,this.form.word_count);"></p>
<HR SIZE=4>
</Center>

<P>World Wide Web nominations must be typed on this form and <b>must not exceed 600 words</b>.<BR>
Nominations not following this format may not be considered for a national award.<BR>
No additional material will be considered. </P>
<p align="right">
Revised May 1999
<HR SIZE=4>
</TD>
</TR>

<?
include ($base_folder . "/form_blurb.h");

echo "<INPUT TYPE='hidden' NAME='school' VALUE='$school'>\r\n";
echo "<INPUT TYPE='hidden' NAME='cuid' VALUE='$otm_coid'>\r\n";
echo "<INPUT TYPE='hidden' NAME='region' VALUE='$region'>\r\n";
echo "<INPUT TYPE='hidden' NAME='crid' VALUE='$otm_crid'>\r\n";

echo "<INPUT TYPE='hidden' NAME='campus_pop' VALUE='$campus_pop'>\r\n";
echo "<INPUT TYPE='hidden' NAME='chapter_size' VALUE='$chapter_size'>\r\n";



?>

<TR><TD COLSPAN=2 ALIGN=CENTER><INPUT TYPE="button" VALUE="Save" OnClick="mysave(form1)"><INPUT TYPE="button" VALUE="Cancel" VALUE="Clear Form" OnClick="mycancel(form1)"><P></TD></TR>
</TABLE>
</TD>
</TR>

</TABLE>
</CENTER>

</FORM><script language="JavaScript" type="text/javascript">
function isempty(type) {
	if(type.length == 0) {
		return true
	}

	return false
}
function mycancel(form1) {
	form1.action = "/login/main.php3";
	form1.submit();

}
function mysave(form1) {
var str

        if(isempty(form1.otm_category.value)) {
                alert("Error: Category is required.")
                form1.otm_category.focus()
		return false
        }
<? if ($allow_suborgs == 1) { ?>
        if(isempty(form1.rec_org.value)) {
                alert("Error: Receiving Organization is required.")
                form1.rec_org.focus()
		return false
        }
        if(isempty(form1.sub_org.value)) {
                alert("Error: Submitting Organization is required.")
                form1.sub_org.focus()
		return false
        }
<? } ?>
        if(isempty(form1.nominee.value)) {
                alert("Error: Nominee is required.")
                form1.nominee.focus()
		return false
        }
        if(isempty(form1.nominator.value)) {
                alert("Error: Nominator is required.")
                form1.nominator.focus()
		return false
        }
        if(isempty(form1.nomination.value)) {
                alert("Error: A Nomination is required.")
                form1.nomination.focus()
		return false
        } 

<? if (($otm_blurb_local == 1) || ($otm_blurb_reg == 1)) { ?>
        if(isempty(form1.otm_blurb.value)) {
                alert("Error: A Short Summary is required.")
                form1.otm_blurb.focus()
		return false
        } 
<? } ?>


                wordCounter(form1.nomination.value,form1.word_count);

if (form1.command.value == "update") {
        URL = "../form_general_submitted.php3?action=" + form1.command.value + "&otmid=" + form1.otmid.value
} else { URL = "../form_general_submitted.php3" } 
	form1.action = URL
        form1.submit();	


}
</script>
</BODY>


</HTML>
<?
}

else {
echo $user_coid;
echo $otms_coid;

 echo "YOU ARE NOT ALLOWED TO EDIT THIS OTM"; }
}
