# README #

OTMS

### Andrew's Original Email ###

* The website is configured using frames
* There are two sections of the website – one section that does not require login and one that does.  The section that does has all the files under the “login” folder
* The admin folder contains files that are set up to run as nighly cron jobs – which primarily help with some reporting and also sending out email notifications to the nominees, etc.
* The phpsecurepages folder is what handles the authentication checks – it was a free plugin, but assume you’ll use something else.
* The config folder contained a file with some basic configuration information which was called from each php file.  It also contains some other routines that were reused throughout the system.
* The queries folder contained all (most) of the sql queries that were reused throughout the system

The two most difficult things that I ran into when building the site:

1.	The entire process is driven by dates

   Campus, regional, and national admins configure dates within the system to determine when OTMs are due and when they are viewable

   This drives which month people are submitting an OTM for (a campus that has their OTMs due on May 10th, if they are submitting on May 9th, they would be submitting for the month of April 2019… but if they submitted May 11th, they are submitting for May 2019).

   This also drives what people see when they log into the system in order to help people have the most important and actionable information at their fingertips.

   This was challenging because I did not have the admins assign dates for each year, but they carried over from year-over-year… which meant I had to derive calculations at the end of the year to determine which year should be shown, etc.  (I can explain more if needed…but in essence, it’s a challenge to derive and assign the year when you allow campuses flexibility into when OTMs are due… )
2.	The most important and challenging file is the login/main.php file, as this drives what data people see when they log into the system.  It is quite lengthy as it goes through many different scenarios.  When I rolled out the system initially, I went through a bunch of different scenarios to test to ensure that this worked correctly.  Unfortunately the code is not documented very well and in some cases, I questioned why it was needed. 

   There is a section for each userLevel and what they should see (see below for list of userLevels)… and also calculate whether they are in the judging period, what columns they should see (aka, should they be seeing voting options right now, etc.)  

   The views also change for a campus/regional admin after due dates I believe… 

   And the views for regional/national admins also change to allow them to pull forward OTMs… aka, just setting an OTM to “regional winner” does not actually “SUBMIT” it to the next level.. people had to use the views within the system to forward it on.  This caught some people over the years which is why I allowed the next level to “pull” them. 

#####Other comments
* The show-table.h file is called many times from multiple files – was done this way to drive consistency in the table formatting for search results, the main.php file upon login, etc.

#####The database 
* Anything starting with “config” drove some type of configuration – some of it is not editable by users… some is (aka, config_orgs) 
* OTM records are split into two files – primarily due to searching reasons as people can search all the basic fields that are consistent across the two types of OTMs from a single table and then when they click on a record, it joins the records across two tables due to different information collected.
* The VID attributes in the tables is a randomly generated code which was implemented to help prevent people from being able to figure out how to edit the URLs and get access to data, etc.
* On the OTM & USERS table… address information is unfortunately not split out by city, state, zip, etc… and is all one field.
* Anything starting with “logs” was used to help with troubleshooting random questions / issues… loginattempts captured failure codes (some people would complain that they were typing everything in correctly, not read the error messages, and so I would have to go back and take a look and explain why it wasn’t working)
* I don’t think Otms_feedback was ever implemented… 
* Anything starting with “stats” was used to aggregate statistical data each night so the system wouldn’t have to churn through all the records for the analytics/reports during each run


