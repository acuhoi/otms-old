<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<HTML>
<HEAD>
<TITLE>Account Info Form</TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='/stylesheet.css'>

</HEAD>
<FORM ACTION='/jserror.php' NAME='form1' METHOD='post'>
<?
echo "<INPUT TYPE='hidden' NAME='user_active' VALUE='$user_active'>\r\n";
echo "<INPUT TYPE='hidden' NAME='userlevel' VALUE='$user_userlevel'>\r\n";
echo "<INPUT TYPE='hidden' NAME='coid' VALUE='$coid'>\r\n";
echo "<INPUT TYPE='hidden' NAME='command' VALUE='$command'>\r\n";
echo "<INPUT TYPE='hidden' NAME='uid' VALUE='$uid'>\r\n";
?>

<CENTER>
<H2>Account Info Form</H2>

<TABLE class='box-base' BORDER='0' CELLSPACING='0' CELLPADDING='8' WIDTH='620'>
<TR>
<TH WIDTH=200 align=left><B><U>Field</b></TD>
<TH WIDTH=200 align=center><U>Your Input</TD>
</TR>


<?
include ($base_folder . "/queries/queries_org.h");

echo "<TR><TD WIDTH=200 align=left><b>University</b></TD><TD WIDTH=200 align=center>\r\n";
echo $conf_org_name;

if ($ID) {
	echo "<BR><a href='change_info_coid.php?uid=$uid'>Request Change of University</a></BR>";
}

if ($allow_suborgs == 1) {

echo "<TR><TD WIDTH=200 align=left><b>Your Default Submitting Organization</b></TD><TD WIDTH=200 align=center>\r\n";
echo "<SELECT NAME='dsoid'>\r\n";
echo "<OPTION VALUE=''>(Select Organization)</OPTION>\r\n";

include ($base_folder . "/queries/queries_org_sub_active.h");
$i=0;
while ($i < $org_sub_num) {
	$org_sub_csoid=mysql_result($org_sub_result,$i,"csoid");
	$org_sub_name=mysql_result($org_sub_result,$i,"name");
	if ($user_dsoid == $org_sub_csoid) {
		echo "<OPTION VALUE='$org_sub_csoid' SELECTED>$org_sub_name</OPTION>\r\n";
	} else {
		echo "<OPTION VALUE='$org_sub_csoid'>$org_sub_name</OPTION>\r\n";
	}
	++$i;
}
echo "</SELECT></CENTER><BR>\r\n";
$coid=$pcoid;
}

?>

<TR>
<TD WIDTH=200 align=left><B>First Name</b></TD>
<? echo "<TD WIDTH=200 align=center><INPUT NAME='fname' SIZE='35' maxlength='35' VALUE='$user_fname'></TD>\r\n"; ?>
</TR>

<TR>
<TD WIDTH=200 align=left><b>Last Name</b></TD>
<? echo "<TD WIDTH=200 align=center><INPUT NAME='lname' SIZE='35' maxlength='35' VALUE='$user_lname'></TD>\r\n"; ?>
</TR>

<TR>
<TD WIDTH=200 align=left valign=top><b>Address</b></TD>
<? echo "<TD WIDTH=200 align=center><INPUT NAME='address_1' SIZE='35' maxlength='35' VALUE='$user_address_1'><BR>\r\n"; ?>
<? echo "			<INPUT NAME='address_2' SIZE='35' maxlength='35' VALUE='$user_address_2'><BR>\r\n"; ?>
</TR>


<TR>
<TD WIDTH=200 align=left valign=top><b>City</b></TD>
<? echo "<TD WIDTH=200 align=center><INPUT NAME='city' SIZE='35' maxlength='35' VALUE='$user_city'><BR>\r\n"; ?>
</TR>

<TR>
<TD WIDTH=200 align=left valign=top><b>State / Province</b></TD>   
<? echo "<TD WIDTH=200 align=center><INPUT NAME='state' SIZE='35' maxlength='2' VALUE='$user_state'><BR>\r\n"; ?>          
</TR>

<TR>
<TD WIDTH=200 align=left valign=top><b>Zip</b></TD>   
<? echo "<TD WIDTH=200 align=center><INPUT NAME='zip' SIZE='35' maxlength='7' VALUE='$user_zip'><BR>\r\n"; ?>          
</TR>


<TR>
<TD WIDTH=200 align=left><b>Phone</b></TD>
<? echo "<TD WIDTH=200 align=center><INPUT NAME='phone' SIZE='35' maxlength='12' VALUE='$user_phone'></TD>\r\n"; ?>
</TR>

<TR>
<TD WIDTH=200 align=left><b>E-Mail Address</b></TD>
<? echo "<TD WIDTH=200 align=center><INPUT NAME='email' SIZE='35' maxlength='50' VALUE='$user_email'></TD>\r\n"; ?>
</TR>

<TR>
<TD WIDTH=200 align=left><b>Username<BR></b></TD>
<TD WIDTH=200 align=center>
<? 

if (($command == "NEW") || (($command == "UPDATE") && (($userLevel == 1) || ($userLevel == 10) || ($userLevel == 20) || ($userLevel == 30)))) {
echo "<INPUT NAME='username' SIZE='35' maxlength='35' VALUE='$user_username'></TD>\r\n"; 
} else {
echo $user_username;
echo "<INPUT TYPE='hidden' NAME='username' VALUE='$user_username'>\r\n";
}
?>

</TR>

<TR>
<TD WIDTH=200 align=left><b>Password</b></TD>
<? echo "<TD WIDTH=200 align=center><INPUT NAME='upassword' SIZE='35' maxlength='35' type='password'></TD>\r\n"; ?>
</TR>

<TR>
<TD WIDTH=200 align=left><b>Re-type Password</b></TD>
<? echo "<TD WIDTH=200 align=center><INPUT NAME='upassword_verify' SIZE='35' maxlength='35' type='password'></TD>\r\n"; ?>
</TR>
<?
if ($userLevel == 1) {
echo "<TR>
<TD WIDTH=200 align=left><b>Password Hash</b></TD><TD> $user_password</TD>
<TD WIDTH=200 align=center><i></i></TD>
</TR>";

}

	if (($userLevel == 1) || ($command == "UPDATE")) {
		echo "<TR><TD COLSPAN=3 ALIGN=CENTER><B>Are you changing your password?</B><input type='checkbox' name='chkp' value='1'></TD></TR>\r\n";
	} else { 
		echo "<INPUT TYPE='hidden' NAME='chkp' VALUE='1'>\r\n";
	}

	if (($ID == $uid) || ($userLevel == 1)) {
		echo "<TR><TD ALIGN=LEFT><B>Navbar Location</TD></B><TD>\r\n";
		if ($user_gui == "left") {
			echo "<input type='radio' name='user_gui' value='left' CHECKED> Left (Text-Mode)  <input type='radio' name='user_gui' value='top'> Top (GUI-Mode)\r\n"; }
			elseif ($user_gui == "top") { echo "<input type='radio' name='user_gui' value='left'> Left (Text-Mode) <input type='radio' name='user_gui' value='top' CHECKED> Top (GUI-Mode)\r\n"; }
			else { echo "<input type='radio' name='user_gui' value='left'> Left (Text-Mode) <input type='radio' name='user_gui' value='top'> Top (GUI-Mode)\r\n"; }

	echo "<BR>(Setting takes affect on next login)</TD></TR>\r\n";
	}


if (($userLevel == 1 || $userLevel == 10 || $userLevel == 20 || $userLevel == 30)) {
   echo "<TR><TD ALIGN=LEFT><B>Access</B></TD><TD>\r\n";
   echo "<SELECT name='userlevel'>\r\n";

include ($base_folder . "/queries/queries_user_types.h");

$i=0;
while ($i < $conf_ut_num) {

$conf_ut_cutid=mysql_result($conf_ut_result,$i,"cutid");
$conf_ut_name=mysql_result($conf_ut_result,$i,"name");

if ($conf_ut_cutid == $user_userlevel) {
echo "<OPTION VALUE='$conf_ut_cutid' SELECTED>$conf_ut_name</OPTION>\r\n";
}
elseif ($userLevel <= $conf_ut_cutid) {
echo "<OPTION VALUE='$conf_ut_cutid'>$conf_ut_name</OPTION>\r\n";
}

++$i;

}


echo "</SELECT>\r\n";
echo "</TD></TR>\r\n";

if ((($userLevel == 1) || ($userLevel == 20)) && ($uid != $ID)) {
	echo "<TR><TD ALIGN=LEFT><B>Regional OTM Committee?</TD></B><TD>\r\n";
	if ($user_reg_voting == 1) {
		echo "<input type='radio' name='user_reg_voting' value='1' CHECKED> Yes  <input type='radio' name='user_reg_voting' value='0'> No\r\n"; }
		else { echo "<input type='radio' name='user_reg_voting' value='1'> Yes <input type='radio' name='user_reg_voting' value='0' CHECKED> No\r\n"; }
		echo "</TD></TR>\r\n";
	} else {
		echo "<INPUT TYPE='hidden' NAME='user_reg_voting' VALUE='0'>\r\n";
}

echo "<TR><TD ALIGN=LEFT><B>Active</TD></B><TD>\r\n";
if ($user_active == 1) {
echo "<input type='radio' name='user_active' value='1' CHECKED> Yes  <input type='radio' name='user_active' value='0'> No\r\n"; }
else { echo "<input type='radio' name='user_active' value='1'> Yes <input type='radio' name='user_active' value='0' CHECKED> No\r\n"; }
echo "</TD></TR>\r\n";
}
else { 

}

?>
<TR><TD COLSPAN=3 ALIGN=CENTER>
<?
if ($command == "UPDATE") {
echo "<INPUT TYPE='button' VALUE='Update' OnClick='mysave(form1)'>\r\n";
}
else {
echo "<INPUT TYPE='button' VALUE='Submit' OnClick='mysave(form1)'><INPUT TYPE='reset' VALUE='Clear Form'><P></TD></TR>\r\n";
}
?>
</TABLE>
</CENTER>
</FORM><script language="JavaScript" type="text/javascript">
function isempty(type) {
	if(type.length == 0) {
		return true
	}

	return false
}

function mysave(form1) {
var str
<?
if ($allow_suborgs == "1") {
echo "        if(isempty(form1.dsoid.value)) {\r\n";
echo "                alert('Error: A Default Sub Organization is required.')\r\n";
echo "                form1.dsoid.focus()\r\n";
echo "		return false\r\n";
 echo "       }\r\n";
}
?>
        if(isempty(form1.fname.value)) {
                alert("Error: Your First Name is required.")
                form1.fname.focus()
		return false
        }
        if(isempty(form1.lname.value)) {
                alert("Error: Your Last Name is required.")
                form1.lname.focus()
		return false
        }
        if(isempty(form1.address_1.value)) {
                alert("Error: Your Address is required.")
                form1.address_1.focus()
		return false
        }
        if(isempty(form1.city.value)) {
                alert("Error: Your City is required.")
                form1.city.focus()
                return false
        }
        if(isempty(form1.state.value)) {
                alert("Error: Your State is required.")
                form1.state.focus()
                return false
        }

        if(isempty(form1.zip.value)) {
                alert("Error: Your Zip is required.")
                form1.zip.focus()
                return false
        }
        if(isempty(form1.phone.value)) {
                alert("Error: Your Phone # is required.")
                form1.phone.focus()
		return false
        }
        if(isempty(form1.email.value)) {
                alert("Error: Your E-Mail Address is required.")
                form1.email.focus()
		return false
        } 
        if(isempty(form1.username.value)) {
                alert("Error: A username is required.")
                form1.username.focus()
		return false
        }
if (form1.chkp.checked == true) {
        if(isempty(form1.upassword.value)) {
                alert("Error: A Password is required.")
                form1.upassword.focus()
		return false
        } 
        if(isempty(form1.upassword_verify.value)) {
                alert("Error: Please re-type your password.")
                form1.upassword.focus()
		return false
	}
}
        if((form1.upassword.value != form1.upassword_verify.value)) {
                alert("Error: Passwords do not match.")
                form1.upassword.focus()
		return false
        } 

if ((form1.command.value == "UPDATE") && (form1.chkp.checked == true)) {
         URL = "/login/admin_users.php3?action=" + form1.command.value + "&uid=" + form1.uid.value + "&pass=1"
}
else if ((form1.command.value == "UPDATE") && (form1.chkp.checked == false)) {
         URL = "/login/admin_users.php3?action=" + form1.command.value + "&uid=" + form1.uid.value
} else { URL = "/form_create_account_submitted.php3" }
	form1.action = URL
        form1.submit();	
}
</script>
</BODY></HTML>


