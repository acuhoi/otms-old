<?
include("/home/httpd/nrhh/otms/config/dbinfo.inc.php");
$server="otms.nrhh.org";


        $today_minute = intval(date("i"));
        $today_hour = intval(date("H"));
        $today_date = intval(date("d"));
        $today_month = intval(date("m"));
        $today_year = intval(date("Y"));

if (($today_hour > 2) && ($today_hour < 22)) {

        if ($today_month < 10) {
                $today_month = "0" . $today_month;
        }
        if ($today_date < 10) {
                $today_date = "0" . $today_date;
        }

	$hid_year=$today_year-1;

$otms_query = "
        SELECT *,
	if (config_orgs.allow_presubmit=1,concat( if (substring(otms.`year_month`,1,7)='$hid_year-12','" . $today_year . "-',left( otms.`year_month`, 5)), config_dates.month_due,  '-', 
config_dates.date_due,  ' ', 
config_dates.hour_due, ':', config_dates.minute_due,  ':00'),concat( if (substring(otms.`year_month`,1,7)='$hid_year-12','" . $today_year . "-',left( otms.`year_month`, 5)), cd2.month_due,  '-',
cd2.date_due,  ' ',
cd2.hour_due, ':', cd2.minute_due,  ':00')) as due
 	FROM otms
        LEFT JOIN config_categories ON otms.ccid=config_categories.ccid
        LEFT JOIN lu_months ON if(substring(otms.`year_month`,6,1)='0',replace(substring(otms.`year_month`,6,2),'0',''),substring(otms.`year_month`,6,2))=lu_months.lumid
        LEFT JOIN config_orgs ON otms.coid=config_orgs.coid
        LEFT JOIN config_orgs co2 ON otms.region=co2.coid
        LEFT JOIN config_orgs_sub so ON otms.sub_org=so.csoid
        LEFT JOIN config_orgs_sub ro ON otms.rec_org=ro.csoid
        LEFT JOIN config_awards ON otms.award=config_awards.caid
	LEFT JOIN config_dates ON otms.coid=config_dates.coid and if(substring(otms.`year_month`,6,1)='0',replace(substring(otms.`year_month`,6,2),'0',''),substring(otms.`year_month`,6,2))=config_dates.month
	LEFT JOIN config_dates cd2 on otms.region=cd2.coid and if(substring(otms.`year_month`,6,1)='0',replace(substring(otms.`year_month`,6,2),'0',''),substring(otms.`year_month`,6,2))=cd2.month 
	WHERE config_categories.send_email='1' and rtrim(ltrim(otms.nominee_email)) REGEXP '^[a-zA-Z0-9][a-zA-Z0-9._-]*[a-zA-Z0-9]@[a-zA-Z0-9][a-zA-Z0-9._-]*[a-zA-Z0-9]\.[a-zA-Z]{2,4}$' and if 
(config_orgs.allow_presubmit=1,concat( if (substring(otms.`year_month`,1,7)='$hid_year-12','" . $today_year . "-',left( otms.`year_month`, 5)), config_dates.month_due,  '-', 
config_dates.date_due,  ' ',
config_dates.hour_due, ':', config_dates.minute_due,  ':00'),concat( if (substring(otms.`year_month`,1,7)='$hid_year-12','" . $today_year . "-',left( otms.`year_month`, 5)), cd2.month_due,  '-',         
cd2.date_due,  ' ',         
cd2.hour_due, ':', cd2.minute_due,  ':00')) 
< '$today_year-$today_month-$today_date $today_hour:$today_minute:00' and (date_of_entry BETWEEN SYSDATE() - INTERVAL 45 DAY AND SYSDATE()) and email_sent is NULL 
order by date_of_entry asc 
limit 200";


$query = $otms_query;

//echo $query;


$result=mysql_query($query);
$num=mysql_numrows($result); 


$subject="NRHH Of The Month Nomination";

$i=0;
while ($i < $num) {

$category_type=mysql_result($result,$i,"config_categories.category_type");
$nominee_email=trim(mysql_result($result,$i,"otms.nominee_email"));
$otms_vid=mysql_result($result,$i,"otms.vid");
$email_address=$nominee_email;
$otms_id=mysql_result($result,$i,"otms.oid");
$chapter_url=mysql_result($result,$i,"config_orgs.nrhhwebaddress");

$message = "
Dear \"Of the Month\" Nominee,<BR><BR>

Congratulations!  You have been nominated for an \"Of the Month\" (OTM) award for your outstanding contributions within your residential community.   OTMs are submitted to recognize individuals, like 
you, who have excelled in leadership qualities and contributed to the residence hall system.  <a 
href='https://otms.nrhh.org/otm_$category_type.php3?otmid=$otms_id&vid=$otms_vid'>Click here</A> if 
you are interested in reading the nomination submitted.
<BR><BR>
The OTM program is sponsored by the National Residence Hall Honorary (NRHH), an organization representing the top 1% of leaders within the residence hall community who strive to achieve excellence in 
scholastics, role models of service, bestowers of recognition, and exercisers of leadership.  NRHH acknowledges those leaders who go above and beyond in making the collegiate experience of living in 
the residence hall community an incredible experience.
<BR><BR>
To learn more about NRHH and the OTM recognition program, please go to <a href='http://www.nrhh.nacurh.org'>www.nrhh.nacurh.org</A> and/or contact your ";


if ($chapter_url != "" ) {
	 $message = $message . "<a href=http://$chapter_url'>NRHH Chapter</A>";
} else {
	 $message = $message . "NRHH chapter or residence life organization";
}
$message = $message . ".  <BR><BR>Again, congratulations on your nomination and best of luck on the campus, regional, and national levels.<BR><BR>";

include($base_folder . "/send_email_html.php");

$sql = "UPDATE otms SET ";      
$sql = $sql . "email_sent='1' ";
$sql = $sql . "WHERE oid='$otms_id'";  
$sql_result= mysql_query($sql,$connection) or die ("Could not execute update query 1 " . mysql_error());

$sql = "INSERT INTO logs_emails(email_type,id) values('nominee','$otms_id')";
$db= mysql_select_db($dbdatabase,$connection) or die ("Unable to select database." .  mysql_error());
$sql_result= mysql_query($sql,$connection) or die ("Could not execute query 1 " . mysql_error());
sleep(15);
++$i;
}
        $msg = "Completed email nomination email cycle, $i,$today_year-$today_month-$today_date $today_hour:$today_minute:00 \r\n";
        echo $msg;

} else {
        $msg = "email nomination cycle did not run,0,$today_year-$today_month-$today_date $today_hour:$today_minute:00\r\n";
        echo $msg;


}
?>
