<?

// local variables
$col_title = 85;
$col_text = 220;
$i=0;
?>

<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<HTML>
<HEAD>
<TITLE>Program OTM Nomination Form</TITLE>
<LINK REL='stylesheet' TYPE='text/css' HREF='../stylesheet.css'>
<script type="text/javascript">

function wordCounter(string, countfield) {
   var re = /\s+/g;
   var words = string.split(re);
   countfield.value = words.length;
}

</script>

</HEAD>
<FORM ACTION='/jserror.php' NAME='form1' METHOD='post'>
<CENTER>

<H2>Program OTM Nomination Form</H2>

<TABLE class='box-base' BORDER='0' CELLSPACING='0' CELLPADDING='8' WIDTH='620'>
<TR>
<TD WIDTH=300 align=center>
<B>Month:</B><BR>
<?
$lumid = $lookup_month;
include($base_folder . "/queries/queries_lu_month.h");
echo $month_name;
?>
</TD>

<TD WIDTH=300 align=center>
<B>Year:</B><BR>
<?
echo $show_year;
?>
</TD>
</TR>


<TR>
<TD COLSPAN=2>
<CENTER><B> Category:</B><BR>
<SELECT NAME="otm_category" size="1">
<OPTION VALUE="">(Select Category)</OPTION>
<?
$i=0;
while ($i < $category_num) {
$ccid=mysql_result($category_result,$i,"ccid");
$cat_name=mysql_result($category_result,$i,"name");
if ((in_array (intval($ccid), $ar_cat)) && (!in_array (intval($ccid), $poc_cat))) {
echo "<OPTION VALUE='$ccid'>$cat_name</OPTION>\r\n";
}
++$i;
}
?>
</SELECT>
<BR>
<? echo "<a href='/category_descriptions_specific.php?cat=$school_categories&type=$otm_type' target='_blank'>Click here for a description of the     
categories</A>"; ?>

</TD>
</TR>

<TR>

<TD align=center>
<B>Nominee's School:</B><BR>
<? echo $school;  ?>
</TD>

<TD align=center>
<B>Region:</B><BR>
<? echo $school_region_name; ?>
</TD>
</TR>

<TR>
<TD COLSPAN=2 ALIGN=CENTER>
<HR>
</TD>
</TR>

<TR>
<TD>

<?

if ($allow_suborgs == 1) {
include ($base_folder . "/queries/queries_org_sub_active.h");
echo "<CENTER><b>Nominee's organization:</b><BR>\r\n";
echo "<SELECT NAME='otm_rec_org'>\r\n";
echo "<OPTION VALUE=''>(Select Organization)</OPTION>\r\n";
$i=0;
while ($i < $org_sub_num) {
$org_sub_csoid=mysql_result($org_sub_result,$i,"csoid");
$org_sub_name=mysql_result($org_sub_result,$i,"name");

echo "<OPTION VALUE='$org_sub_csoid'>$org_sub_name</OPTION>\r\n";
++$i;
}
echo "</SELECT></CENTER><BR>\r\n";
}
?>

<b>Person in Charge:</b><BR><INPUT NAME="otm_person_in_charge" SIZE=35 maxlength="50"><P>
<B>Address:</B><BR>
<INPUT NAME="otm_nominee_address_1" SIZE=35 maxlength="35"><BR>
<INPUT NAME="otm_nominee_address_2" SIZE=35 maxlength="35"><BR>
<INPUT NAME="otm_nominee_address_3" SIZE=35 maxlength="35"><P>
<B>Phone:</B><BR>
<INPUT NAME="otm_nominee_phone" SIZE=15 VALUE="" maxlength="12"><P>
<B>Email Address:</B><BR>
<INPUT NAME="otm_nominee_email" SIZE=35 maxlength="50">
<P>
<CENTER>
<b>On-Campus Population:</b><BR>
<? 
if ($campus_pop != "") {
echo "$campus_pop</CENTER>";
echo "<INPUT TYPE='hidden' NAME='otm_campus_pop' VALUE='$campus_pop'>\r\n";
} else {
echo "<input name='otm_campus_pop' siz='15' maxlength='5'>";
}
?>
</CENTER>
</TD>

<TD>

<?
if ($allow_suborgs == 1) {
include ($base_folder . "/queries/queries_org_sub_active.h");
echo "<CENTER><b>Nominator's organization:</b><BR>\r\n";
echo "<SELECT NAME='otm_sub_org'>\r\n";
echo "<OPTION VALUE=''>(Select Organization)</OPTION>\r\n";
$i=0;
while ($i < $org_sub_num) {
$org_sub_csoid=mysql_result($org_sub_result,$i,"csoid");
$org_sub_name=mysql_result($org_sub_result,$i,"name");
if ($user_dsoid == $org_sub_csoid) {
echo "<OPTION VALUE='$org_sub_csoid' SELECTED>$org_sub_name</OPTION>\r\n";
}
else {
echo "<OPTION VALUE='$org_sub_csoid'>$org_sub_name</OPTION>\r\n";
}
++$i;
}
echo "</SELECT></CENTER><BR>\r\n";
}


if ($user_fname && $user_lname) {
$nominator = "$user_fname $user_lname";
} else { $nominator=""; }
echo "<B>Nominator:</B><BR><INPUT NAME='otm_nominator' SIZE='35' maxlength='50' value='$nominator'><P>\r\n";
echo "<B>Address:</B><BR>\r\n";
echo "<INPUT NAME='otm_nominator_address_1' SIZE='35' maxlength='35' value='$user_address_1'><BR>\r\n";
echo "<INPUT NAME='otm_nominator_address_2' SIZE='35' maxlength='35' value='$user_address_2'><BR>\r\n";
echo "<INPUT NAME='otm_nominator_address_3' SIZE='35' maxlength='35' value='$user_address_3'><P>\r\n";
echo "<B>Phone:</B><BR>\r\n";
echo "<INPUT NAME='otm_nominator_phone' VALUE='$user_phone' SIZE='15' maxlength='12'><P>\r\n";
echo "<B>Email Address:</B><BR>\r\n";
echo "<INPUT NAME='otm_nominator_email' SIZE='35' maxlength='50' value='$user_email'><P>\r\n";
echo "<CENTER><b>Chapter Size:</b><BR>\r\n";
if ($chapter_size != "") {
echo "$chapter_size</CENTER>";
echo "<INPUT TYPE='hidden' NAME='otm_chapter_size' VALUE='$chapter_size'>\r\n";
} else {
echo "<input name='otm_chapter_size' siz='15' maxlength='3'>";
}
?>
</TD></TR>

<TR>
<TD COLSPAN=2>
<CENTER><B>Program Title:</B><BR>
<INPUT NAME="otm_nominee" size="50" maxlength="50">
</TD>
</TR>

<TD>
<B>Target Population (in numbers):</B><BR>
<INPUT NAME="otm_target_pop" SIZE=35 maxlength="4">
<P>
<B>Number of People in Attendance:</B><BR>
<INPUT NAME="otm_attendance_pop" SIZE=35 maxlength="4">
<P>
<B>Number of People Needed to Organize:</B><BR>
<INPUT NAME="otm_organize_pop" SIZE=35 maxlength="4">
</TD>

<TD>
<B>Time Needed to Organize:</B><BR>
<INPUT NAME="otm_organize_time" SIZE=35 maxlength="35">
<P>
<B>Date(s) of Program:</B><BR>
<INPUT NAME="otm_date_of_program" SIZE=35 maxlength="50">
<P>
<B>Cost of Program:</B><BR>
<INPUT NAME="otm_cost_of_program" SIZE=35 maxlength="35">
</TD>
</TR>

<TR>
<TD COLSPAN=2 ALIGN=CENTER>
<HR>
<P><B>Origin of program:</b>
<TEXTAREA NAME="otm_origin" WRAP=HARD ROWS=5 COLS=72>
</TEXTAREA>
<p><b>Word Count (200 Maximum)</font>:</b><BR>
<INPUT NAME="otm_origin_count" SIZE=20 maxlength="3"><BR>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.otm_origin.value,this.form.otm_origin_count);"></p>


<HR>
<P><B>Please give a short description of program:</B>
<TEXTAREA NAME="otm_description" WRAP=HARD ROWS=20 COLS=70>
</TEXTAREA>
<p><b>Word Count (400 Maximum)</font>:</b><BR>
<INPUT NAME="otm_description_count" SIZE=20 maxlength="3"><BR>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.otm_description.value,this.form.otm_description_count);"></p>

<HR>
<P><B>Goals of program:</B>
<TEXTAREA NAME="otm_goals" WRAP=HARD ROWS=10 COLS=70>
</TEXTAREA>
<p><b>Word Count (200 Maximum)</font>:</b><BR>
<INPUT NAME="otm_goal_count" SIZE=20 maxlength="3"><BR>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.otm_goals.value,this.form.otm_goal_count);"></p>

<HR>
<P><B>Positive and lasting effects of the program:</B>
<TEXTAREA NAME="otm_effects" WRAP=HARD ROWS=10 COLS=70>
</TEXTAREA>
<p><b>Word Count <font size="2">(200 Maximum)</font>:</b><BR>
<INPUT NAME="otm_effect_count" SIZE=20 maxlength="3"><BR>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.otm_effects.value,this.form.otm_effect_count);"></p>

<HR>
<P><B>Short evaluation of the program:</B>
<TEXTAREA NAME="otm_evaluation" WRAP=HARD ROWS=10 COLS=70>
</TEXTAREA>
<p><b>Word Count <font size="2">(200 Maximum)</font>:</b><BR>
<INPUT NAME="otm_evaluation_count" SIZE=20 maxlength="3"><BR>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.otm_evaluation.value,this.form.otm_evaluation_count);"></p>

<HR>
<P><B>How could this program be adapted to other campuses?</B>
<TEXTAREA NAME="otm_adapted" WRAP=HARD ROWS=10 COLS=70>
</TEXTAREA>
<p><b>Word Count <font size="2">(200 Maximum)</font>:</b><BR>
<INPUT NAME="otm_adapt_count" SIZE=20 maxlength="3"><BR>
<input type="button" value="Calculate Words"
      onClick="wordCounter(this.form.otm_adapted.value,this.form.otm_adapt_count);"></p>
</Center>
</TD>
</TR>



<TR>
<TD COLSPAN=2 ALIGN=CENTER>

<HR SIZE=4>
</Center>

<P>World Wide Web nominations must be typed on this form and each area <BR>
<b>must not exceed maximum word count specified</b>.  Nominations not following <BR>
this format may not be considered for a national award.  No additional material <BR>
will be considered.  Nominations must be submitted by midnight on the <BR>
<? 
if ($allow_presubmit == 1) {
echo $today_local_date_due; 
}
else {
echo $today_regional_date_due; 
}
?>
th of the month following the month of nomination.</P>
<p align="right">Revised May 1999</FONT>
<HR SIZE=4>
</TD>
</TR>

<?

include ($base_folder . "/form_blurb.h");

echo "<INPUT TYPE='hidden' NAME='otm_month' VALUE='$lumid'>\r\n";
echo "<INPUT TYPE='hidden' NAME='month_name' VALUE='$month_name'>\r\n";
echo "<INPUT TYPE='hidden' NAME='school' VALUE='$univ'>\r\n"; // ??
echo "<INPUT TYPE='hidden' NAME='otm_coid' VALUE='$school_id'>\r\n";
echo "<INPUT TYPE='hidden' NAME='cuid' VALUE='$cuid'>\r\n";
echo "<INPUT TYPE='hidden' NAME='school_region_id' VALUE='$school_region_id'>\r\n";
echo "<INPUT TYPE='hidden' NAME='crid' VALUE='$crid'>\r\n";
echo "<INPUT TYPE='hidden' NAME='otm_year' VALUE='$show_year'>\r\n";
echo "<INPUT TYPE='hidden' NAME='uid' VALUE='$ID'>\r\n";

?>

<TR><TD COLSPAN=2 ALIGN=CENTER><INPUT TYPE="button" VALUE="Submit OTM" OnClick="mysave(form1)"><INPUT TYPE="reset" VALUE="Clear Form"><P></TD></TR>
</TABLE>
</TD>
</TR>

</TABLE>
</CENTER>

</FORM><script language="JavaScript" type="text/javascript">
function isempty(type) {
	if(type.length == 0) {
		return true
	}

	return false
}

function mysave(form1) {
var str

        if(isempty(form1.otm_category.value)) {
                alert("Error: Category is required.")
                form1.otm_category.focus()
		return false
        }
<? if ($allow_suborgs == 1) { ?>
        if(isempty(form1.otm_rec_org.value)) {
                alert("Error: Receiving Organization is required.")
                form1.otm_rec_org.focus()
		return false
        }
        if(isempty(form1.otm_sub_org.value)) {
                alert("Error: Submitting Organization is required.")
                form1.otm_sub_org.focus()
		return false
        }
<? } ?>
        if(isempty(form1.otm_nominee.value)) {
                alert("Error: A Program Title is required.")
                form1.otm_nominee.focus()
		return false
        }
        if(isempty(form1.otm_nominator.value)) {
                alert("Error: Nominator is required.")
                form1.otm_nominator.focus()
		return false
        }
        if(isempty(form1.otm_person_in_charge.value)) {
                alert("Error: A Nominee is required.")
                form1.otm_person_in_charge.focus()
		return false
        }
        if(isempty(form1.otm_origin.value)) {
                alert("Error: The origin of the program is required.")
                form1.otm_origin.focus()
		return false
        } 
        if(isempty(form1.otm_description.value)) {
                alert("Error: A short description of the program is required.")
                form1.otm_description.focus()
		return false
        } 
        if(isempty(form1.otm_goals.value)) {
                alert("Error: The goals of the program is required.")
                form1.otm_goals.focus()
		return false
        } 
        if(isempty(form1.otm_effects.value)) {
                alert("Error: Possitive and lasting effects of the program is required.")
                form1.otm_effects.focus()
		return false
        } 
        if(isempty(form1.otm_evaluation.value)) {
                alert("Error: A short evaluation of the program is required.")
                form1.otm_evaluation.focus()
		return false
        } 
        if(isempty(form1.otm_adapted.value)) {
                alert("Error: How this program could be adapted is required.")
                form1.otm_adapted.focus()
		return false
        } 
<? if (($otm_blurb_local == 1) || ($otm_blurb_reg == 1)) { ?>
        if(isempty(form1.otm_blurb.value)) {
                alert("Error: A Short Summary is required.")
                form1.otm_blurb.focus()
		return false
        } 
<? } ?>
                wordCounter(form1.otm_origin.value,form1.otm_origin_count);
                wordCounter(form1.otm_description.value,form1.otm_description_count);
                wordCounter(form1.otm_goals.value,form1.otm_goal_count);
                wordCounter(form1.otm_effects.value,form1.otm_effect_count);
                wordCounter(form1.otm_evaluation.value,form1.otm_evaluation_count);
                wordCounter(form1.otm_adapted.value,form1.otm_adapt_count);

	form1.action = "/form_program_submitted.php3"
	form1.submit();


}
</script>
</BODY>


</HTML>


