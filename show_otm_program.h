<?
// local variables
//$col_title = 85;
//$col_text = 220;
?>

<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<? FBOpener(); ?>
<HEAD>
<? echo "<TITLE>$cat_name OTM - $otm_nominee - $otm_month_name $otm_year</TITLE>"; ?>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<? FBMetaData($FBappID); ?>
<? echo $stylesheet; ?>
</HEAD>
<BODY>
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KVVFFC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KVVFFC');</script>
<!-- End Google Tag Manager -->
<? FBScript($FBappID) ?>

<TABLE class='' BORDER='0' CELLSPACING='0' CELLPADDING='8' WIDTH='820'><TR><TD>
<TABLE class='box-displayotm' BORDER='0' CELLSPACING='0' CELLPADDING='4' WIDTH='620'>
<?
echo "<tr><td colspan=2 class='box-displayotm-winner'><div align=left><b>$award</b></div></td>\r\n";
echo "<td colspan=2><div align=right><B>Month</B>:  $otm_month_name $otm_year</div></td></tr>\r\n";

echo "<tr><td colspan=4 class='box-displayotm-title'><div align=center><p><b>NACURH, INC.</font></b></p></div></td></tr>\r\n";


echo "<tr><td colspan=4 class='box-displayotm-title'><div align=center><b>$cat_name of the Month</b></div></td></tr>\r\n";

echo "<tr><td colspan=4></td></tr><TR><TD COLSPAN=4 class='box-displayotm-title'><div align=center><B>$otm_nominee</b></div></td></tr><tr><td colspan=4></td></tr>\r\n";

echo "<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>\r\n";
echo "<tr><td width=85 valign=top><b>School:</b></td>\r\n";
echo "<td width=$col_text>$school</td>\r\n";
echo "<td width=$col_title valign=top><b>Region:</b></td>\r\n";
echo "<td width=$col_text>$region</td></tr>\r\n";

echo "<tr><td width=$col_title valign=top><b>Person in charge:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_person_in_charge</td>\r\n";
echo "<td width=$col_title valign=top><b>Nominator:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_nominator</td></tr>\r\n";

if ($ID > 1) {
echo "<tr><td width=$col_title height=2 valign=top><b>Address:</b></td>\r\n";
echo "<td width=$col_text height=2>$otm_nominee_address_1<BR>$otm_nominee_address_2<BR>$otm_nominee_address_3</td>\r\n";
echo "<td width=$col_title height=2 valign=top><b>Address:</b></td>\r\n";
echo "<td width=$col_text height=2>$otm_nominator_address_1<BR>$otm_nominator_address_2<BR>$otm_nominator_address_3</td></tr>\r\n";

echo "<tr><td width=$col_title valign=top><b>Phone:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_nominee_phone</td>\r\n";
echo "<td width=$col_title valign=top><b>Phone:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_nominator_phone</td></tr>\r\n";
echo "<tr><td width=$col_title valign=top><b>Email:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_nominee_email</td>\r\n";
echo "<td width=$col_title valign=top><b>Email:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_nominator_email</td></tr>\r\n";
}

echo "<tr><td colspan=4></td></tr>\r\n";
echo "<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>\r\n";
echo "<tr><td colspan=2 valign=top><b>Target Population:</b> $otm_target_pop</td>\r\n";
echo "	   <td colspan=2 valign=top><b>Time Needed to Organize:</b> $otm_organize_time</td></tr>\r\n";
echo "<tr><td colspan=2 valign=top><b>Number of People in Attendance:</b> $otm_attendance_pop</td>\r\n";
echo "	   <td colspan=2 valign=top><b>Date(s) of Program:</b> $otm_date_of_program</td></tr>\n\r\n";
echo "<tr><td colspan=2 valign=top><b>Number of People Needed to Organize:</b> $otm_organize_pop</td>\r\n";
echo "	   <td colspan=2 valign=top><b>Cost of Program:</b> $otm_cost_of_program</td></tr>\n\r\n";
echo "<tr><td colspan=2 valign=top><b>On-Campus Population:</b> $otm_campus_pop</td>\r\n";
echo "	   <td colspan=2 valign=top><b>Chapter Size:</b> $otm_chapter_size</td></tr>\r\n";

echo "<tr><td colspan=4></td></tr>\r\n";
echo "<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>\r\n";
$otm_origin = ereg_replace("(\r\n\r\n)", "<P>", $otm_origin);
echo "<tr><td colspan=4 ALIGN=LEFT><B><CENTER>Origin of Program:</CENTER></B><P><font size=-1>$otm_origin<P>Word Count: $otm_origin_count</td></tr>\r\n";

echo "<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>\r\n";
$otm_description = ereg_replace("(\r\n\r\n)", "<P>", $otm_description);
echo "<tr><td colspan=4 ALIGN=LEFT><B><CENTER>Please give a short description of the program:</CENTER></B><P><font size=-1>$otm_description<P>Word Count: $otm_description_count</td></tr>\r\n";

echo "<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>\r\n";
$otm_goals = ereg_replace("(\r\n\r\n)", "<P>", $otm_goals);
echo "<tr><td colspan=4 ALIGN=LEFT><B><CENTER>Goals of the program:</CENTER></B><P><font size=-1>$otm_goals<P><font size=-1>Word Count: $otm_goal_count</td></tr>\r\n";

echo "<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>\r\n";
$otm_effects = ereg_replace("(\r\n\r\n)", "<P>", $otm_effects);
echo "<tr><td colspan=4 ALIGN=LEFT><B><CENTER>Positive and lasting effects of the program:</CENTER></B><P><font size=-1>$otm_effects<P>Word Count: $otm_effect_count</td></tr>\r\n";

echo "<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>\r\n";
$otm_evaluation = ereg_replace("(\r\n\r\n)", "<P>", $otm_evaluation);
echo "<tr><td colspan=4 ALIGN=LEFT><B><CENTER>Short evaluation of the program:</CENTER></B><P><font size=-1>$otm_evaluation<P>Word Count: $otm_evaluation_count</td></tr>\r\n";

echo "<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>\r\n";
$otm_adapted = ereg_replace("(\r\n\r\n)", "<P>", $otm_adapted);
echo "<tr><td colspan=4 ALIGN=LEFT><B><CENTER>How could this program be adapted to other campuses?</CENTER></B><P><font size=-1>$otm_adapted<P>Word Count: $otm_adapt_count</td></tr>\r\n";

echo "<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width='80%' size='2'></TD></TR>\r\n";

// $nomination = ereg_replace("(\r\n\r\n)", "<P>", $nomination);
// print stripslashes($nomination);

?>

</td></tr></table>
</TD><TD valign=top align=left>

<? 
$activateSocialMedia = $year_view . $month_view . $date_view . $hour_view . "0000";

if ($today_year . $today_date_value > $activateSocialMedia) {
//	if ($ID == 1) { FBLike("program",$queryvar); } else { 
FBLike("program",$queryvar); FBShare("program",$queryvar); FBComment("program",$queryvar,$FBappID); 
//}
/*
if ($ID == 1) {
?>
<CENTER><h5>Sign into OTMS.NRHH.org <BR>to post comments on Facebook</h5></CENTER>

<div class="login-component">
  <form method="post" action='<? echo $queryvar; ?>'>
    <div class="login-box">
        <div class="input-row">
          Username:  <input tabindex="2" type="text" name="entered_login" size="10" maxlength="64" value="" />
        </div>
        <div class="input-row">
          Password: <input tabindex="2" type="password" name="entered_password" size="10" maxlength="32" />
        </div>   
        <div class="input-row">
          <input tabindex="2" type="submit" value=" &#160; Sign In&#160; " />
        </div>
    </div>
  </form>
<?     
}
*/

}
 ?>

</TD></TR></TABLE>
<BR>
<?

echo "Date of entry into database: $otm_date_of_entry";
echo "<P>\r\n";
if ((action != "update") && ($uid == 1)) { echo "<h3><a href='/'>NRHH Database Home</a></h3>\r\n"; }
?>
</BODY></HTML>
