<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML//EN">
<? FBOpener(); ?>
<HEAD>
<? echo "<TITLE>$cat_name OTM - $otm_nominee - $otm_month_name $otm_year</TITLE>"; ?>
<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">
<? FBMetaData($FBappID); ?>
<? echo $stylesheet; ?>
</HEAD>
<BODY>

<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-KVVFFC"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KVVFFC');</script>
<!-- End Google Tag Manager -->

<? FBScript($FBappID) ?>

<left><TABLE BORDER='0' CELLSPACING='0' CELLPADDING='8' WIDTH='820'><TR><TD valign='top'>
<TABLE class='box-displayotm' BORDER='0' CELLSPACING='0' CELLPADDING='4' WIDTH='620'>
<?
echo "<tr><td colspan=2 class='box-displayotm-winner'><div align=left><b>$award</b></div></td>\r\n";
echo "<td colspan=2><div align=right><B>Month</B>:  $otm_month_name $otm_year</div></td></tr>\r\n";
echo "<tr><td colspan=4 class='box-displayotm-title'><div align=center><p><b>NACURH, INC.</font></b></p></div></td></tr>\r\n";
echo "<tr><td colspan=4 class='box-displayotm-title'><div align=center><b>$cat_name of the Month</b></div></td></tr>\r\n";
echo "<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>\r\n";
echo "<tr><td width=85 valign=top><b>School:</b></td>\r\n";
echo "<td width=$col_text>$school</td>\r\n";
echo "<td width=$col_title valign=top><b>Region:</b></td>\r\n";
echo "<td width=$col_text>$region</td></tr>\r\n";

echo "<tr><td width=$col_title valign=top><b>Nominee:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_nominee</td>\r\n";
echo "<td width=$col_title valign=top><b>Nominator:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_nominator</td></tr>\r\n";

if ($ID > 1) {
echo "<tr><td width=$col_title height=2 valign=top><b>Address:</b></td>\r\n";
echo "<td width=$col_text height=2>$otm_nominee_address_1<BR>$otm_nominee_address_2<BR>$otm_nominee_address_3</td>\r\n";
echo "<td width=$col_title height=2 valign=top><b>Address:</b></td>\r\n";
echo "<td width=$col_text height=2>$otm_nominator_address_1<BR>$otm_nominator_address_2<BR>$otm_nominator_address_3</td></tr>\r\n";

echo "<tr><td width=$col_title valign=top><b>Phone:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_nominee_phone</td>\r\n";
echo "<td width=$col_title valign=top><b>Phone:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_nominator_phone</td></tr>\r\n";
echo "<tr><td width=$col_title valign=top><b>Email:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_nominee_email</td>\r\n";
echo "<td width=$col_title valign=top><b>Email:</b></td>\r\n";
echo "<td width=$col_text valign=top>$otm_nominator_email</td></tr>\r\n";
}

echo "<tr><td colspan=4></td></tr>\r\n";
echo "<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>\r\n";
echo "<tr><td colspan=2 valign=top><b>On-Campus Population:</b> $campus_pop</td>\r\n";
echo "<td colspan=2 valign=top><b>Chapter Size:</b> $chapter_size</td></tr>\r\n";
?>
        
<tr><td colspan=4></td></tr>
<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>
<tr><td colspan=4>Please explain the <b>outstanding</b> contributions of the nominee <b>during the month of nomination</b></td></tr>
<TR><TD COLSPAN=4 ALIGN=CENTER><hr noshade width=80% size=2></TD></TR>
<tr><td COLSPAN=4>
<?

$nomination = ereg_replace("(\r\n\r\n)", "<P>", $otm_nomination);
$nomination = ereg_replace("(\r\n\t)", "<P>", $nomination);  

print stripslashes($nomination);

echo "<P>Word Count: $otm_word_count";
?>

</td></tr></table>
</TD><TD valign=top align=left width=200>

<? 
$activateSocialMedia = $year_view . $month_view . $date_view . $hour_view . "0000";

if ($today_year . $today_date_value > $activateSocialMedia) {
//	if ($ID == 1) { FBLike("general",$queryvar); } else { 
FBLike("general",$queryvar); FBShare("general",$queryvar); 
FBComment("general",$queryvar,$FBappID); 

//} 
/*
if ($ID == 1) {
?>
<CENTER><h5>Sign into OTMS.NRHH.org <BR>to post comments on Facebook</h5></CENTER>

<div class="login-component">
  <form method="post" action='<? echo $queryvar; ?>'>
    <div class="login-box">
        <div class="input-row">
          Username:  <input tabindex="2" type="text" name="entered_login" size="10" maxlength="64" value="" />
        </div>
        <div class="input-row">
          Password: <input tabindex="2" type="password" name="entered_password" size="10" maxlength="32" />
        </div>
        <div class="input-row">
          <input tabindex="2" type="submit" value=" &#160; Sign In&#160; " />
        </div>  
    </div>
  </form>
<?
}
*/
//echo "<fb:comments-count href=http://otms.nrhh.org$queryvar></fb:comments-count> awesome comments";
//echo "<iframe src='http://www.facebook.com/plugins/comments.php?href=otms.nrhh.org" . addslashes($queryvar) . "&permalink=1' scrolling='no' frameborder='0' style='border:none; overflow:hidden; width:130px; height:16px;' allowTransparency='true'></iframe>";
}
?>

</TD></TR></TABLE>
<BR>
<?

echo "Date of entry into database: $otm_date_of_entry";
echo "<P>\r\n";
if ((action != "update") && ($uid == 1)) { echo "<h3><a href='/'>NRHH Database Home</a></h3>\r\n"; }
?>
