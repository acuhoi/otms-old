<?

// otms database connection
$otm_query="
	SELECT * FROM otms 
	LEFT JOIN otms_program ON otms.oid=otms_program.oid 
	LEFT JOIN config_categories ON otms.ccid=config_categories.ccid 
	LEFT JOIN config_orgs ON otms.coid=config_orgs.coid
	LEFT JOIN config_orgs co2 ON otms.region=co2.coid
	LEFT JOIN lu_months ON concat( REPLACE (substring( otms.year_month, 6, 1 ) , '0', '' ) ,substring( otms.year_month, 7, 1 )) = lu_months.lumid
	LEFT JOIN config_awards ON otms.award=config_awards.caid 
	where otms.oid='$oid'";
$otm_result=mysql_query($otm_query);
$otm_num=mysql_numrows($otm_result); 

$otm_uid=mysql_result($otm_result,0,"uid");
$otm_sub_org_id=mysql_result($otm_result,0,"sub_org");
$otm_rec_org_id=mysql_result($otm_result,0,"rec_org");
$otm_year_month=mysql_result($otm_result,0,"year_month");
$otm_ccid=mysql_result($otm_result,0,"ccid");
$otm_cpid=mysql_result($otm_result,0,"cpid");
$otm_coid=mysql_result($otm_result,0,"coid");
$otm_crid=mysql_result($otm_result,0,"region"); // its a coid but a region
$otm_nominee=mysql_result($otm_result,0,"nominee");
$otm_nominee_address_1=mysql_result($otm_result,0,"nominee_address_1");
$otm_nominee_address_2=mysql_result($otm_result,0,"nominee_address_2");
$otm_nominee_address_3=mysql_result($otm_result,0,"nominee_address_3");
$otm_nominee_phone=mysql_result($otm_result,0,"nominee_phone");
$otm_nominee_email=mysql_result($otm_result,0,"nominee_email");
$otm_nominator=mysql_result($otm_result,0,"nominator");
$otm_nominator_address_1=mysql_result($otm_result,0,"nominator_address_1");
$otm_nominator_address_2=mysql_result($otm_result,0,"nominator_address_2");
$otm_nominator_address_3=mysql_result($otm_result,0,"nominator_address_3");
$otm_nominator_phone=mysql_result($otm_result,0,"nominator_phone");
$otm_nominator_email=mysql_result($otm_result,0,"nominator_email");
$otm_campus_pop=mysql_result($otm_result,0,"campus_pop");
$otm_chapter_size=mysql_result($otm_result,0,"chapter_size");
$otm_date_of_entry=mysql_result($otm_result,0,"date_of_entry");
$otm_caid=mysql_result($otm_result,0,"award");
$otm_viewed=mysql_result($otm_result,0,"viewed");
$otm_award_level=mysql_result($otm_result,0,"award_level");
$otm_vid=mysql_result($otm_result,0,"vid");

$otm_person_in_charge=mysql_result($otm_result,0,"person_in_charge");
$otm_target_pop=mysql_result($otm_result,0,"target_pop");
$otm_attendance_pop=mysql_result($otm_result,0,"attendance_pop");
$otm_organize_pop=mysql_result($otm_result,0,"organize_pop");
$otm_organize_time=mysql_result($otm_result,0,"organize_time");
$otm_date_of_program=mysql_result($otm_result,0,"date_of_program");
$otm_cost_of_program=mysql_result($otm_result,0,"cost_of_program");
$otm_origin=mysql_result($otm_result,0,"origin");
$otm_origin_count=mysql_result($otm_result,0,"origin_count");
$otm_description=mysql_result($otm_result,0,"description");
$otm_description_count=mysql_result($otm_result,0,"description_count");
$otm_goals=mysql_result($otm_result,0,"goals");
$otm_goal_count=mysql_result($otm_result,0,"goal_count");
$otm_effects=mysql_result($otm_result,0,"effects");
$otm_effect_count=mysql_result($otm_result,0,"effect_count");
$otm_evaluation=mysql_result($otm_result,0,"evaluation");
$otm_evaluation_count=mysql_result($otm_result,0,"evaluation_count");
$otm_adapted=mysql_result($otm_result,0,"adapted");
$otm_adapt_count=mysql_result($otm_result,0,"adapt_count");

$cat_name=mysql_result($otm_result,0,"config_categories.name");
$school=mysql_result($otm_result,0,"config_orgs.name");
$region=mysql_result($otm_result,0,"co2.name");

$otm_year=substr($otm_year_month, 0, 4);
$otm_month=(intval(substr($otm_year_month, 5, 2)));

$otm_month_name=mysql_result($otm_result,0,"lu_months.name");

// AWARD LOOKUP
  if ($otm_caid > 0) {
    $award=mysql_result($otm_result,0,"config_awards.name");
  } else { $award = ""; }


$mon_lookup = substr($otm_year_month, 5, 2);

$view_query="SELECT * FROM config_dates_otmview WHERE coid='$otm_crid' AND month='$mon_lookup'";
$view_result=mysql_query($view_query);
$view_num=mysql_numrows($view_result);
$month_view=mysql_result($view_result,0,"month_view");
$date_view=mysql_result($view_result,0,"date_view");
$hour_view=mysql_result($view_result,0,"hour_view");
$year_view=substr($otm_year_month, 0, 4);


$allow_presubmit=mysql_result($otm_result,0,"config_orgs.allow_presubmit");
if ($allow_presubmit == 1) { 
	$school_categories=mysql_result($otm_result,0,"config_orgs.categories");
} else { 
	$school_categories=mysql_result($otm_result,0,"co2.categories");
}
$ar_cat = split(",", $school_categories);
?>
