<?
	$logout = true; 
	$cfgProgDir =  'login/phpSecurePages/';
	include($cfgProgDir . "secure.php");
	include ("config/dbinfo.inc.php");
?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>    
    <title>NRHH OTMs</title>
    <link rel="shortcut icon" href="/img/favicon.ico" />
    <link rel="stylesheet" href="nrhh-base.css" type="text/css" />
  </head>

  <body>
<img id="navlogo" src="/img/otm_search_img.gif" alt="NRHH Logo" />
<span id="header">
<b><font size="+1">Welcome to the</font><br><font size="+2">OTM Database Search</center></b></font>
</span><BR><BR>
<span id="navtabs">
</td>
</tr>
</table>

</span>
<a href=/support.php><img id="navhelp" src="img/help.gif" alt="Help" /></a>
<a href=/faqs.php id="navhelp"><BR><BR><B>F.A.Q.</B></a>&nbsp;&nbsp;<BR>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
      <tr class="bar-undertabs">
        <td><div></div></td>
      </tr>
</table>
    

    <table class="bar-status" width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr valign="middle">
        <td class="bar-login" width="25%">
	&#160; 
        </td>

        <td class="bar-ssm" width="30%">
          &#160; 
        </td>

      </tr>
    </table>


    <table width="90%" align="center" cellspacing="20" cellpadding="0">
      <tr valign="top" align="left">
        <td>

	  <div class="linkchain">
	    <a href="/">NRHH Database Home</a>
	    <BR>
	  </div>


         <h2>Submit OTMS</h2>
	  <div class="linkchain">
	    <a href="/form_general.php3">General Category</a>
	    <BR>
	    <a href="/form_program.php3">Program Category</a>
	  </div>
<!--
         <h2>Search OTMS</h2>
	  <div class="linkchain">
	    <a href="/nrhh_search.php3">Advanced Search</a>
	    <BR>
	  </div>
-->
        <H2>Quick Report Links</H2>
          <div class="linkchain">
           <a 
href="/nrhh_search_query.php3?column=coid&condition=is&keyword_crid=&either_crid=AND&column=coid&condition=is&keyword_coid=&either=AND&column_ccid=ccid&condition_ccid=is&keyword_ccid=&either2=AND&column_award=award&condition_award=is&keyword_award=6&column_ym=year_month&condition_ym=is&keyword_ym=&year=&either4=AND&column_nom=nominee&condition_nom=is&keyword_nom=&cl=iau&order_field=otms.year_month&order=DESC&llimit=0&increment=250">National 
Winners</A><BR>
          <a href="/nrhh_search_query.php3?column=coid&condition=is&keyword_crid=&either_crid=AND&column=coid&condition=is&keyword_coid=&either=AND&column_ccid=ccid&condition_ccid=is&keyword_ccid=&either2=AND&column_award=award&condition_award=%3E%3D&keyword_award=4&column_ym=year_month&condition_ym=is&keyword_ym=&year=&either4=AND&column_nom=nominee&condition_nom=is&keyword_nom=&cl=iau&order_field=otms.year_month&order=DESC&llimit=0&increment=250">Regional 
Winners</A>
        <BR>

<? 
$query="select * from config_orgs left join config_dates_otmview on config_orgs.coid=config_dates_otmview.coid where config_orgs.level='regional' and month_view=substring(NOW(),6,2) and date_view < 
substring(NOW(),9,2)";
        $result=mysql_query($query);
        $num=mysql_numrows($result);

$i=0;
while ($i < $num) {
$coid=mysql_result($result,$i,"config_orgs.coid");
$org_name=mysql_result($result,$i,"config_orgs.name");
$lookup_month=mysql_result($result,$i,"month");

if ((intval($month_due) < $lookup_month)) {
        $show_year = $today_year - 1;
} else {
        $show_year = $today_year;
}

echo "<a 
href='/nrhh_search_query.php3?column=coid&condition=is&keyword_crid=$coid&either_crid=AND&column=coid&condition=is&keyword_coid=&either=AND&column_ccid=ccid&condition_ccid=is&keyword_ccid=&either2=AND&column_award=award&condition_award=is&keyword_award=&column_ym=year_month&condition_ym=is&keyword_ym=$lookup_month&year=$show_year&either4=AND&column_nom=nominee&condition_nom=is&keyword_nom=&cl=iau&order_field=config_categories.name&order=ASC&llimit=0&increment=250'>
$lookup_month/$show_year $org_name OTMs</A><BR>";

++$i;
}

?>

        </div>


          <h2>Sign into OTMS.NRHH.org</h2>

<div class="login-component">
  <form method="post" action='login/'>
    <div class="login-box">
        <div class="input-row">
          Username:  <input tabindex="2" type="text" name="entered_login" size="10" maxlength="64" value="" />
        </div>
        <div class="input-row">
          Password: <input tabindex="2" type="password" name="entered_password" size="10" maxlength="32" />
        </div>
        <div class="input-row">
          <input tabindex="2" type="submit" value=" &#160; Sign In&#160; " />
        </div>  
    </div>
  </form>
  <div class="linkchain">
    <a href="/form_create_account.php3">Create Account</a>
    <BR>
    <a href="/form_password.php3">Lost Password?</a>
  </div>
  
</div><BR><BR>
<CENTER><img src="img/img_logos.gif"></CENTER>
        </td>




<!-- MIDDLE COLUMN -->
        <td>

<!-- START PRIVACY POLICY CODE --><div style="font-family:arial"><strong>What information do we collect?</strong> <br /><br />We collect information from you when you register on our site.  <br /><br 
/>When ordering or registering on our site, as appropriate, you may be asked to enter your: name, e-mail address or mailing address. You may, however, visit our site anonymously.<br /><br 
/><strong>What do we use your information for?</strong> <br /><br />Any of the information we collect from you may be used in one of the following ways: <br /><br />; To personalize your experience<br 
/>(your information helps us to better respond to your individual needs)<br /><br />; To improve our website<br />(we continually strive to improve our website offerings based on the information and 
feedback we receive from you)<br /><br />; To improve customer service<br />(your information helps us to more effectively respond to your customer service requests and support needs)<br /><br />; To 
process transactions<br /><blockquote>Your information, whether public or private, will not be sold, exchanged, transferred, or given to any other company for any reason whatsoever, without your 
consent, other than for the express purpose of delivering the purchased product or service requested.</blockquote><br />; To send periodic emails<br /><br />The email address you provide may be used 
to send you information, respond to inquiries, and/or other requests or questions.<br /><br /><strong>How do we protect your information?</strong> <br /><br />We implement a variety of security 
measures to maintain the safety of your personal information when you enter, submit, or access your personal information. <br /> <br />We offer the use of a secure server. All supplied 
sensitive/credit information is transmitted via Secure Socket Layer (SSL) technology and then encrypted into our Database to be only accessed by those authorized with special access rights to our 
systems, and are required to?keep the information confidential. <br /><br />After a transaction, your private information (credit cards, social security numbers, financials, etc.) will not be stored 
on our servers.<br /><br /><strong>Do we use cookies?</strong> <br /><br />We do not use cookies.<br /><br /><strong>Do we disclose any information to outside parties?</strong> <br /><br />We do not 
sell, trade, or otherwise transfer to outside parties your personally identifiable information. This does not include trusted third parties who assist us in operating our website, conducting our 
business, or servicing you, so long as those parties agree to keep this information confidential. We may also release your information when we believe release is appropriate to comply with the law, 
enforce our site policies, or protect ours or others rights, property, or safety. However, non-personally identifiable visitor information may be provided to other parties for marketing, advertising, 
or other uses.<br /><br /><strong>Third party links</strong> <br /><br /> Occasionally, at our discretion, we may include or offer third party products or services on our website. These third party 
sites have separate and independent privacy policies. We therefore have no responsibility or liability for the content and activities of these linked sites. Nonetheless, we seek to protect the 
integrity of our site and welcome any feedback about these sites.<br /><br /><strong>California Online Privacy Protection Act Compliance</strong><br /><br />Because we value your privacy we have taken 
the necessary precautions to be in compliance with the California Online Privacy Protection Act. We therefore will not distribute your personal information to outside parties without your consent.<br 
/><br />As part of the California Online Privacy Protection Act, all users of our site may make any changes to their information at anytime by logging into their control panel and going to the 'Edit 
Profile' page.<br /><br /><strong>Online Privacy Policy Only</strong> <br /><br />This online privacy policy applies only to information collected through our website and not to information collected 
offline.<br /><br /><strong>Your Consent</strong> <br /><br />By using our site, you consent to our <a style='text-decoration:none; color:#3C3C3C;' href='http://www.freeprivacypolicy.com/' 
target='_blank'>privacy policy</a>.<br /><br /><strong>Changes to our Privacy Policy</strong> <br /><br />If we decide to change our privacy policy, we will post those changes on this page. <br /><br 
/><br /><span></span><span></span>This policy is powered by Free Privacy Policy and Rhino Support <a style='color:#000; text-decoration:none;' href='http://www.rhinosupport.com' 
target='_blank'>helpdesk software</a>.<span></span><span></span><span></span><!-- END PRIVACY POLICY CODE -->
	</TD> 
      </tr>
    </table>

<? include "copyright.h"; ?>

  </body>
</html>




