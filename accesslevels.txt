1 -- Total Administrator's Rights -- Andrew Bell -- See more than anyone else.

10 -- National OTM Administrator -- be able to edit any OTM submitted.  Also have the power to edit any User in the System.  Should only be able to make OTMs a national winner when they are a regional winner.  Should not have the option of allowing a university to presubmit.

20 -- Regional OTM Administrator -- be able to edit OTMs in their region.  Should only be able to make OTMs a regional winner when they are a campus winner from their region.  Their OTM point system should only calculate points for universities and not individual orgs_sub.  Should be able to add a University.  When they do add a university -- should allow them the option of adding the option of them presubmitting.   If they are presubmitting, system needs to insert rows with default values.  If they are deleted... systems should remove the extra information.
Should have the option of determining regional due dates.

30 -- Local OTM Administrator -- be able to edit OTMs from their univ before they are due to the regional level.  Should only have this account when univ is presubmitting.  Will be in charge of determining wwhat columns their university sees, campus info, and local dates and point values for their system.

35 -- Local OTM Committee Member -- should have the option of editing any otms during grading period.  Should have the option of voting on the local/regional/national levels (0-3) if deemed by the respective parties.

40 -- Regular user -- be able to edit OTMs that they have submitted before their local univ is due or their regional level is due.  any user should have the option of voting on the regional/national levels (0-2) if deemed by the respective parties.


any user can not make a person have rights above them -- only equal to them.  OTM Committee members will not be able to edit or view members of the system.  Same for regular user.



What if a local univ wants someone else to keep track of OTM points?
What if a univ has its own point structure... can that integrate in?

Any administrator should have the option of sending an e-mail on behalf of the system... however, the name of the From line should be different.
An adminsitrator's account should not have the option of submitting and OTM.  They should use a separate account for submitting any and all OTMs.